import { Title } from './title';

export interface ScienceField {
  id?: number;
  name: string;
  title?: Title;
}
