import { Student } from './student';
import { CourseRealization } from './courseRealization';

export interface CourseAttendance {
  id?: number;
  finalGrade: number;
  student: Student;
  courseRealization: CourseRealization;
}
