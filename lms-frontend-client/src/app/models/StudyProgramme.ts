import { Teacher } from './teacher';
import { Faculty } from './faculty';
import { YearOfStudy } from './yearOfStudy';
import { Subject } from './subject';

export interface StudyProgramme {
  id?: number;
  name: string;
  coordinator: Teacher;
  faculty: Faculty;
  yearOfStudy: YearOfStudy;
  subjects: Subject[];
}
