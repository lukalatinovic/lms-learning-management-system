import { Address } from './address';
import { StudentOnYear } from './studentOnYear';
import { User } from './user';

export interface Student {
  id: number;
  jmbg: string;
  firstName: string;
  lastName: string;
  address: Address;
  studentOnYear: StudentOnYear[];
  formattedAddress: string;
  user: User;
}
