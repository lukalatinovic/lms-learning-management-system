import { Country } from './country';

export interface PlaceOfResidence {
  id?: number;
  name: string;
  country: Country;
}
