import { Address } from './address';
import { Teacher } from './teacher';
import { StudyProgramme } from './StudyProgramme';
import { University } from './university';

export interface Faculty {
  id?: number;
  name: string;
  address: Address;
  dean: Teacher;
  studyProgrammes: StudyProgramme[];
  university: University;
}
