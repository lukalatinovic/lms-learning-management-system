import { Subject } from './subject';
import { CourseAttendance } from './courseAttendance';
import { Teacher } from './teacher';

export interface CourseRealization {
  id?: number;
  subject: Subject;
  teacher: Teacher
  courseAttendances?: CourseAttendance[];
}
