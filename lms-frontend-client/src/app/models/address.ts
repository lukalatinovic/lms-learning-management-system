import { PlaceOfResidence } from './placeOfResidence';

export interface Address {
  id?: number;
  street: string;
  number: string;
  placeOfResidence: PlaceOfResidence;
  latitude?: number;
  longitude?: number;
}
