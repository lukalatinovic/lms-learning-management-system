import { Title } from './title';
import { Faculty } from './faculty';
import { University } from './university';
import { StudyProgramme } from './StudyProgramme';
import { Address } from './address';
import { User } from './user';

export class Teacher {
  id?: number;
  jmbg: string;
  firstName: string;
  lastName: string;
  biography: string;
  title: Title;
  faculty?: Faculty;
  university?: University;
  studyProgramme?: StudyProgramme;
  address?: Address;
  user?: User;

  constructor (jmbg: string, firstName: string, lastName: string, biography: string, 
    title: Title, user: User,
  ) {
    this.jmbg = jmbg
    this.firstName =firstName
    this.lastName = lastName
    this.biography = biography
    this.title = title
    this.user = user
  }
}
