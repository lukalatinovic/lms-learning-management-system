import { Title } from './title';

export interface TitleType {
  id?: number;
  name: string;
  title?: Title;
}
