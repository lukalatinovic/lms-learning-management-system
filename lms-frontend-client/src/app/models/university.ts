import { Teacher } from './teacher';
import { Address } from './address';
import { Faculty } from './faculty';

export interface University {
  id?: number;
  name: string;
  foundationDate: Date;
  rector?: Teacher;
  address: Address;
  faculties: Faculty[];
}


