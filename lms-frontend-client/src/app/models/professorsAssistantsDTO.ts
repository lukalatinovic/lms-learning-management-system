import { Teacher } from "./teacher";

export interface ProfessorsAssistantsDTO {
    professors: Teacher[],
    assistants: Teacher[]
}