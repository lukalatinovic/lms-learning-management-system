import { CourseRealization } from './courseRealization';

export interface Subject {
  id?: number;
  name: string;
  espb: number;
  mandatory: boolean;
  numLectureClasses: number;
  numPracticeClasses: number;
  numOtherFormsClasses: number;
  researchWork: number;
  numOtherClasses: number;
  courseRealizations: CourseRealization[];
}
