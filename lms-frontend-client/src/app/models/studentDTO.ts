export class StudentDTO {
    firstName: String;
    lastName: String;
    index?: String;
    yearOfEnrollments?: String;
    averageGrade: number;
    ectsScoredPoints: number;

    constructor(firstName: String, lastName: String, averageGrade: number, ectsScoredPoints: number, index?: String, yearOfEnrollments?: String,
    
    ) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.index = index;
        this.yearOfEnrollments = yearOfEnrollments;
        this.averageGrade = averageGrade;
        this.ectsScoredPoints = ectsScoredPoints;
    }
}