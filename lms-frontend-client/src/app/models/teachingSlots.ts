import { CourseRealization } from "./courseRealization"
import { Outcome } from "./outcome"
import { TeachingType } from "./teachingType"

export interface TeachingSlots {
    id?: number,
    startTime: Date
    endTime: Date,
    outcome?: Outcome,
    courseRealization?: CourseRealization,
    teachingType?: TeachingType,
}