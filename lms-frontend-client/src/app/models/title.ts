import { ScienceField } from './scienceField';
import { TitleType } from './titleType';

export interface Title {
  id?: number;
  electedDate: Date;
  expireDate: Date;
  scienceField: ScienceField;
  titleType: TitleType;
}
