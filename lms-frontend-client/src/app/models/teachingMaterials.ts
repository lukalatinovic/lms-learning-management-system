export interface TeachingMaterials {
    id?: number;
    name: string;
    authors: string;
    yearOfPublication: string;
  }
  