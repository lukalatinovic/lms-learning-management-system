import { Student } from './student';
import { YearOfStudy } from './yearOfStudy';

export interface StudentOnYear {
  id?: number;
  dateOfEnroll: Date | null;
  indexNumber: string;
  student: Student;
  yearOfStudy: YearOfStudy | null;
}
