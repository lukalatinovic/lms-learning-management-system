import { Subject } from "./subject";
import { Teacher } from "./teacher";

export interface CourseViewDTO {
    subject: Subject,
    year: Date,
    studyProgramName: string,
    facultyName: string,
    teacher: Teacher,
    courseRealizationId?: number,
}