import { StudyProgramme } from './StudyProgramme';
import { StudentOnYear } from './studentOnYear';

export interface YearOfStudy {
  id?: number;
  year: Date;
  studyProgramme: StudyProgramme;
  studentOnYear: StudentOnYear[];
}
