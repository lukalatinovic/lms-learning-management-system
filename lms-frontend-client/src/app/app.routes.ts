import { Routes } from '@angular/router';
import { FacultyTableComponent } from './components/faculty-table/faculty-table.component';
import { HomepageComponent } from './homepage/homepage.component';
import { StudentPageComponent } from './components/student-page/student-page.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { UserTableComponent } from './components/user-table/user-table.component';
import { AuthGuard } from './auth_guard';
import { StudentTableComponent } from './components/student-table/student-table.component';
import { SubjectsTableComponent } from './components/subjects-table/subjects-table.component';
import { StudentsBySubjectComponent } from './components/students-by-subject/students-by-subject.component';
import { SubjectsComponent } from './components/subjects/subjects.component';
import { TeachingSlotsComponent } from './components/teaching-slots/teaching-slots.component';
import { StudentsSearchComponent } from './components/students-search/students-search.component';
import { TeacherFormComponent } from './components/teacher-form/teacher-form.component';
import { StudentServiceFormComponent } from './components/student-service-form/student-service-form.component';
import { CoursesTableComponent } from './components/courses-table/courses-table.component';
import { StudentFormComponent } from './components/student-form/student-form.component';
import { TeachingSlotFormComponent } from './components/teaching-slot-form/teaching-slot-form.component';
import { TeachingMaterialsComponent } from './components/teaching-materials/teaching-materials.component';

export const routes: Routes = [
    { path: 'students', component: StudentPageComponent, data: { animation: "students", roles: ["studentService"] }, canActivate: [AuthGuard] },
    { path: 'faculties', component: FacultyTableComponent, data: { animation: "faculties" } },
    { path: 'homepage', component: HomepageComponent, data: { animation: "homepage" } },
    { path: 'teaching-materials', component: TeachingMaterialsComponent },
    { path: '', component: HomepageComponent, data: { animation: "homepage"} },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'user-table', component: UserTableComponent, canActivate: [AuthGuard], data: {roles: ["admin"]}},
    { path: 'subjects', component: SubjectsComponent, canActivate: [AuthGuard], data: {roles: ["teacher"]}, children: [
        { path: '', redirectTo: 'subjects-by-teacher', pathMatch: 'full' },
        { path: 'subjects-by-teacher', component: SubjectsTableComponent, canActivate: [AuthGuard] },
        { path: 'students-by-subject', component: StudentsBySubjectComponent, canActivate: [AuthGuard] }
    ]},
    { path: 'teaching-slots', component: TeachingSlotsComponent, canActivate: [AuthGuard], data: {roles: ["teacher"]}},
    { path: 'students-search', component: StudentsSearchComponent, canActivate: [AuthGuard], data: {roles: ["teacher"]}},
    {path: "teacher-form", component: TeacherFormComponent, canActivate: [AuthGuard], data: {roles: ["admin"]}},
    {path: "student-service-form", component: StudentServiceFormComponent, canActivate: [AuthGuard], data: {roles: ["admin"]}},
    {path: "courses-table", component: CoursesTableComponent, canActivate: [AuthGuard], data: {roles: ["admin"]}},
    {path: "teaching-slots-form", component: TeachingSlotFormComponent, canActivate: [AuthGuard], data: {roles: ["studentService"]}}
];
