import { Component, AfterViewInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import * as L from 'leaflet';
import { UniversityService } from '../services/university/university.service';
import { University } from '../models/university';

@Component({
  selector: 'app-homepage',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements AfterViewInit {
  university: University | null = null;
  latitude: number = 44.7866; // Default latitude (e.g., Belgrade)
  longitude: number = 20.4489; // Default longitude (e.g., Belgrade)
  map: any;

  constructor(private universityService: UniversityService) {}

  ngAfterViewInit(): void {
    this.universityService.getUniversity(1).subscribe(
      (data) => {
        this.university = data;
        if (data.address.latitude && data.address.longitude) {
          this.latitude = data.address.latitude;
          this.longitude = data.address.longitude;
        }

        // Proverite da li element postoji pre nego što pokrenete inicijalizaciju mape
        setTimeout(() => {
          this.initMap();
        }, 0);
      },
      (error) => {
        console.error('Error fetching university data', error);
      }
    );
  }

  initMap(): void {
    if (document.getElementById('map')) {
      this.map = L.map('map').setView([this.latitude, this.longitude], 13);

      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
      }).addTo(this.map);

      L.marker([this.latitude, this.longitude]).addTo(this.map)
        .bindPopup(`${this.university?.name}`)
        .openPopup();
    } else {
      console.error('Map container not found');
    }
  }
}
