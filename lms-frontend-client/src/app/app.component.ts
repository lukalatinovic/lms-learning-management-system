import { CommonModule, NgFor } from '@angular/common';
import { Component, ViewChild } from '@angular/core';
import { RouterModule, Router, RouterOutlet } from '@angular/router';
import { MatTabsModule } from '@angular/material/tabs';
import { User } from './models/user';
import { AuthenticationService } from './services/authentication/authentication.service';
import { NgIf } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSidenav, MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    RouterOutlet, NgFor, RouterModule, MatTabsModule, NgIf, CommonModule,
    MatToolbarModule, MatCardModule, MatGridListModule, MatSidenavModule, MatListModule, MatIconModule
  ],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'lms-frontend-client'; 

  isUserAuthenticated: boolean;

  @ViewChild('drawer') drawer: MatSidenav;

  isDrawerOpened = true;

  ngOnInit(): void {
    this.authenticationService.isAuthenticated.subscribe(status => {
      this.isUserAuthenticated = status;
    });
  }

  toggleDrawer(): void {
    this.isDrawerOpened = !this.isDrawerOpened;
  }

  links = ['homepage', 'students', 'faculties', 'register', 'login', 'user-table'];
  activeLink = this.links[0];
  background = '';

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }

  hasAnyRole(roles: string[]) {
    return this.authenticationService.hasAnyRole(roles)
  }

  currentUser: User;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) { }

  get showLogoutButton() {
    return this.authenticationService.isAuthenticated;
  }

  logout() {
    this.authenticationService.logout();
    this.isUserAuthenticated = false;
    this.router.navigate(['/login']);
  }
}
