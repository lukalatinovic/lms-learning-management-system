import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Student } from '../../models/student';
import { AppConstants } from '../../constants';
import { AuthenticationService } from '../authentication/authentication.service';
import { StudentDTO } from '../../models/studentDTO';

@Injectable({
  providedIn: 'root'
})
export class StudentService {
  private apiUrl = 'http://localhost:8091/api/students';

  constructor(private http: HttpClient, private authenticationService: AuthenticationService) { }

  getStudents(): Observable<Student[]> {
    return this.http.get<Student[]>(this.apiUrl);
  }

  getStudent(id: number): Observable<Student> {
    return this.http.get<Student>(`${this.apiUrl}/${id}`);
  }

  createStudent(student: Student): Observable<Student> {
    return this.http.post<Student>(this.apiUrl, student);
  }

  updateStudent(id: number, student: Student): Observable<Student> {
    return this.http.put<Student>(`${this.apiUrl}/${id}`, student);
  }

  deleteStudent(id: number): Observable<void> {
    return this.http.delete<void>(`${this.apiUrl}/${id}`);
  }

  deleteStudents(ids: number[]): Observable<void> {
    return this.http.post<void>(`${this.apiUrl}/batchDelete`, ids, {
        headers: { 'Content-Type': 'application/json' }
    });
}

  getStudentsByTeacherId(): Observable<StudentDTO[]> {
    return this.http.get<StudentDTO[]>(`${AppConstants.BASE_URL}students/teacher/${this.authenticationService.getUserId()}`);
  }

  searchStudents(criteria: any): Observable<Student[]> {
    return this.http.get<Student[]>(`${this.apiUrl}?search=${JSON.stringify(criteria)}`);
  }
}
