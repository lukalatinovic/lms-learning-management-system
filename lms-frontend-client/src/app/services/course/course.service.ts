import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppComponent } from '../../app.component';
import { AppConstants } from '../../constants';
import { CourseViewDTO } from '../../models/courseViewDTO';
import { CourseRealization } from '../../models/courseRealization';

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<CourseViewDTO[]>(`${AppConstants.BASE_URL}subjects/courses`)
  }

  addCourse(courseRealization: CourseRealization) {
    return this.http.post(`${AppConstants.BASE_URL}course-realizations`, courseRealization)
  }

  editCourse(courseRealization: CourseRealization) {
    console.log(`dsafds fdsfds ${courseRealization}`)
    console.log(courseRealization)
    return this.http.put(`${AppConstants.BASE_URL}course-realizations/${courseRealization.id}`, courseRealization)
  }
}
