import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { YearOfStudy } from '../../models/yearOfStudy';
import { AppConstants } from '../../constants';

@Injectable({
  providedIn: 'root'
})
export class YearOfStudyService {

  constructor(private http: HttpClient) {}

  getAllYearsOfStudy(): Observable<YearOfStudy[]> {
    return this.http.get<YearOfStudy[]>(AppConstants.buildUrl('yearsOfStudy'));
  }

  getYearOfStudyById(id: number): Observable<YearOfStudy> {
    return this.http.get<YearOfStudy>(`${AppConstants.buildUrl('yearsOfStudy')}/${id}`);
  }

  createYearOfStudy(yearOfStudy: YearOfStudy): Observable<YearOfStudy> {
    return this.http.post<YearOfStudy>(AppConstants.buildUrl('yearsOfStudy'), yearOfStudy);
  }

  updateYearOfStudy(id: number, yearOfStudy: YearOfStudy): Observable<YearOfStudy> {
    return this.http.put<YearOfStudy>(`${AppConstants.buildUrl('yearsOfStudy')}/${id}`, yearOfStudy);
  }

  deleteYearOfStudy(id: number): Observable<void> {
    return this.http.delete<void>(`${AppConstants.buildUrl('yearsOfStudy')}/${id}`);
  }
}
