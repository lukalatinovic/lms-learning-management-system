import { TestBed } from '@angular/core/testing';

import { TeachingSlotsService } from './teaching-slots.service';

describe('TeachingSlotsService', () => {
  let service: TeachingSlotsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TeachingSlotsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
