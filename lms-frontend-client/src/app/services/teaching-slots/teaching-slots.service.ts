import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { TeachingSlots } from '../../models/teachingSlots';
import { AppConstants } from '../../constants';

@Injectable({
  providedIn: 'root'
})
export class TeachingSlotsService {

  constructor(private http: HttpClient, private router: Router) { }

  fetchSlots(teacherId: number) {
    return this.http.get<TeachingSlots[]>(`${AppConstants.BASE_URL}teaching-slots/teacher/${teacherId}`)
  }

  addSlot(teachingSlots: any) {
    return this.http.post(`${AppConstants.BASE_URL}teaching-slots`, {
      "startTime": teachingSlots["startTime"],
      "endTime": teachingSlots["endTime"],
      "teachingType": {
        "id": teachingSlots["teachingType"]
      },
      "outcome": null,
      "courseRealization": {
        "id": teachingSlots["courseRealization"]
      }
    })
  }
}
