import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TeachingSlots } from '../../models/teachingSlots';
import { AppConstants } from '../../constants';

@Injectable({
  providedIn: 'root'
})
export class OutcomeService {

  constructor(private http: HttpClient) { }

  addOutcomeToSlot(teachingSlot: TeachingSlots) {
    return this.http.put(`${AppConstants.BASE_URL}teaching-slots/teacher/${teachingSlot.id}`, teachingSlot)
  }
}
