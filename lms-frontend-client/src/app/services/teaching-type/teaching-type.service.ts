import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppConstants } from '../../constants';
import { TeachingType } from '../../models/teachingType';

@Injectable({
  providedIn: 'root'
})
export class TeachingTypeService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<TeachingType[]>(`${AppConstants.BASE_URL}teaching-types`)
  }
}
