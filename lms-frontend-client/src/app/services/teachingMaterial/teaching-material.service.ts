import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TeachingMaterials } from '../../models/teachingMaterials';
import { AppConstants } from '../../constants';

@Injectable({
  providedIn: 'root'
})
export class TeachingMaterialsService {
  constructor(private http: HttpClient) {}

  getTeachingMaterials(): Observable<TeachingMaterials[]> {
    return this.http.get<TeachingMaterials[]>(AppConstants.buildUrl('teaching-materials'));
  }

  getTeachingMaterial(id: number): Observable<TeachingMaterials> {
    return this.http.get<TeachingMaterials>(AppConstants.buildUrl(`teaching-materials/${id}`));
  }

  createTeachingMaterial(teachingMaterial: TeachingMaterials): Observable<TeachingMaterials> {
    return this.http.post<TeachingMaterials>(AppConstants.buildUrl('teaching-materials'), teachingMaterial);
  }

  updateTeachingMaterial(id: number, teachingMaterial: TeachingMaterials): Observable<TeachingMaterials> {
    return this.http.put<TeachingMaterials>(AppConstants.buildUrl(`teaching-materials/${id}`), teachingMaterial);
  }

  deleteTeachingMaterial(id: number): Observable<void> {
    return this.http.delete<void>(AppConstants.buildUrl(`teaching-materials/${id}`));
  }
}
