import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import * as jwt_decode from "jwt-decode";

import { User } from '../../models/user';
import { AppConstants } from '../../constants';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {

    private roles: string[] = [];
    isAuthenticated = new BehaviorSubject<boolean>(false);    

    constructor(private http: HttpClient) {
        if (typeof localStorage !== 'undefined') {
            this.isAuthenticated.next(!!(localStorage.getItem("token") ?? null));
            const token = localStorage.getItem("token");
            if (token) {
                this.setRolesFromToken(token);
            }
        }
    }

    getUserId() {
        return this.getDecodedAccessToken(localStorage.getItem("token") ?? '')["id"];
    }

    hasAnyRole(roles: string[]): boolean {
        return roles.some(role => this.roles.includes(role));
    }

    setRolesFromToken(token: string): void {
        const decodedToken: any = jwt_decode.jwtDecode(token);
        this.roles = decodedToken.roles || [];
    }

    getDecodedAccessToken(token: string): any {
        try {
            return jwt_decode.jwtDecode(token);
        } catch(Error) {
            console.error(Error);
            return null;
        }
    }

    register(user: User) {
        console.log(user);
        return this.http.post<any>(`${AppConstants.BASE_URL}auth/signup`, user).pipe(map(token => {
            console.log(token);
            localStorage.setItem('token', token['accessToken']);

            let tokenData = this.getDecodedAccessToken(token['accessToken']);
            this.roles = tokenData.roles;

            this.isAuthenticated.next(true);

            return token;
        }));
    }

    registerWithoutLogin(user: User) {
        return this.http.post<any>(`${AppConstants.BASE_URL}auth/signup`, user);
    }
    
    login(email: string, password: string) {
        return this.http.post<any>(`${AppConstants.BASE_URL}auth/signin`, {
            "username": email,
            "password": password
        })
        .pipe(map(token => {
            console.log(token);

            let tokenData = this.getDecodedAccessToken(token['accessToken']);
            this.roles = tokenData.roles;

            console.log(tokenData);

            this.isAuthenticated.next(true);

            localStorage.setItem('token', token['accessToken']);
        
            return token;
        }));
    }
    
    logout() {
        localStorage.removeItem('token');
        this.isAuthenticated.next(false);
        this.roles = [];
    }
}
