import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { University } from '../../models/university';
import { AppConstants } from '../../constants';

@Injectable({
  providedIn: 'root',
})
export class UniversityService {
  constructor(private http: HttpClient) {}

  getUniversity(id: number): Observable<University> {
    return this.http.get<University>(AppConstants.buildUrl(`universities/${id}`));
  }

  getAllUniversities(): Observable<University[]> {
    return this.http.get<University[]>(AppConstants.buildUrl('universities'));
  }

  createUniversity(university: University): Observable<University> {
    return this.http.post<University>(AppConstants.buildUrl('universities'), university);
  }

  updateUniversity(id: number, university: University): Observable<University> {
    return this.http.put<University>(AppConstants.buildUrl(`universities/${id}`), university);
  }

  deleteUniversity(id: number): Observable<void> {
    return this.http.delete<void>(AppConstants.buildUrl(`universities/${id}`));
  }
}
