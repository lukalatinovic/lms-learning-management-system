import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppConstants } from '../../constants';
import { TitleType } from '../../models/titleType';

@Injectable({
  providedIn: 'root'
})
export class TitleTypeService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<TitleType[]>(`${AppConstants.BASE_URL}titleTypes`)
  }
}
