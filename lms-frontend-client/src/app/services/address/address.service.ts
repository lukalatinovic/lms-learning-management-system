import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Country } from '../../models/country';
import { PlaceOfResidence } from '../../models/placeOfResidence';
import { Address } from '../../models/address';
import { AppConstants } from '../../constants';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  constructor(private http: HttpClient) {}

  getCountries(): Observable<Country[]> {
    return this.http.get<Country[]>(AppConstants.buildUrl('countries'));
  }

  getPlaces(): Observable<PlaceOfResidence[]> {
    return this.http.get<PlaceOfResidence[]>(AppConstants.buildUrl('placesOfResidence'));
  }

  getPlacesByCountryId(countryId: number): Observable<PlaceOfResidence[]> {
    return this.http.get<PlaceOfResidence[]>(AppConstants.buildUrl(`placesOfResidence/country/${countryId}`));
  }

  createAddress(address: Address): Observable<Address> {
    return this.http.post<Address>(AppConstants.buildUrl('addresses'), address);
  }

  findAddressByDetails(street: string, number: string, placeOfResidenceId: number): Observable<Address | null> {
    return this.http.get<Address | null>(AppConstants.buildUrl(`addresses/find?street=${street}&number=${number}&placeOfResidenceId=${placeOfResidenceId}`));
  }
}
