import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ScienceField } from '../../models/scienceField';
import { AppConstants } from '../../constants';

@Injectable({
  providedIn: 'root'
})
export class ScienceFieldService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<ScienceField[]>(`${AppConstants.BASE_URL}scienceFields`)
  }
}
