import { TestBed } from '@angular/core/testing';

import { ScienceFieldService } from './science-field.service';

describe('ScienceFieldService', () => {
  let service: ScienceFieldService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ScienceFieldService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
