import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { StudentOnYear } from '../../models/studentOnYear';
import { AppConstants } from '../../constants';

@Injectable({
  providedIn: 'root'
})
export class StudentOnYearService {

  constructor(private http: HttpClient) {}

  getAllStudentsOnYear(): Observable<StudentOnYear[]> {
    return this.http.get<StudentOnYear[]>(AppConstants.buildUrl('studentsOnYear'));
  }

  getStudentOnYearById(id: number): Observable<StudentOnYear> {
    return this.http.get<StudentOnYear>(`${AppConstants.buildUrl('studentsOnYear')}/${id}`);
  }

  createStudentOnYear(studentOnYear: StudentOnYear): Observable<StudentOnYear> {
    return this.http.post<StudentOnYear>(AppConstants.buildUrl('studentsOnYear'), studentOnYear);
  }

  updateStudentOnYear(id: number, studentOnYear: StudentOnYear): Observable<StudentOnYear> {
    return this.http.put<StudentOnYear>(`${AppConstants.buildUrl('studentsOnYear')}/${id}`, studentOnYear);
  }

  deleteStudentOnYear(id: number): Observable<void> {
    return this.http.delete<void>(`${AppConstants.buildUrl('studentsOnYear')}/${id}`);
  }
}
