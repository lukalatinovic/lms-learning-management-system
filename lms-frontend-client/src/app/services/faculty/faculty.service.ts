import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Faculty } from '../../models/faculty';
import { AppConstants } from '../../constants';

@Injectable({
  providedIn: 'root'
})
export class FacultyService {
  constructor(private http: HttpClient) {}

  getFaculties(): Observable<Faculty[]> {
    return this.http.get<Faculty[]>(AppConstants.buildUrl('faculties'));
  }

  getFaculty(id: number): Observable<Faculty> {
    return this.http.get<Faculty>(AppConstants.buildUrl(`faculties/${id}`));
  }

  createFaculty(faculty: Faculty): Observable<Faculty> {
    return this.http.post<Faculty>(AppConstants.buildUrl('faculties'), faculty);
  }

  updateFaculty(id: number, faculty: Faculty): Observable<Faculty> {
    return this.http.put<Faculty>(AppConstants.buildUrl(`faculties/${id}`), faculty);
  }

  deleteFaculty(id: number): Observable<void> {
    return this.http.delete<void>(AppConstants.buildUrl(`faculties/${id}`));
  }
}
