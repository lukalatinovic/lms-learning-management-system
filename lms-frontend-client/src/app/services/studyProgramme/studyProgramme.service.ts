import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { StudyProgramme } from '../../models/StudyProgramme';
import { AppConstants } from '../../constants';

@Injectable({
  providedIn: 'root'
})
export class StudyProgrammeService {

  constructor(private http: HttpClient) {}

  getAllStudyProgrammes(): Observable<StudyProgramme[]> {
    return this.http.get<StudyProgramme[]>(AppConstants.buildUrl('studyProgrammes'));
  }

  getStudyProgrammeById(id: number): Observable<StudyProgramme> {
    return this.http.get<StudyProgramme>(`${AppConstants.buildUrl('studyProgrammes')}/${id}`);
  }

  createStudyProgramme(studyProgramme: StudyProgramme): Observable<StudyProgramme> {
    return this.http.post<StudyProgramme>(AppConstants.buildUrl('studyProgrammes'), studyProgramme);
  }

  updateStudyProgramme(id: number, studyProgramme: StudyProgramme): Observable<StudyProgramme> {
    return this.http.put<StudyProgramme>(`${AppConstants.buildUrl('studyProgrammes')}/${id}`, studyProgramme);
  }

  deleteStudyProgramme(id: number): Observable<void> {
    return this.http.delete<void>(`${AppConstants.buildUrl('studyProgrammes')}/${id}`);
  }
}
