import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from '../../models/subject';
import { AppConstants } from '../../constants';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {

  constructor(private http: HttpClient) { }

  getSubjects(): Observable<Subject[]> {
    return this.http.get<Subject[]>(AppConstants.buildUrl('subjects'));
  }

  fetchSubjectsByTeacherId(teacherId: number): Observable<Subject[]> {
    return this.http.get<Subject[]>(`${AppConstants.BASE_URL}subjects/teacher/${teacherId}`);
  }
}
