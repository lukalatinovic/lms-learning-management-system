import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Teacher } from '../../models/teacher';
import { AppComponent } from '../../app.component';
import { AppConstants } from '../../constants';
import { ProfessorsAssistantsDTO } from '../../models/professorsAssistantsDTO';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {

  constructor(private http: HttpClient) { }

  getProfessorsAndAssistants() {
    return this.http.get<ProfessorsAssistantsDTO>(`${AppConstants.BASE_URL}teachers/professorsAndAssistents`);
  }

  addTeacher(teacher: any) {
    return this.http.post(`${AppConstants.BASE_URL}teachers`, {
      "user": {
        "email": teacher["email"],
        "username": teacher["username"],
        "password": teacher["password"],
        "dateOfBirth": teacher["dateOfBirth"]
      },
      "jmbg": teacher["jmbg"],
      "firstName": teacher["firstName"],
      "lastName": teacher["lastName"],
      "biography": teacher["biography"],
      "title": {
          "electedDate": teacher["electedDate"],
          "expireDate": teacher["expireDate"],
          "scienceField": {
            "id": teacher["scienceField"]
          },
          "titleType": {
            "id": teacher["title"]
          }
      }
    })
  }
}
