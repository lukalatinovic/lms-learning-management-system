import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../../models/user';
import { AppConstants } from '../../constants';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
 
  getUsers() {
    return this.http.get<User[]>(AppConstants.buildUrl('users'));
  }

  changeRole(user: User) {
    return this.http.put<User>(`${AppConstants.BASE_URL}users/${user.id}`, user);
  }

  addStudentService(user: User) {
    return this.http.post(`${AppConstants.BASE_URL}users/studentService`, user)
  }
}
