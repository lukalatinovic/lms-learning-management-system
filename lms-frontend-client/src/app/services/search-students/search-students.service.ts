import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppConstants } from '../../constants';
import { AuthenticationService } from '../authentication/authentication.service';
import { StudentDTO } from '../../models/studentDTO';

@Injectable({
  providedIn: 'root'
})
export class SearchStudentsService {

  constructor(private http: HttpClient, private authService: AuthenticationService) { }

  search(student: StudentDTO) {
    return this.http.post<StudentDTO[]>(`${AppConstants.BASE_URL}students/search/${this.authService.getUserId()}`, student)
  }
}
