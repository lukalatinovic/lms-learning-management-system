import { NgFor, NgIf } from '@angular/common';
import { Component } from '@angular/core';
import { SubjectService } from '../../services/subject/subject.service';
import { Subject } from '../../models/subject';
import { ActivatedRoute, Router, RouterOutlet } from '@angular/router';
import { routes } from '../../app.routes';
import { AuthenticationService } from '../../services/authentication/authentication.service';

@Component({
  selector: 'app-subjects-table',
  standalone: true,
  imports: [NgFor, NgIf, RouterOutlet],
  templateUrl: './subjects-table.component.html',
  styleUrl: './subjects-table.component.css'
})
export class SubjectsTableComponent {

  subjects: Subject[]

  constructor(private subjectService: SubjectService, private authService: AuthenticationService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getSubjects()
  }

  getSubjects() {
    this.subjectService.fetchSubjectsByTeacherId(this.authService.getUserId()).subscribe(r => {
      this.subjects = r;
      console.log(r)
    })
  }

  viewStudents() {
    // You can navigate to a students page with the subject information
    this.router.navigate(['../students-by-subject'], { relativeTo: this.route });
  }
}
