import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { Teacher } from '../../models/teacher';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { NgFor, NgForOf, NgIf } from '@angular/common';
import { Subject } from '../../models/subject';

@Component({
  selector: 'app-edit-teacher-dialog',
  standalone: true,
  imports: [MatFormFieldModule, MatSelectModule, FormsModule, MatButtonModule, NgFor, NgForOf, NgIf],
  templateUrl: './edit-teacher-dialog.component.html',
  styleUrl: './edit-teacher-dialog.component.css'
})
export class EditTeacherDialogComponent {

  professors: Teacher[]
  assistants: Teacher[]
  selectedTeacher: Teacher;
  

  constructor(
    public dialogRef: MatDialogRef<EditTeacherDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.professors = data.professors;
    this.assistants = data.assistants;
    this.selectedTeacher = data.selectedTeacher;
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onSave(): void {
    this.dialogRef.close(this.selectedTeacher);
  }
}
