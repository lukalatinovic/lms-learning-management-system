import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router, RouterModule } from '@angular/router';
 
import { UserService } from '../../services/user/user.service';
import { NgClass, NgFor, NgIf } from '@angular/common';
import { AuthenticationService } from '../../services/authentication/authentication.service';
@Component({
selector: 'app-register',
standalone: true,
imports: [ReactiveFormsModule, NgIf, NgFor, NgClass, RouterModule],
templateUrl: './register.component.html',
styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit {
 
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthenticationService,
  ) { }

  registerForm: FormGroup = new FormGroup({});
  loading = false;
  submitted = false;
 
  ngOnInit() {
    this.registerForm = this.formBuilder.group({
    username: ['', Validators.required],
    email: ['', [Validators.required,Validators.email]],
    password: ['', [Validators.required, Validators.minLength(6)]],
    dateOfBirth: [''],
    });
  }
 
  get fval() { return this.registerForm.controls; }
 
  onFormSubmit(){
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    this.loading = true;

    this.authService.register(this.registerForm.value).subscribe(r => this.router.navigate(['/']));

    this.loading = false
    this.submitted = false
  }
 
}
