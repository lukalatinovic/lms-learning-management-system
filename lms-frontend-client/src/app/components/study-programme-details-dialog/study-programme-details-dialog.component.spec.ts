import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudyProgrammeDetailsDialogComponent } from './study-programme-details-dialog.component';

describe('StudyProgrammeDetailsDialogComponent', () => {
  let component: StudyProgrammeDetailsDialogComponent;
  let fixture: ComponentFixture<StudyProgrammeDetailsDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StudyProgrammeDetailsDialogComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(StudyProgrammeDetailsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
