import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { StudyProgramme } from '../../models/StudyProgramme';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-study-programme-details-dialog',
  templateUrl: './study-programme-details-dialog.component.html',
  styleUrls: ['./study-programme-details-dialog.component.css'],
  standalone: true,
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
  ]
})
export class StudyProgrammeDetailsDialogComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: StudyProgramme) {}

  getYear(date: Date | string): string {
    if (typeof date === 'string') {
      date = new Date(date);
    }
    return date.getFullYear().toString();
  }
}
