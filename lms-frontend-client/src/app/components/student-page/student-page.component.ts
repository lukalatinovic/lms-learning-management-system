import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog } from '@angular/material/dialog'; // Import MatDialog
import { StudentFormComponent } from '../student-form/student-form.component';
import { StudentTableComponent } from '../student-table/student-table.component';
import { Student } from '../../models/student';
import { StudentService } from '../../services/student/student.service';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component'; // Import the ConfirmationDialogComponent

@Component({
  selector: 'app-student-page',
  templateUrl: './student-page.component.html',
  styleUrls: ['./student-page.component.css'],
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatButtonModule,
    StudentFormComponent,
    StudentTableComponent
  ]
})
export class StudentPageComponent implements OnInit, OnChanges {
  formOpen = false;
  selectedStudent: Student | null = null;
  selectedStudents: Student[] = [];
  allStudents: Student[] = [];
  searchValue: string = '';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private studentService: StudentService,
    private dialog: MatDialog // Inject MatDialog
  ) {}

  ngOnInit(): void {
    console.log('StudentPageComponent initialized');
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('Changes detected:', changes);
  }

  toggleForm(): void {
    if (this.formOpen && this.selectedStudents.length !== 1) {
      this.resetForm();
    }
    this.formOpen = !this.formOpen;
  }

  isStudentSelected(): boolean {
    return this.selectedStudents.length > 0;
  }

  onStudentSelected(students: Student[]): void {
    this.selectedStudents = students;
    if (students.length === 1) {
      this.selectedStudent = students[0];
    } else {
      this.selectedStudent = null;
      this.resetForm();
    }
  }

  onAllStudentsLoaded(students: Student[]): void {
    this.allStudents = students;
  }

  resetForm(): void {
    this.selectedStudent = null;
  }

  editStudent(): void {
    if (this.selectedStudent) {
      this.toggleForm();
    }
  }

  deleteStudents(): void {
    if (this.selectedStudents.length > 0) {
        const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
            width: '300px',
            data: { message: 'Are you sure you want to delete the selected student(s)?' }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                const studentIds: number[] = this.selectedStudents.map(student => student.id); // Osiguranje da je ID broj
                this.studentService.deleteStudents(studentIds).subscribe(
                    () => {
                        this.reloadPage(); // Refresh the page after deletion
                    },
                    error => {
                        console.error('Failed to delete students', error);
                    }
                );
            }
        });
    }
  }

  private reloadPage(): void {
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([this.route.snapshot.url.join('/')]);
    });
  }

  getDeleteButtonText(): string {
    const numSelected = this.selectedStudents.length;
    const numTotal = this.allStudents.length;

    if (numSelected === 1) {
      return 'Delete 1 student';
    } else if (numSelected === numTotal) {
      return 'Delete all students';
    } else {
      return `Delete ${numSelected} students`;
    }
  }

  onSearchChange(event: Event): void {
    const inputElement = event.target as HTMLInputElement;
    this.searchValue = inputElement.value;
  }

  handleStudentCreated(): void {
    this.reloadPage(); // Refresh the page after student creation
  }

  private loadStudents(): void {
    this.studentService.getStudents().subscribe(students => {
      this.allStudents = students;
      this.selectedStudents = [];
    });
  }
}
