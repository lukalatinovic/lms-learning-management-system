import { Component } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { HttpClient } from '@angular/common/http';
import { CommonModule, NgFor, NgIf } from '@angular/common';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatTableModule } from '@angular/material/table';
import { HttpClientModule } from '@angular/common/http';
import { TeachingSlotsService } from '../../services/teaching-slots/teaching-slots.service';
import { TeachingSlots } from '../../models/teachingSlots';
import { OutcomeService } from '../../services/outcome/outcome.service';
import { Outcome } from '../../models/outcome';
import { AuthenticationService } from '../../services/authentication/authentication.service';

@Component({
  selector: 'app-teaching-slots',
  standalone: true,
  imports: [NgFor, NgIf, CommonModule, MatTableModule],
  templateUrl: './teaching-slots.component.html',
  styleUrl: './teaching-slots.component.css'
})
export class TeachingSlotsComponent {

  constructor(private teachingSlotsService: TeachingSlotsService, private authService: AuthenticationService, private outcomeService: OutcomeService) { }

  getSlots() {
    console.log(this.authService.getUserId())
    this.teachingSlotsService.fetchSlots(this.authService.getUserId()).subscribe(r => {
      console.log(r);
      this.teachingSlots = r
    })
  }

  ngOnInit(): void {
    this.getSlots()
  }

  displayedColumns: string[] = ['startTime', 'endTime', 'outcome', 'actions'];
  teachingSlots: TeachingSlots[]

  addOutcome(slot: TeachingSlots) {
    // Logic to add an outcome to the slot
    const newOutcome = prompt('Enter new outcome:');

    if (newOutcome) {
      let outcome: Outcome = {
        description: newOutcome ?? ''
      }

      slot.outcome = outcome
      this.outcomeService.addOutcomeToSlot(slot).subscribe(r => console.log(r))
    }
  }

}
