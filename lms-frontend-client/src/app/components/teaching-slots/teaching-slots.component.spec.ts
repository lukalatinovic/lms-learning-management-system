import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeachingSlotsComponent } from './teaching-slots.component';

describe('TeachingSlotsComponent', () => {
  let component: TeachingSlotsComponent;
  let fixture: ComponentFixture<TeachingSlotsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TeachingSlotsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TeachingSlotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
