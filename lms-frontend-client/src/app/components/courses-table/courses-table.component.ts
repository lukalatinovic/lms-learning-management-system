import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CourseService } from '../../services/course/course.service';
import { NgFor, NgIf } from '@angular/common';
import { CourseViewDTO } from '../../models/courseViewDTO';
import { MatDialog } from '@angular/material/dialog';
import { TeacherService } from '../../services/teacher/teacher.service';
import { TeacherSelectDialogComponent } from '../teacher-select-dialog/teacher-select-dialog.component';
import { Teacher } from '../../models/teacher';
import { MatButtonModule } from '@angular/material/button';
import { Subject } from '../../models/subject';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { EditTeacherDialogComponent } from '../edit-teacher-dialog/edit-teacher-dialog.component';


@Component({
  selector: 'app-courses-table',
  standalone: true,
  imports: [NgFor, NgIf, MatButtonModule, MatProgressSpinnerModule],
  templateUrl: './courses-table.component.html',
  styleUrl: './courses-table.component.css'
})
export class CoursesTableComponent {

  courses: CourseViewDTO[]
  professors: Teacher[]
  assistants: Teacher[]

  showSpiner: boolean = false;

  constructor(private router: Router, private courseService: CourseService, 
    public dialog: MatDialog, private teacherService: TeacherService) {}

  openTeacherSelectDialog(subject: Subject): void {
    const dialogRef = this.dialog.open(TeacherSelectDialogComponent, {
      width: '300px',
      data: {
        professors: this.professors,
        assistants: this.assistants,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.showSpiner = true
        console.log(result)
        console.log(result.assistant);
        this.courseService.addCourse({
          subject: subject,
          teacher: result.professor
        }).subscribe(r => console.log(r))
        this.courseService.addCourse({
          subject: subject,
          teacher: result.assistant
        }).subscribe(r => console.log(r))

        location.reload();
      }
    });
  }

  editTeacher(course: CourseViewDTO) {
    const dialogRef = this.dialog.open(EditTeacherDialogComponent, {
      width: '300px',
      data: {
        professors: this.professors,
        assistants: this.assistants,
        selectedTeacher: course.teacher
      }
    });

    dialogRef.afterClosed().subscribe(result => { 
      console.log(result)
      this.courseService.editCourse({
        id: course.courseRealizationId,
        subject: course.subject,
        teacher: result,
      }).subscribe(r => console.log(r));

      location.reload();

    } )

  }

  ngOnInit(): void {

    this.showSpiner = true
    
    this.courseService.getAll().subscribe(r=> {
      console.log(r)
      this.courses = r
      this.showSpiner = false;
    })

    this.teacherService.getProfessorsAndAssistants().subscribe(r => {
      console.log(r)
      this.professors = r.professors;
      this.assistants = r.assistants;
    })
    
  }
}
