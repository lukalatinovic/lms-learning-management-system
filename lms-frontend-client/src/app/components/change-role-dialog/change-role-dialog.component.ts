import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import { NgFor } from '@angular/common';
import { Role } from '../../models/role';

@Component({
  selector: 'app-change-role-dialog',
  standalone: true,
  imports: [MatButtonModule, MatInputModule, MatSelectModule, NgFor],
  templateUrl: './change-role-dialog.component.html',
  styleUrl: './change-role-dialog.component.css'
})
export class ChangeRoleDialogComponent {
  roles: Role[] = this.data.roles;
  selectedRole: Role = this.data.currentRole;

  constructor(
    public dialogRef: MatDialogRef<ChangeRoleDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { currentRole: Role, roles: Role[] }
  ) {
    // Ensure the current role is the first in the list
    this.roles = this.roles.filter(role => role.id !== this.data.currentRole.id);
    this.roles.unshift(this.data.currentRole);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onSave(): void {
    this.dialogRef.close(this.selectedRole);
  }
}
