import { NgFor, NgIf } from '@angular/common';
import { Component } from '@angular/core';
import { StudentService } from '../../services/student/student.service';
import { Router } from '@angular/router';
import { Student } from '../../models/student';
import { StudentDTO } from '../../models/studentDTO';

@Component({
  selector: 'app-students-by-subject',
  standalone: true,
  imports: [NgFor, NgIf],
  templateUrl: './students-by-subject.component.html',
  styleUrl: './students-by-subject.component.css'
})
export class StudentsBySubjectComponent {

  constructor(private studentService: StudentService, private router: Router) {}

  students: StudentDTO []

  ngOnInit(): void {
    this.studentService.getStudentsByTeacherId().subscribe(r => {
      this.students = r
      console.log(r)})
  }
}
