import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsBySubjectComponent } from './students-by-subject.component';

describe('StudentsBySubjectComponent', () => {
  let component: StudentsBySubjectComponent;
  let fixture: ComponentFixture<StudentsBySubjectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StudentsBySubjectComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(StudentsBySubjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
