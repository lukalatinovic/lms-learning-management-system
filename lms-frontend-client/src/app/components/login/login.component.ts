import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, RouterModule } from '@angular/router';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
 
import { AuthenticationService } from '../../services/authentication/authentication.service';
import { NgClass, NgFor, NgIf } from '@angular/common';
 
@Component({
selector: 'app-login',
standalone: true,
imports: [ReactiveFormsModule, NgIf, NgFor, NgClass, RouterModule],
templateUrl: './login.component.html',
styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup = new FormGroup({});
  loading = false;
  submitted = false;
  returnUrl: string = '';
  
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService : AuthenticationService,
  ) { }
  
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
    });
  
  }
  
  // for accessing to form fields
  get fval() { return this.loginForm.controls; }
  
  onFormSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
  
    this.loading = true;
    this.authenticationService.login(this.fval['username'].value, this.fval['password'].value)
    .subscribe(
      data => {
        console.log(data)
        this.router.navigate(['/']);
        return;
      });
      this.loading = false
      this.submitted = false
  }
}