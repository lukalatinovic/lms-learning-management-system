// components/teaching-materials/teaching-materials.component.ts
import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeachingMaterialsService } from '../../services/teachingMaterial/teaching-material.service';
import { TeachingMaterials } from '../../models/teachingMaterials';

@Component({
  selector: 'app-teaching-materials',
  templateUrl: './teaching-materials.component.html',
  styleUrls: ['./teaching-materials.component.css'],
  standalone: true,
  imports: [CommonModule]
})
export class TeachingMaterialsComponent implements OnInit {
  teachingMaterials: TeachingMaterials[] = [];

  constructor(private teachingMaterialsService: TeachingMaterialsService) {}

  ngOnInit(): void {
    this.loadTeachingMaterials();
  }

  loadTeachingMaterials() {
    this.teachingMaterialsService.getTeachingMaterials().subscribe(data => {
      this.teachingMaterials = data;
    });
  }
}
