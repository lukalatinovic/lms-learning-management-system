import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { Student } from '../../models/student';
import { StudentOnYear } from '../../models/studentOnYear';
import { YearOfStudy } from '../../models/yearOfStudy';
import { StudyProgramme } from '../../models/StudyProgramme';
import { UserService } from '../../services/user/user.service';
import { AddressService } from '../../services/address/address.service';
import { YearOfStudyService } from '../../services/yearOfStudy/yearOfStudy.service';
import { StudyProgrammeService } from '../../services/studyProgramme/studyProgramme.service';
import { User } from '../../models/user';
import { Address } from '../../models/address';
import { Country } from '../../models/country';
import { PlaceOfResidence } from '../../models/placeOfResidence';
import { StudentService } from '../../services/student/student.service';
import { AuthenticationService } from '../../services/authentication/authentication.service';

@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css'],
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatButtonModule
  ]
})
export class StudentFormComponent implements OnInit, OnChanges {
  @Input() student: Student | null = null;
  @Input() isEditing: boolean = false;
  @Output() closeFormEvent = new EventEmitter<void>();
  @Output() studentCreated = new EventEmitter<Student>(); // Emitovanje novog studenta ili ažuriranog

  studentOnYear: StudentOnYear = {} as StudentOnYear;
  studentOnYearCopy: StudentOnYear = {} as StudentOnYear;
  users: User[] = [];
  filteredUsers: User[] = [];
  students: Student[] = [];
  countries: Country[] = [];
  placesOfResidence: PlaceOfResidence[] = [];
  filteredPlaces: PlaceOfResidence[] = [];
  selectedUser: string = '';
  selectedCountry: number = 0;
  selectedPlace: string = '';
  newUser: User = {} as User;
  newUserFormattedDateOfBirth: string = '';
  isCreatingNewUser: boolean = false;
  isExistingUser: boolean = false;

  jmbg: string = '';
  firstName: string = '';
  lastName: string = '';
  street: string = '';
  number: string = '';
  enrollDate: string = '';
  selectedYearOfStudy: number = 0;
  selectedStudyProgramme: number = 0;
  yearsOfStudy: YearOfStudy[] = [];
  studyProgrammes: StudyProgramme[] = [];

  initialStudent: Student | null = null;

  constructor(
    private userService: UserService,
    private addressService: AddressService,
    private yearOfStudyService: YearOfStudyService,
    private studyProgrammeService: StudyProgrammeService,
    private studentService: StudentService,
    private authService: AuthenticationService
  ) {}

  ngOnInit() {
    this.loadUsers();
    this.loadStudents();
    this.loadCountries();
    this.loadStudyProgrammes();
    this.loadYearsOfStudy();

    if (this.isEditing) {
        this.initializeStudentData();
    }
}

ngOnChanges(changes: SimpleChanges) {
  if (changes['student'] && this.student) {
      this.fillFormWithStudentData();
      this.initialStudent = { ...this.student };
  } else if (changes['student'] && this.student === null) {
      this.resetForm();
  }
  this.filterUsers();
}

initializeStudentData() {
  this.isCreatingNewUser = false; // Resetovanje stanja pri editovanju
  this.isExistingUser = true;
  if (this.student) {
    this.fillFormWithStudentData();
  } else {
    this.resetForm();
  }
}

  loadUsers() {
    this.userService.getUsers().subscribe(data => {
      this.users = data;
      this.filterUsers();
    });
  }

  loadStudents() {
    this.studentService.getStudents().subscribe(data => {
      this.students = data;
      this.filterUsers();
    });
  }

  filterUsers() {
    const studentUserIds = this.students.map(student => student.user.id);
    this.filteredUsers = this.users.filter(user => 
      user.roles.some(role => role.name === 'user') && !studentUserIds.includes(user.id)
    );
    console.log('Filtered users:', this.filteredUsers); // Dodati log za debagovanje
  }

  loadCountries() {
    this.addressService.getCountries().subscribe(data => {
      this.countries = data;
    });
  }

  loadYearsOfStudy() {
    this.yearOfStudyService.getAllYearsOfStudy().subscribe({
      next: (data) => {
        this.yearsOfStudy = data;
      },
      error: (err) => {
        console.error('Failed to load years of study:', err);
      }
    });
  }

  loadStudyProgrammes() {
    this.studyProgrammeService.getAllStudyProgrammes().subscribe(data => {
      this.studyProgrammes = data;
    });
  }

  formatDateForFrontend(date: Date): string {
    const year = date.getFullYear();
    const month = ('0' + (date.getMonth() + 1)).slice(-2);
    const day = ('0' + date.getDate()).slice(-2);
    return `${year}-${month}-${day}`;
}

  formatDateForBackend(dateString: string): Date {
    return new Date(dateString);
  }

  onUserSelectionChange(value: string) {
    if (value === 'new') {
        this.isCreatingNewUser = true;
        this.isExistingUser = false;
        this.newUser = {} as User;
        this.newUserFormattedDateOfBirth = '';
    } else {
        this.isCreatingNewUser = false;
        this.isExistingUser = true;
        const selectedUser = this.users.find(user => user.username === value);
        if (selectedUser) {
            this.newUser = { ...selectedUser };
            this.newUserFormattedDateOfBirth = this.newUser.dateOfBirth ? this.formatDateForFrontend(new Date(this.newUser.dateOfBirth)) : '';
        }
    }
}

  onCountrySelectionChange(value: number) {
    this.selectedCountry = value;
    this.selectedPlace = ''; 
    this.filteredPlaces = []; 
    this.loadPlacesByCountryId(value);
  }

  onPlaceSelectionChange(value: string) {
    this.selectedPlace = value;
  }

  loadPlacesByCountryId(countryId: number) {
    this.addressService.getPlacesByCountryId(countryId).subscribe(data => {
      this.filteredPlaces = data;
    });
  }

  fillFormWithStudentData() {
    if (this.student) {
        // Popunjavanje podataka o studentu
        this.jmbg = this.student.jmbg || '';
        this.firstName = this.student.firstName || '';
        this.lastName = this.student.lastName || '';

        // Popunjavanje adrese
        if (this.student.address) {
            this.street = this.student.address.street || '';
            this.number = this.student.address.number || '';
            this.selectedPlace = this.student.address.placeOfResidence?.name || '';
            this.selectedCountry = this.student.address.placeOfResidence?.country?.id || 0;
            this.loadPlacesByCountryId(this.selectedCountry); // Učitaj mesta po državi
        }

        // Popunjavanje korisničkih podataka
        if (this.student.user) {
            this.newUser = { ...this.student.user };
            this.selectedUser = this.newUser.username || '';
            if (this.newUser.dateOfBirth) {
                this.newUserFormattedDateOfBirth = this.formatDateForFrontend(new Date(this.newUser.dateOfBirth));
            } else {
                this.newUserFormattedDateOfBirth = '';
            }
            this.isExistingUser = true;
        } else {
            this.newUser = {} as User;
            this.newUserFormattedDateOfBirth = '';
        }

        // Popunjavanje informacija o upisu
        if (this.student.studentOnYear && this.student.studentOnYear.length > 0) {
            const studentOnYear = this.student.studentOnYear[0];
            this.enrollDate = studentOnYear.dateOfEnroll 
                ? this.formatDateForFrontend(new Date(studentOnYear.dateOfEnroll)) 
                : '';
            this.selectedYearOfStudy = studentOnYear.yearOfStudy?.id || 0;
            this.selectedStudyProgramme = studentOnYear.yearOfStudy?.studyProgramme?.id || 0;
            this.studentOnYearCopy.indexNumber = studentOnYear.indexNumber || ''; // Dodaj ovo za popunjavanje indexNumber polja
        }

        // Filtriranje korisnika
        this.filterUsers(); 
    }
}

  updateStudent() {
    if (!this.student || !this.student.id) {
        console.error('No student selected for editing');
        return;
    }

    // Proveravamo da li se adresa promenila
    const initialAddress = this.student.address;
    const isAddressChanged = 
        initialAddress?.street !== this.street || 
        initialAddress?.number !== this.number || 
        initialAddress?.placeOfResidence?.name !== this.selectedPlace || 
        initialAddress?.placeOfResidence?.country?.id !== this.selectedCountry;

    let addressToUse = initialAddress;

    if (isAddressChanged) {
        // Pronalaženje izabrane države
        const selectedCountry = this.countries.find(country => country.id === this.selectedCountry);
        if (!selectedCountry) {
            console.error('Invalid country selection');
            return;
        }

        // Pronalaženje izabranog mesta boravka (grada) na osnovu države
        const selectedPlace = this.filteredPlaces.find(place => place.name === this.selectedPlace && place.country.id === selectedCountry.id);
        if (!selectedPlace || !selectedPlace.id) {
            console.error('Invalid place of residence selection');
            return;
        }

        // Provera da li već postoji adresa sa istim street, number i placeOfResidence
        this.addressService.findAddressByDetails(this.street, this.number, selectedPlace.id).subscribe(existingAddress => {
            if (existingAddress) {
                // Ako adresa već postoji, koristi je
                addressToUse = existingAddress;
            } else {
                // Ako ne postoji, kreiraj novu adresu
                addressToUse = {
                    street: this.street,
                    number: this.number,
                    placeOfResidence: selectedPlace
                };

                // Napravi novu adresu na backendu
                this.addressService.createAddress(addressToUse).subscribe(newAddress => {
                    this.updateStudentWithAddress(newAddress);
                }, error => {
                    console.error('Failed to create new address', error);
                });

                return; // Izlazimo iz trenutne logike jer ćemo nastaviti unutar createAddress metode
            }

            // Ako adresa već postoji, ažuriramo studenta sa postojećom adresom
            this.updateStudentWithAddress(addressToUse);
        }, error => {
            if (error.status === 404) {
                // Adresa nije pronađena, kreiramo novu
                const newAddress = {
                    street: this.street,
                    number: this.number,
                    placeOfResidence: selectedPlace
                };

                this.addressService.createAddress(newAddress).subscribe(createdAddress => {
                    this.updateStudentWithAddress(createdAddress);
                }, createError => {
                    console.error('Failed to create address', createError);
                });
            } else {
                console.error('Failed to fetch address details', error);
            }
        });
    } else {
        // Ako se adresa nije promenila, koristi staru adresu
        this.updateStudentWithAddress(addressToUse!);
    }
}

updateStudentWithAddress(addressToUse: Address) {
  if (!addressToUse || !addressToUse.placeOfResidence || !addressToUse.placeOfResidence.country) {
      console.error('Address or PlaceOfResidence is not valid');
      return;
  }

  if (!addressToUse.street || addressToUse.street.trim() === '' || !addressToUse.number || addressToUse.number.trim() === '') {
      console.error('Street and Number cannot be empty');
      alert('Street and Number fields cannot be empty. Please fill in these details.');
      return;
  }

  const updatedStudent: Student = {
      id: this.student!.id!,
      jmbg: this.jmbg,
      firstName: this.firstName,
      lastName: this.lastName,
      address: {
          ...addressToUse,
          placeOfResidence: {
              ...addressToUse.placeOfResidence,
              country: addressToUse.placeOfResidence.country || { id: this.selectedCountry, name: this.selectedCountry }
          }
      },
      user: this.student!.user!,
      studentOnYear: this.student!.studentOnYear,
      formattedAddress: `${addressToUse.placeOfResidence.country.name}, ${addressToUse.placeOfResidence.name}, ${addressToUse.street} ${addressToUse.number}`
  };

  // Slanje ažuriranog studenta na backend
  this.studentService.updateStudent(this.student!.id!, updatedStudent).subscribe(
      response => {
          console.log('Student updated successfully', response);
          this.studentCreated.emit(response);
          this.resetForm();
          this.closeFormEvent.emit();
      },
      error => {
          console.error('Failed to update student', error);
      }
  );
}

onSubmit() {
  if (this.newUserFormattedDateOfBirth) {
      this.newUser.dateOfBirth = this.formatDateForBackend(this.newUserFormattedDateOfBirth);
  } else {
      this.newUser.dateOfBirth = null;
  }

  if (this.isEditing) {
      this.updateStudent();
  } else {
      if (this.isCreatingNewUser) {
          const signUpData: User = {
              username: this.newUser.username,
              password: this.newUser.password,
              email: this.newUser.email,
              dateOfBirth: this.newUser.dateOfBirth,
              roles: [{ id: 3, name: 'student' }]
          };

          this.authService.registerWithoutLogin(signUpData).subscribe(
              newUserResponse => {
                  const tokenData = this.authService.getDecodedAccessToken(newUserResponse.accessToken);
                  this.newUser.id = tokenData.id;

                  this.createAddressAndStudent();
              },
              error => {
                  console.error('Failed to create new user', error);
              }
          );
      } else {
          this.createAddressAndStudent();
      }
  }
}

  createAddressAndStudent() {
    const selectedCountry = this.countries.find(country => country.id === this.selectedCountry);
    const selectedPlace = this.filteredPlaces.find(place => place.name === this.selectedPlace);
  
    if (!selectedCountry) {
        console.error('Invalid country selection');
        return;
    }
  
    if (!selectedPlace) {
        console.error('Invalid place of residence selection');
        return;
    }
  
    this.addressService.getPlacesByCountryId(selectedCountry.id!).subscribe(
        (places: PlaceOfResidence[]) => {
            const placeOfResidence = places.find(
                place => place.name === this.selectedPlace && place.country.id === selectedCountry.id
            );
  
            if (!placeOfResidence || !placeOfResidence.id) {
                console.error('No matching place of residence found or placeOfResidence ID is missing');
                return;
            }
  
            const addressData: Address = {
                street: this.street,
                number: this.number,
                placeOfResidence: placeOfResidence
            };
  
            this.addressService.createAddress(addressData).subscribe(
                (createdAddress: Address) => {
                    this.createStudentWithAddress(createdAddress);
                },
                (error: any) => {
                    console.error('Failed to create address', error);
                }
            );
        },
        (error: any) => {
            console.error('Failed to fetch places by country', error);
        }
    );
  }
  
  createStudentWithAddress(createdAddress: Address) {
    if (!createdAddress || !createdAddress.placeOfResidence) {
        console.error('Place of residence is null');
        return;
    }
  
    if (!this.student) {
        console.error('Student object is null');
        return;
    }
  
    if (!this.newUser || !this.newUser.id) {
        console.error('User ID is missing');
        return;
    }
  
    if (!createdAddress.id) {
        console.error('Address ID is missing');
        return;
    }
  
    this.student.user = this.newUser;
    this.student.address = createdAddress;
  
    this.student.jmbg = this.jmbg;
    this.student.firstName = this.firstName;
    this.student.lastName = this.lastName;
  
    this.student.studentOnYear = [];  
  
    this.studentService.createStudent(this.student as Student).subscribe(
        response => {
            console.log('Student created successfully', response);
            this.studentCreated.emit(response);
            this.resetForm();
            this.closeFormEvent.emit();
        },
        error => {
            console.error('Failed to create student', error);
  
            if (error.status === 500 && error.error && error.error.detail && error.error.detail.includes('PlaceOfResidence.getCountry()')) {
                console.log('Student created but encountered known error');
                this.studentCreated.emit(this.student as Student);
                this.resetForm();
                this.closeFormEvent.emit();
            }
        }
    );
  }
  
  resetForm() {
      if (this.initialStudent && this.isEditing) {
        this.student = { ...this.initialStudent };
        this.fillFormWithStudentData();
      } else {
        this.student = {} as Student;
        this.studentOnYear = {} as StudentOnYear;
        this.studentOnYearCopy = {} as StudentOnYear;
        this.newUser = {} as User;
        this.selectedUser = '';
        this.isCreatingNewUser = false;
        this.isExistingUser = false;

        this.jmbg = '';
        this.firstName = '';
        this.lastName = '';
        this.street = '';
        this.number = '';
        this.selectedPlace = '';
        this.selectedCountry = 0;
        this.enrollDate = '';
        this.selectedYearOfStudy = 0;
        this.selectedStudyProgramme = 0;
        this.filteredPlaces = [];
      }
      this.filterUsers();
  }

  closeForm() {
    this.resetForm(); // Ovo resetuje sve varijable
    this.isCreatingNewUser = false; // Resetujemo isCreatingNewUser na false
    this.isExistingUser = false; // Resetujemo isExistingUser na false
    this.closeFormEvent.emit(); // Emitujemo event da je forma zatvorena
}
}
