import { Component } from '@angular/core';
import { UserService } from '../../services/user/user.service';
import { NgClass, NgFor, NgIf } from '@angular/common';
import { User } from '../../models/user';
import { map } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { ChangeRoleDialogComponent } from '../change-role-dialog/change-role-dialog.component';
import { Role } from '../../models/role';
import { RoleService } from '../../services/role/role.service';

@Component({
  selector: 'app-user-table',
  standalone: true,
  imports: [NgFor, NgClass, NgIf, ChangeRoleDialogComponent],
  templateUrl: './user-table.component.html',
  styleUrl: './user-table.component.css'
})
export class UserTableComponent {

  users: User []

  roles: Role[]
  
  constructor(private userService: UserService, public dialog: MatDialog, private roleService: RoleService) {
    
  }

  ngOnInit(): void {
     this.userService.getUsers().subscribe(r => {
      console.log(r)
      this.users = r
    })

    this.roleService.getAll().subscribe(r => {
      console.log(r)
      this.roles = r
    })
    
  }

  roleNames(username: String): String {
    return this.users.filter(u => u.username === username)[0].roles.map(r => r.name).join(', ')
  }
  
  openRoleDialog(user: User): void {
    const dialogRef = this.dialog.open(ChangeRoleDialogComponent, {
      width: '250px',
      data: { currentRole: user.roles[0], roles: this.roles }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log(`Role changed to: ${result.name}`);

        user.roles = [result];
        
        this.userService.changeRole(user).subscribe(r => {
          console.log(r)
        })
      }
    });
  }
}
