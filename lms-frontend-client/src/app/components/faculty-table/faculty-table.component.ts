import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FacultyService } from '../../services/faculty/faculty.service';
import { Faculty } from '../../models/faculty';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { StudyProgramme } from '../../models/StudyProgramme';
import { StudyProgrammeDetailsDialogComponent } from '../study-programme-details-dialog/study-programme-details-dialog.component';

@Component({
  selector: 'app-faculty-table',
  templateUrl: './faculty-table.component.html',
  styleUrls: ['./faculty-table.component.css'],
  standalone: true,
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
  ]
})
export class FacultyTableComponent implements OnInit {
  faculties: Faculty[] = [];

  constructor(private facultyService: FacultyService, private dialog: MatDialog) {}

  ngOnInit(): void {
    this.loadFaculties();
  }

  loadFaculties() {
    this.facultyService.getFaculties().subscribe(data => {
      this.faculties = data;
    });
  }

  openStudyProgrammeDetails(programme: StudyProgramme): void {
    this.dialog.open(StudyProgrammeDetailsDialogComponent, {
      data: programme,
      width: '400px'
    });
  }
}
