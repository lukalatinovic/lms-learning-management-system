import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { Teacher } from '../../models/teacher';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { NgFor, NgForOf } from '@angular/common';
import { Subject } from '../../models/subject';


@Component({
  selector: 'app-teacher-select-dialog',
  standalone: true,
  imports: [MatFormFieldModule, MatSelectModule, FormsModule, MatButtonModule, NgFor, NgForOf],
  templateUrl: './teacher-select-dialog.component.html',
  styleUrl: './teacher-select-dialog.component.css'
})
export class TeacherSelectDialogComponent {
  professors: Teacher[]
  assistants: Teacher[]
  selectedProfessor: Teacher;
  selectedAssistant: Teacher;

  constructor(
    public dialogRef: MatDialogRef<TeacherSelectDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.professors = data.professors;
    this.assistants = data.assistants;
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onSave(): void {
    const selectedTeachers = {
      professor: this.selectedProfessor,
      assistant: this.selectedAssistant
    };
    this.dialogRef.close(selectedTeachers);
  }
}
