import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherSelectDialogComponent } from './teacher-select-dialog.component';

describe('TeacherSelectDialogComponent', () => {
  let component: TeacherSelectDialogComponent;
  let fixture: ComponentFixture<TeacherSelectDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TeacherSelectDialogComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TeacherSelectDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
