import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { ScienceFieldService } from '../../services/science-field/science-field.service';
import { TitleTypeService } from '../../services/title-type/title-type.service';
import { TitleType } from '../../models/titleType';
import { ScienceField } from '../../models/scienceField';
import { NgFor } from '@angular/common';
import { TeacherService } from '../../services/teacher/teacher.service';
import { Teacher } from '../../models/teacher';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-teacher-form',
  standalone: true,
  imports: [FormsModule, ReactiveFormsModule, NgFor, MatSnackBarModule],
  templateUrl: './teacher-form.component.html',
  styleUrl: './teacher-form.component.css'
})
export class TeacherFormComponent {

  titleTypes: TitleType[]
  scienceFields: ScienceField[]

  constructor(private scinceFieldService: ScienceFieldService,
     private titleTypeService: TitleTypeService, private teacherService: TeacherService,
      private fb: FormBuilder, private router: Router,private snackBar: MatSnackBar) {}

  teacherForm: FormGroup

  ngOnInit(): void {
    
    this.titleTypeService.getAll().subscribe(r => {
      console.log(r)
      this.titleTypes = r
      this.teacherForm.controls['title'].setValue(r[0] || '');
    })
    this.scinceFieldService.getAll().subscribe(r => {
      console.log(r)
      this.scienceFields = r
      this.teacherForm.controls['scienceField'].setValue(r[0] || '');
    })

    this.teacherForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      username: ['', Validators.required],
      password: ['', Validators.required],
      dateOfBirth: [''],
      jmbg: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      biography: [''],
      title: ['', Validators.required],
      scienceField: ['', Validators.required],
      electedDate: ['', Validators.required],
      expireDate: ['', Validators.required]
    });
  }


  onSubmit() {
    if (this.teacherForm.valid) {
      console.log(this.teacherForm.value);
      let teacher = this.teacherForm.value

      console.log(teacher)

      this.teacherService.addTeacher(teacher).subscribe(r => {
        console.log(r)
        this.snackBar.open('Teacher added successfully!', 'Close', {
          duration: 3000,
        });
        this.router.navigate(["/"])
      })
    }
  }
}
