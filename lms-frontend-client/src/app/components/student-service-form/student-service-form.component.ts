import { NgFor } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { UserService } from '../../services/user/user.service';

@Component({
  selector: 'app-student-service-form',
  standalone: true,
  imports: [FormsModule, ReactiveFormsModule, NgFor, MatSnackBarModule],
  templateUrl: './student-service-form.component.html',
  styleUrl: './student-service-form.component.css'
})
export class StudentServiceFormComponent {
  constructor( private router: Router,private snackBar: MatSnackBar, private fb: FormBuilder, private userUservice: UserService) {}

 studentServiceForm: FormGroup

 ngOnInit(): void {
   this.studentServiceForm = this.fb.group({
     email: ['', [Validators.required, Validators.email]],
     username: ['', Validators.required],
     password: ['', Validators.required],
     dateOfBirth: [''],
   });
 }

 onSubmit() {
    if (this.studentServiceForm.valid) {
      this.userUservice.addStudentService(this.studentServiceForm.value).subscribe(r => {
        console.log(r)
        this.snackBar.open('Student service added successfully!', 'Close', {
          duration: 3000,
        });
        this.router.navigate(["/"])
      })
    }
 }
}
