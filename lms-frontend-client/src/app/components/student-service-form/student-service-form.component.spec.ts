import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentServiceFormComponent } from './student-service-form.component';

describe('StudentServiceFormComponent', () => {
  let component: StudentServiceFormComponent;
  let fixture: ComponentFixture<StudentServiceFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StudentServiceFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(StudentServiceFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
