import { Component } from '@angular/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { CourseService } from '../../services/course/course.service';
import { Subject } from '../../models/subject';
import { TeachingTypeService } from '../../services/teaching-type/teaching-type.service';
import { TeachingType } from '../../models/teachingType';
import { NgFor, NgIf } from '@angular/common';
import { Router } from '@angular/router';
import { CourseViewDTO } from '../../models/courseViewDTO';
import { TeachingSlotsService } from '../../services/teaching-slots/teaching-slots.service';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-teaching-slot-form',
  standalone: true,
  imports: [ReactiveFormsModule, FormsModule, NgFor, NgIf],
  templateUrl: './teaching-slot-form.component.html',
  styleUrl: './teaching-slot-form.component.css'
})
export class TeachingSlotFormComponent {

  teacherSlotForm: FormGroup
  courses: CourseViewDTO[] = []
  teachingTypes: TeachingType[]

  constructor(private fb: FormBuilder, private router: Router,
     private courseService: CourseService, private teachingTypeService: TeachingTypeService,
      private teachingSlotService: TeachingSlotsService, private snackBar: MatSnackBar) {}

     onSubmit(): void {
      if (this.teacherSlotForm.valid) {
        console.log('Form Submitted', this.teacherSlotForm.value);
        this.teachingSlotService.addSlot(this.teacherSlotForm.value).subscribe(r => {
          console.log(r)
          this.snackBar.open('Slot added successfully!', 'Close', {
            duration: 3000,
          });
          this.router.navigate(["/"])
        })
      } 
    }

    onReset(): void {
      this.teacherSlotForm.reset();
    }

ngOnInit(): void {
  this.teacherSlotForm = this.fb.group({
    startTime: ['', [Validators.required]],
    endTime: ['', Validators.required],
    teachingType: ['', Validators.required],
    courseRealization: ['', Validators.required],
  });

  this.courseService.getAll().subscribe(r => {
    console.log(r)
    this.courses = r;
  })

  this.teachingTypeService.getAll().subscribe(r => {
    console.log(r)
    this.teachingTypes = r
  })
  
}

}
