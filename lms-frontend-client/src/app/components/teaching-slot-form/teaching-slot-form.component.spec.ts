import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeachingSlotFormComponent } from './teaching-slot-form.component';

describe('TeachingSlotFormComponent', () => {
  let component: TeachingSlotFormComponent;
  let fixture: ComponentFixture<TeachingSlotFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TeachingSlotFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TeachingSlotFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
