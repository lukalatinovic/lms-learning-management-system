
import { DragDropModule } from '@angular/cdk/drag-drop';
import {MatFormFieldModule} from '@angular/material/form-field';
import { NgFor, NgIf } from '@angular/common';
import {Component, ViewChild} from '@angular/core';
import {CdkDragDrop, CdkDropList, CdkDrag, moveItemInArray} from '@angular/cdk/drag-drop';
import {MatTable, MatTableDataSource, MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import { Student } from '../../models/student';
import {MatChipsModule} from '@angular/material/chips';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import { StudentService } from '../../services/student/student.service';
import { SearchStudentsService } from '../../services/search-students/search-students.service';
import { StudentDTO } from '../../models/studentDTO';


@Component({
  selector: 'app-students-search',
  standalone: true,
  imports: [CdkDropList, ReactiveFormsModule, CdkDrag, MatButtonModule, MatTableModule, MatInputModule, MatIconModule, MatChipsModule, NgFor, MatFormFieldModule, MatSelectModule],
  templateUrl: './students-search.component.html',
  styleUrl: './students-search.component.css'
})
export class StudentsSearchComponent {
  @ViewChild('table', {static: true}) table: MatTable<StudentDTO>;

  constructor(private studentService: StudentService, private searchStudentService: SearchStudentsService) {}

  searchForm: FormGroup = new FormGroup({
    firstName: new FormControl(),
    lastName: new FormControl(),
    indexNumber: new FormControl(),
    yearOfEnrollment: new FormControl(),
  })

  ngOnInit(): void {
    this.studentService.getStudentsByTeacherId().subscribe(r => {
      console.log(r)
      this.allStudents = r.map(s => new StudentDTO(s.firstName,
         s.lastName, s.averageGrade, s.ectsScoredPoints, s.index ? s.index : undefined, 
         s.yearOfEnrollments ? s.yearOfEnrollments.substring(0, 10) : undefined ));

      this.filteredStudents = [...this.allStudents];

      console.log(this.allStudents)
    })    
  }
  
  years: number[] = [2020, 2021, 2022, 2023];
  grades: number[] = [5, 6, 7, 8, 9, 10];

  allStudents: StudentDTO []

  filteredStudents: StudentDTO []

  onSearch() {
    const filters = this.searchForm.value;

    this.filteredStudents = this.allStudents.filter(student => {
      return (
        (!filters.firstName || student.firstName.toLowerCase().includes(filters.firstName.toLowerCase())) &&
        (!filters.lastName || student.lastName.toLowerCase().includes(filters.lastName.toLowerCase())) &&
        (!filters.indexNumber || student.index?.toLowerCase().includes(filters.indexNumber.toLowerCase())) &&
        (!filters.yearOfEnrollment || student.yearOfEnrollments?.includes(filters.yearOfEnrollment))
      );
    });
  }

  clearInputs() {
    this.searchForm.reset()
    this.filteredStudents = this.allStudents
  }

  displayedColumns: string[] = ['first name', 'last name', 'index', 'year of enrollment', 'average grade', 'ectsScoredPoints'];

  drop(event: CdkDragDrop<string>) {
    const previousIndex = this.filteredStudents.findIndex(d => d === event.item.data);

    moveItemInArray(this.filteredStudents, previousIndex, event.currentIndex);
    this.table.renderRows();
  }
}

