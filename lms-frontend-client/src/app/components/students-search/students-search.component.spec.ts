import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsSearchComponent } from './students-search.component';

describe('StudentsSearchComponent', () => {
  let component: StudentsSearchComponent;
  let fixture: ComponentFixture<StudentsSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StudentsSearchComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(StudentsSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
