import { Component, OnInit, EventEmitter, Output, Input, OnChanges, SimpleChanges } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { StudentService } from '../../services/student/student.service';
import { YearOfStudyService } from '../../services/yearOfStudy/yearOfStudy.service';
import { StudyProgrammeService } from '../../services/studyProgramme/studyProgramme.service';
import { Student } from '../../models/student';

@Component({
  selector: 'app-student-table',
  templateUrl: './student-table.component.html',
  styleUrls: ['./student-table.component.css'],
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule
  ]
})
export class StudentTableComponent implements OnInit, OnChanges {
  @Input() formOpen: boolean = false;
  @Input() searchValue: string = '';
  @Output() studentsSelected = new EventEmitter<Student[]>();
  @Output() allStudentsLoaded = new EventEmitter<Student[]>();
  students: (Student & { checked: boolean })[] = [];
  filteredStudents: (Student & { checked: boolean })[] = [];
  searchCriteria: any = {};

  constructor(
    private studentService: StudentService,
    private yearOfStudyService: YearOfStudyService,
    private studyProgrammeService: StudyProgrammeService
  ) {}

  ngOnInit(): void {
    this.loadStudents();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['searchValue']) {
      this.filterStudents();
    }
  }

  loadStudents() {
    this.studentService.getStudents().subscribe(data => {
        this.students = data.map(student => ({
            ...student,
            checked: false
        }));

        this.filteredStudents = this.students;
        this.allStudentsLoaded.emit(this.students);
    });
  }

  searchStudents() {
    this.studentService.searchStudents(this.searchCriteria).subscribe(data => {
      this.students = data.map(student => ({
        ...student,
        checked: false
      }));
      this.filteredStudents = this.students;
      this.allStudentsLoaded.emit(this.students);
    });
  }

  onStudentCheckChange(student: Student & { checked: boolean }) {
    if (!this.formOpen) {
      this.emitSelectedStudents();
    }
  }

  toggleSelectAll(event: any) {
    if (!this.formOpen) {
      const checked = event.target.checked;
      this.students.forEach(student => student.checked = checked);
      this.emitSelectedStudents();
    }
  }

  emitSelectedStudents() {
    const selectedStudents = this.students.filter(student => student.checked);
    this.studentsSelected.emit(selectedStudents);
  }

  filterStudents() {
    const searchValue = this.searchValue.toLowerCase();
    this.filteredStudents = this.students.filter(student => 
      student.firstName.toLowerCase().includes(searchValue) ||
      student.lastName.toLowerCase().includes(searchValue) ||
      student.jmbg.includes(searchValue)
    );
  }

  getYearOfStudy(student: Student): string {
    if (student.studentOnYear && student.studentOnYear.length > 0) {
        const yearOfStudy = student.studentOnYear[0].yearOfStudy;
        if (yearOfStudy) {
            return new Date(yearOfStudy.year).getFullYear().toString();
        }
    }
    return '/';
  }

  getStudyProgramme(student: Student): string {
    if (student.studentOnYear && student.studentOnYear.length > 0) {
        const yearOfStudy = student.studentOnYear[0].yearOfStudy;
        if (yearOfStudy && yearOfStudy.studyProgramme) {
            return yearOfStudy.studyProgramme.name;
        }
    }
    return '/';
  }

  getDateOfEnroll(student: Student): string {
    return student.studentOnYear?.length > 0 && student.studentOnYear[0].dateOfEnroll
        ? student.studentOnYear[0].dateOfEnroll.toString().split('T')[0]
        : '/';
  }
}
