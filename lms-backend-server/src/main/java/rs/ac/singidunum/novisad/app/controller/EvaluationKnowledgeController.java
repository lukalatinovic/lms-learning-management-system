package rs.ac.singidunum.novisad.app.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rs.ac.singidunum.novisad.app.dto.EducationalGoalDTO;
import rs.ac.singidunum.novisad.app.dto.EvaluationKnowledgeDTO;
import rs.ac.singidunum.novisad.app.model.EducationalGoal;
import rs.ac.singidunum.novisad.app.model.EvaluationKnowledge;
import rs.ac.singidunum.novisad.app.service.EducationalGoalService;
import rs.ac.singidunum.novisad.app.service.EvaluationKnowledgeService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/evaluation-knowledges")
public class EvaluationKnowledgeController {
    private EvaluationKnowledgeService evaluationKnowledgeService;

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<EvaluationKnowledgeDTO>> findAll(){
        List<EvaluationKnowledgeDTO> evaluationKnowledgeDTOS = new ArrayList<>();
        Iterable<EvaluationKnowledge> evaluationKnowledgeList = this.evaluationKnowledgeService.findAll();

        for (EvaluationKnowledge ek : evaluationKnowledgeList) {
            evaluationKnowledgeDTOS.add(new EvaluationKnowledgeDTO(ek.getId(), ek.getStartTime(), ek.getEndTime(), ek.getPoints()));
        }

        return new ResponseEntity<>(evaluationKnowledgeDTOS, HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<EvaluationKnowledgeDTO> create(@RequestBody EvaluationKnowledgeDTO ekDto) {
        EvaluationKnowledge evaluationKnowledge = this.evaluationKnowledgeService.save(new EvaluationKnowledge(null, ekDto.getStartTime(), ekDto.getEndTime(),
                ekDto.getPoints(), ekDto.getEvaluationType(), ekDto.getOutcome()));
        return new ResponseEntity<>(new EvaluationKnowledgeDTO(evaluationKnowledge.getId(), evaluationKnowledge.getStartTime(), evaluationKnowledge.getEndTime(), evaluationKnowledge.getPoints()), HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<EvaluationKnowledgeDTO> getById(@PathVariable("id") Long id) {
        Optional<EvaluationKnowledge> evaluationKnowledge = this.evaluationKnowledgeService.findById(id);
        if (evaluationKnowledge.isPresent()) {
            EvaluationKnowledge ek = evaluationKnowledge.get();
            return new ResponseEntity<>(new EvaluationKnowledgeDTO(ek.getId(), ek.getStartTime(), ek.getEndTime(), ek.getPoints()), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<EvaluationKnowledgeDTO> update(@PathVariable("id") Long id, @RequestBody EvaluationKnowledgeDTO newEvaluationKnowledge) {
        Optional<EvaluationKnowledge> evaluationKnowledgeOptional = this.evaluationKnowledgeService.findById(id);

        if (evaluationKnowledgeOptional.isPresent()) {
            EvaluationKnowledge evaluationKnowledge = evaluationKnowledgeOptional.get();
            evaluationKnowledge.setId(newEvaluationKnowledge.getId());

            this.evaluationKnowledgeService.save(evaluationKnowledge);
            return new ResponseEntity<>(new EvaluationKnowledgeDTO(evaluationKnowledge.getId(), evaluationKnowledge.getStartTime(), evaluationKnowledge.getEndTime(), evaluationKnowledge.getPoints()), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        Optional<EvaluationKnowledge> evaluationKnowledgeOptional = this.evaluationKnowledgeService.findById(id);
        if (evaluationKnowledgeOptional.isPresent()) {
            this.evaluationKnowledgeService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
