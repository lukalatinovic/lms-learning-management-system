package rs.ac.singidunum.novisad.app.repository;

import org.springframework.data.repository.CrudRepository;

import rs.ac.singidunum.novisad.app.model.ScienceField;

public interface ScienceFieldRepository extends CrudRepository<ScienceField, Integer>{

}
