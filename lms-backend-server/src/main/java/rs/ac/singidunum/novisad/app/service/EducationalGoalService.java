package rs.ac.singidunum.novisad.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.singidunum.novisad.app.model.CourseRealization;
import rs.ac.singidunum.novisad.app.model.EducationalGoal;
import rs.ac.singidunum.novisad.app.repository.CourseRealizationRepository;
import rs.ac.singidunum.novisad.app.repository.EducationalGoalRepository;

import java.util.Optional;

@Service

public class EducationalGoalService {
    @Autowired
    private EducationalGoalRepository educationalGoalRepository;

    public Iterable<EducationalGoal> findAll(){
        return this.educationalGoalRepository.findAll();
    }

    public Optional<EducationalGoal> findById(Long id) {
        return this.educationalGoalRepository.findById(id);
    }

    public EducationalGoal save(EducationalGoal educationalGoal) {
        return this.educationalGoalRepository.save(educationalGoal);
    }

    public void delete(EducationalGoal educationalGoal) {
        this.educationalGoalRepository.delete(educationalGoal);
    }

    public void delete(Long id) {
        this.educationalGoalRepository.deleteById(id);
    }
}
