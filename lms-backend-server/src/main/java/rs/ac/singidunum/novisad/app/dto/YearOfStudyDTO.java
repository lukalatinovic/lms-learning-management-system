package rs.ac.singidunum.novisad.app.dto;

import java.util.Date;

public class YearOfStudyDTO {
    private Integer id;
    private Date year;
    private StudyProgrammeDTO studyProgramme;
    private StudentOnYearDTO studentOnYear;

    // Getters and Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getYear() {
        return year;
    }

    public void setYear(Date year) {
        this.year = year;
    }

    public StudyProgrammeDTO getStudyProgramme() {
        return studyProgramme;
    }

    public void setStudyProgramme(StudyProgrammeDTO studyProgramme) {
        this.studyProgramme = studyProgramme;
    }

    public StudentOnYearDTO getStudentOnYear() {
        return studentOnYear;
    }

    public void setStudentOnYear(StudentOnYearDTO studentOnYear) {
        this.studentOnYear = studentOnYear;
    }

    // Constructors

    public YearOfStudyDTO() {
        super();
    }

    public YearOfStudyDTO(Integer id, Date year) {
        this.id = id;
        this.year = year;
    }

    public YearOfStudyDTO(Integer id, Date year, StudyProgrammeDTO studyProgramme) {
        this.id = id;
        this.year = year;
        this.studyProgramme = studyProgramme;
    }

    public YearOfStudyDTO(Integer id, Date year, StudyProgrammeDTO studyProgramme, StudentOnYearDTO studentOnYear) {
        this.id = id;
        this.year = year;
        this.studyProgramme = studyProgramme;
        this.studentOnYear = studentOnYear;
    }
}
