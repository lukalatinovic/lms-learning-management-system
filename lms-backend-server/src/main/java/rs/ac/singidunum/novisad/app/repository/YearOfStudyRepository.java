package rs.ac.singidunum.novisad.app.repository;

import org.springframework.data.repository.CrudRepository;

import rs.ac.singidunum.novisad.app.model.YearOfStudy;

public interface YearOfStudyRepository extends CrudRepository<YearOfStudy, Integer>{

}
