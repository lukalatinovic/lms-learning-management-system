package rs.ac.singidunum.novisad.app.dto;

import rs.ac.singidunum.novisad.app.model.Subject;
import rs.ac.singidunum.novisad.app.model.Teacher;

import java.util.Date;

public class CourseViewDTO {
    private Subject subject;
    private Date year;
    private String studyProgramName;
    private String facultyName;
    private Teacher teacher;
    private Integer courseRealizationId;

    public CourseViewDTO(Subject subject, Date year, String studyProgramName, String facultyName, Teacher teacher, Integer courseRealizationId) {
        this.subject = subject;
        this.year = year;
        this.studyProgramName = studyProgramName;
        this.facultyName = facultyName;
        this.teacher = teacher;
        this.courseRealizationId = courseRealizationId;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Date getYear() {
        return year;
    }

    public void setYear(Date year) {
        this.year = year;
    }

    public String getStudyProgramName() {
        return studyProgramName;
    }

    public void setStudyProgramName(String studyProgramName) {
        this.studyProgramName = studyProgramName;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Integer getCourseRealizationId() {
        return courseRealizationId;
    }

    public void setCourseRealizationId(Integer courseRealizationId) {
        this.courseRealizationId = courseRealizationId;
    }
}
