package rs.ac.singidunum.novisad.app.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;
import rs.ac.singidunum.novisad.app.model.StudentOnYear;

import java.util.List;
import java.util.Optional;

public interface StudentOnYearRepository extends JpaRepository<StudentOnYear, Integer> {

    List<StudentOnYear> findByStudentId(Integer studentId);

    @EntityGraph(attributePaths = {"yearOfStudy", "yearOfStudy.studyProgramme"})
    @NonNull
    Optional<StudentOnYear> findById(@NonNull Integer id);
}
