package rs.ac.singidunum.novisad.app.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import rs.ac.singidunum.novisad.app.model.PlaceOfResidence;

public interface PlaceOfResidenceRepository extends CrudRepository<PlaceOfResidence, Integer> {
    List<PlaceOfResidence> findByCountryId(Integer countryId);
}
