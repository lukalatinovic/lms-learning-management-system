package rs.ac.singidunum.novisad.app.dto;

import rs.ac.singidunum.novisad.app.model.Role;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public class UserDTO {

    private Long id;
    private String username;
    private String password;
    private List<Role> roles;
    private String email;
    private Date dateOfBirth;

    public UserDTO(String username, String password, String email, Date dateOfBirth) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
    }
    // Constructors
    public UserDTO() {
        super();
    }

    public UserDTO(String username, String password, String email, Date dateOfBirth, List<Role> roles) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
        this.roles = roles;
    }

    public UserDTO(Long id, String username, String password, String email, Date dateOfBirth, List<Role> roles) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
        this.roles = roles;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}
