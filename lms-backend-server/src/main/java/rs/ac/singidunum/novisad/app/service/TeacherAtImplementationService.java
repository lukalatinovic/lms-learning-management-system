package rs.ac.singidunum.novisad.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.app.model.TeacherAtImplementation;
import rs.ac.singidunum.novisad.app.repository.TeacherAtImplementationRepository;

@Service
public class TeacherAtImplementationService {
	@Autowired
	private TeacherAtImplementationRepository teacherAtImplementationRepository;

	public Iterable<TeacherAtImplementation> findAll(){
		return this.teacherAtImplementationRepository.findAll();
	}
	
	public Optional<TeacherAtImplementation> findById(Integer id) {
		return this.teacherAtImplementationRepository.findById(id);
	}
	
	public TeacherAtImplementation save(TeacherAtImplementation c) {
		return this.teacherAtImplementationRepository.save(c);
	}
	
	public void delete(TeacherAtImplementation c) {
		this.teacherAtImplementationRepository.delete(c);
	}
	
	public void delete(Integer id) {
		this.teacherAtImplementationRepository.deleteById(id);
	}
}
