package rs.ac.singidunum.novisad.app.model;

import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
public class EvaluationKnowledge {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDateTime startTime;

    @Column(nullable = false)
    private LocalDateTime endTime;

    @Column(nullable = false)
    private Double points;

    @ManyToOne
    private EvaluationType evaluationType;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "outcome_id", referencedColumnName = "id")
    private Outcome outcome;


    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "evaluation_knowledge_examination",
            joinColumns = @JoinColumn(name = "evaluation_knowledge_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "examination_id", referencedColumnName = "id"))
    private List<Examination> examinations = new ArrayList<>();

    public EvaluationKnowledge() {}

    public EvaluationKnowledge(Long id, LocalDateTime startTime, LocalDateTime endTime, Double points, EvaluationType evaluationType, Outcome outcome) {
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
        this.points = points;
        this.evaluationType = evaluationType;
        this.outcome = outcome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public Double getPoints() {
        return points;
    }

    public void setPoints(Double points) {
        this.points = points;
    }

    public EvaluationType getEvaluationType() {
        return evaluationType;
    }

    public void setEvaluationType(EvaluationType evaluationType) {
        this.evaluationType = evaluationType;
    }

    public Outcome getOutcome() {
        return outcome;
    }

    public void setOutcome(Outcome outcome) {
        this.outcome = outcome;
    }
    
    public List<Examination> getExaminations() {
        return examinations;
    }
}
