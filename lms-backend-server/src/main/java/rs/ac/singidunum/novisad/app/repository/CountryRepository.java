package rs.ac.singidunum.novisad.app.repository;

import org.springframework.data.repository.CrudRepository;

import rs.ac.singidunum.novisad.app.model.Country;

public interface CountryRepository extends CrudRepository<Country, Integer>{

}
