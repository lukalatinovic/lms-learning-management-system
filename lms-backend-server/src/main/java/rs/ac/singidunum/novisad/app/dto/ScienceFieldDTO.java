package rs.ac.singidunum.novisad.app.dto;

import rs.ac.singidunum.novisad.app.model.Title;

public class ScienceFieldDTO {
    private Integer id;
    private String name;
    private Title title;
    
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public Title getTitle() {
        return title;
    }
    
    public void setTitle(Title title) {
        this.title = title;
    }
    
    public ScienceFieldDTO() {
        super();
    }
    
    public ScienceFieldDTO(String name) {
        super();
        this.name = name;
    }
    
    public ScienceFieldDTO(Integer id, String name) {
        super();
        this.id = id;
        this.name = name;
    }
    
    public ScienceFieldDTO(Integer id, String name, Title title) {
        super();
        this.id = id;
        this.name = name;
        this.title = title;
    }
}
