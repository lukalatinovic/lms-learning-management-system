package rs.ac.singidunum.novisad.app.model;


import jakarta.persistence.*;

import java.util.List;

@Entity
public class EvaluationType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @OneToMany(mappedBy = "evaluationType")
    private List<EvaluationKnowledge> evaluationKnowledgeList;

    public EvaluationType(Long id, String name, List<EvaluationKnowledge> evaluationKnowledgeList) {
        this.id = id;
        this.name = name;
        this.evaluationKnowledgeList = evaluationKnowledgeList;
    }

    public EvaluationType() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EvaluationKnowledge> getEvaluationKnowledgeList() {
        return evaluationKnowledgeList;
    }

    public void setEvaluationKnowledgeList(List<EvaluationKnowledge> evaluationKnowledgeList) {
        this.evaluationKnowledgeList = evaluationKnowledgeList;
    }
}
