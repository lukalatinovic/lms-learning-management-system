package rs.ac.singidunum.novisad.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.app.model.Teacher;
import rs.ac.singidunum.novisad.app.repository.TeacherRepository;

@Service
public class TeacherService {
	@Autowired
	private TeacherRepository teacherRepository;

	public Iterable<Teacher> findAll(){
		return this.teacherRepository.findAll();
	}
	
	public Optional<Teacher> findById(Long id) {
		return this.teacherRepository.findById(id);
	}
	
	public Teacher save(Teacher t) {
		return this.teacherRepository.save(t);
	}

	public Iterable<Teacher> getAllProfessors() {
		return this.teacherRepository.getAllProfessors();
	}

	public Iterable<Teacher> getAllAssistants() {
		return this.teacherRepository.getAllAssistants();
	}
	
	public void delete(Teacher t) {
		this.teacherRepository.delete(t);
	}
	
	public void delete(Long id) {
		this.teacherRepository.deleteById(id);
	}
}
