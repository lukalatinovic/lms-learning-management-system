package rs.ac.singidunum.novisad.app.model;

import java.util.List;
import jakarta.persistence.*;

@NamedEntityGraph(name = "Student.graph", attributeNodes = {
        @NamedAttributeNode("studentOnYear"),
        @NamedAttributeNode("courseAttendances")
})
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@Entity
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 13)
    private String jmbg;

    @Column(nullable = false, length = 30)
    private String firstName;

    @Column(nullable = false, length = 30)
    private String lastName;

    @ManyToOne
    @JoinColumn(name = "address_id", nullable = false)
    private Address address;

    @OneToMany(mappedBy = "student", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<StudentOnYear> studentOnYear;

    @OneToMany(mappedBy = "student", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<CourseAttendance> courseAttendances;

    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    private User user;

    // Getters and Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getJmbg() {
        return jmbg;
    }

    public void setJmbg(String jmbg) {
        this.jmbg = jmbg;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<StudentOnYear> getStudentOnYear() {
        return studentOnYear;
    }

    public void setStudentOnYear(List<StudentOnYear> studentOnYear) {
        this.studentOnYear = studentOnYear;
    }

    public List<CourseAttendance> getCourseAttendances() {
        return courseAttendances;
    }

    public void setCourseAttendances(List<CourseAttendance> courseAttendances) {
        this.courseAttendances = courseAttendances;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Student() {
        super();
    }

    public Student(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Student(Integer id, String jmbg, String firstName, String lastName, Address address) {
        this.id = id;
        this.jmbg = jmbg;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
    }

    public Student(Integer id, String jmbg, String firstName, String lastName, Address address, User user) {
        this.id = id;
        this.jmbg = jmbg;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.user = user;
    }

    public Student(Integer id, String jmbg, String firstName, String lastName, Address address, User user,
                   List<StudentOnYear> studentOnYear, List<CourseAttendance> courseAttendances) {
        this.id = id;
        this.jmbg = jmbg;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.user = user;
        this.studentOnYear = studentOnYear;
        this.courseAttendances = courseAttendances;
    }

    public String getIndexNumber() {
        if (studentOnYear != null && !studentOnYear.isEmpty()) {
            return studentOnYear.get(studentOnYear.size() - 1).getIndexNumber();
        }
        return null;
    }
}
