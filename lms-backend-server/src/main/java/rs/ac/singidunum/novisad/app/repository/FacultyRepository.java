package rs.ac.singidunum.novisad.app.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;
import rs.ac.singidunum.novisad.app.model.Faculty;

import java.util.List;

public interface FacultyRepository extends JpaRepository<Faculty, Integer>{

    @Override
    @EntityGraph(attributePaths = {
        "address",
        "dean",
        "studyProgrammes.yearOfStudy.subjects",
        "university"
    }, type = EntityGraph.EntityGraphType.FETCH)
    @NonNull
    List<Faculty> findAll();
}
