package rs.ac.singidunum.novisad.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import rs.ac.singidunum.novisad.app.dto.StudentOnYearDTO;
import rs.ac.singidunum.novisad.app.dto.YearOfStudyDTO;
import rs.ac.singidunum.novisad.app.dto.StudyProgrammeDTO;
import rs.ac.singidunum.novisad.app.model.Student;
import rs.ac.singidunum.novisad.app.model.StudentOnYear;
import rs.ac.singidunum.novisad.app.model.StudyProgramme;
import rs.ac.singidunum.novisad.app.model.YearOfStudy;
import rs.ac.singidunum.novisad.app.service.StudentOnYearService;
import rs.ac.singidunum.novisad.app.service.StudentService;
import rs.ac.singidunum.novisad.app.service.YearOfStudyService;

import java.util.ArrayList;
import java.util.Optional;

@Controller
@RequestMapping("/api/studentsOnYear")
public class StudentOnYearController {

    private StudentOnYearService studentOnYearService;
    private StudentService studentService;
    private YearOfStudyService yearOfStudyService;

    @Autowired
    public StudentOnYearController(StudentOnYearService studentOnYearService, StudentService studentService, YearOfStudyService yearOfStudyService) {
        this.studentOnYearService = studentOnYearService;
        this.studentService = studentService;
        this.yearOfStudyService = yearOfStudyService;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<StudentOnYearDTO>> findAll() {
        ArrayList<StudentOnYearDTO> studentsOnYearDTO = new ArrayList<>();
        Iterable<StudentOnYear> studentsOnYear = studentOnYearService.findAll();

        for (StudentOnYear s : studentsOnYear) {
            YearOfStudyDTO yearOfStudyDTO = new YearOfStudyDTO(
                s.getYearOfStudy().getId(),
                s.getYearOfStudy().getYear(),
                new StudyProgrammeDTO(
                    s.getYearOfStudy().getStudyProgramme().getId(),
                    s.getYearOfStudy().getStudyProgramme().getName()
                )
            );

            studentsOnYearDTO.add(new StudentOnYearDTO(
                s.getId(),
                s.getDateOfEnroll(),
                s.getIndexNumber(),
                s.getStudent().getId(),
                yearOfStudyDTO
            ));
        }

        return new ResponseEntity<>(studentsOnYearDTO, HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<StudentOnYearDTO> createStudentOnYear(@RequestBody StudentOnYearDTO newStudentOnYear) {
        Optional<Student> student = studentService.findById(newStudentOnYear.getStudentId());
        Optional<YearOfStudy> yearOfStudy = yearOfStudyService.findById(newStudentOnYear.getYearOfStudyId());

        if (student.isPresent() && yearOfStudy.isPresent()) {
            StudentOnYear s = this.studentOnYearService.save(new StudentOnYear(
                    null,
                    newStudentOnYear.getDateOfEnroll(),
                    newStudentOnYear.getIndexNumber(),
                    student.get(),
                    yearOfStudy.get()
            ));
            if (s != null) {
                return new ResponseEntity<>(new StudentOnYearDTO(s.getId(), s.getDateOfEnroll(), s.getIndexNumber(), s.getStudent().getId(), s.getYearOfStudy().getId()), HttpStatus.CREATED);
            }
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<StudentOnYearDTO> getStudentOnYear(@PathVariable("id") Integer id) {
        Optional<StudentOnYear> studentOnYearOptional = this.studentOnYearService.findById(id);
        if (studentOnYearOptional.isPresent()) {
            StudentOnYear studentOnYear = studentOnYearOptional.get();

            YearOfStudy yearOfStudy = studentOnYear.getYearOfStudy();
            if (yearOfStudy != null) {
                StudyProgramme studyProgramme = yearOfStudy.getStudyProgramme();
                StudyProgrammeDTO studyProgrammeDTO = new StudyProgrammeDTO(
                    studyProgramme.getId(),
                    studyProgramme.getName()
                );

                YearOfStudyDTO yearOfStudyDTO = new YearOfStudyDTO(
                    yearOfStudy.getId(),
                    yearOfStudy.getYear(),
                    studyProgrammeDTO
                );

                StudentOnYearDTO studentOnYearDTO = new StudentOnYearDTO(
                    studentOnYear.getId(),
                    studentOnYear.getDateOfEnroll(),
                    studentOnYear.getIndexNumber(),
                    studentOnYear.getStudent().getId(),
                    yearOfStudyDTO
                );

                return new ResponseEntity<>(studentOnYearDTO, HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<StudentOnYearDTO> updateStudent(@PathVariable("id") Integer id, @RequestBody StudentOnYearDTO newStudentOnYear) {
        Optional<StudentOnYear> studentOnYear = this.studentOnYearService.findById(id);
        Optional<Student> student = studentService.findById(newStudentOnYear.getStudentId());
        Optional<YearOfStudy> yearOfStudy = yearOfStudyService.findById(newStudentOnYear.getYearOfStudyId());

        if (studentOnYear.isPresent() && student.isPresent() && yearOfStudy.isPresent()) {
            StudentOnYear s = studentOnYear.get();
            s.setDateOfEnroll(newStudentOnYear.getDateOfEnroll());
            s.setIndexNumber(newStudentOnYear.getIndexNumber());
            s.setStudent(student.get());
            s.setYearOfStudy(yearOfStudy.get());
            this.studentOnYearService.save(s);
            return new ResponseEntity<>(
                    new StudentOnYearDTO(s.getId(), s.getDateOfEnroll(), s.getIndexNumber(), s.getStudent().getId(), s.getYearOfStudy().getId()),
                    HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteStudentOnYear(@PathVariable("id") Integer id) {
        Optional<StudentOnYear> studentOnYear = this.studentOnYearService.findById(id);
        if (studentOnYear.isPresent()) {
            this.studentOnYearService.delete(studentOnYear.get());
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
