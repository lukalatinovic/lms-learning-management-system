package rs.ac.singidunum.novisad.app.repository;

import org.springframework.data.repository.CrudRepository;

import rs.ac.singidunum.novisad.app.model.TeacherAtImplementation;

public interface TeacherAtImplementationRepository extends CrudRepository<TeacherAtImplementation, Integer>{

}
