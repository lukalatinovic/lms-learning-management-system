package rs.ac.singidunum.novisad.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import rs.ac.singidunum.novisad.app.dto.YearOfStudyDTO;
import rs.ac.singidunum.novisad.app.model.YearOfStudy;
import rs.ac.singidunum.novisad.app.service.YearOfStudyService;

import java.util.ArrayList;
import java.util.Optional;

@Controller
@RequestMapping("/api/yearsOfStudy")
public class YearOfStudyController {

    @Autowired
    private YearOfStudyService yearOfStudyService;

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<YearOfStudyDTO>> findAll() {
        ArrayList<YearOfStudyDTO> yearsOfStudyDTO = new ArrayList<>();
        Iterable<YearOfStudy> yearsOfStudy = yearOfStudyService.findAll();

        for (YearOfStudy y : yearsOfStudy) {
            yearsOfStudyDTO.add(new YearOfStudyDTO(y.getId(), y.getYear()));
        }

        return new ResponseEntity<>(yearsOfStudyDTO, HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<YearOfStudyDTO> createYearOfStudy(@RequestBody YearOfStudyDTO newYearOfStudy) {
        YearOfStudy a = this.yearOfStudyService.save(new YearOfStudy(null, newYearOfStudy.getYear(), null, null));
        if (a != null) {
            return new ResponseEntity<>(new YearOfStudyDTO(a.getId(), a.getYear()), HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<YearOfStudyDTO> getYearOfStudy(@PathVariable("id") Integer id) {
        Optional<YearOfStudy> yearOfStudy = this.yearOfStudyService.findById(id);
        if (yearOfStudy.isPresent()) {
            return new ResponseEntity<>(new YearOfStudyDTO(yearOfStudy.get().getId(), yearOfStudy.get().getYear()), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<YearOfStudyDTO> updateYearOfStudy(@PathVariable("id") Integer id, @RequestBody YearOfStudyDTO newYearOfStudy) {
        Optional<YearOfStudy> yearOfStudy = this.yearOfStudyService.findById(id);

        if (yearOfStudy.isPresent()) {
            YearOfStudy year = yearOfStudy.get();
            year.setYear(newYearOfStudy.getYear());
            year.setStudentOnYear(null);
            year.setStudyProgramme(null);
            this.yearOfStudyService.save(year);
            return new ResponseEntity<>(new YearOfStudyDTO(year.getId(), year.getYear()), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<YearOfStudyDTO> deleteYearOfStudy(@PathVariable("id") Integer id) {
        Optional<YearOfStudy> yearOfStudy = this.yearOfStudyService.findById(id);
        if (yearOfStudy.isPresent()) {
            this.yearOfStudyService.delete(yearOfStudy.get());
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
