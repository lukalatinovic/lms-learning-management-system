package rs.ac.singidunum.novisad.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.app.model.TitleType;
import rs.ac.singidunum.novisad.app.repository.TitleTypeRepository;

@Service
public class TitleTypeService {
	@Autowired
	private TitleTypeRepository titleTypeRepository;

	public Iterable<TitleType> findAll(){
		return this.titleTypeRepository.findAll();
	}
	
	public Optional<TitleType> findById(Integer id) {
		return this.titleTypeRepository.findById(id);
	}
	
	public TitleType save(TitleType t) {
		return this.titleTypeRepository.save(t);
	}
	
	public void delete(TitleType t) {
		this.titleTypeRepository.delete(t);
	}
	
	public void delete(Integer id) {
		this.titleTypeRepository.deleteById(id);
	}
}
