package rs.ac.singidunum.novisad.app.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import rs.ac.singidunum.novisad.app.dto.TeacherStudentsDTO;
import rs.ac.singidunum.novisad.app.model.EvaluationKnowledge;
import rs.ac.singidunum.novisad.app.model.Examination;
import rs.ac.singidunum.novisad.app.model.Student;
import rs.ac.singidunum.novisad.app.model.StudentOnYear;
import rs.ac.singidunum.novisad.app.repository.ExaminationRepository;
import rs.ac.singidunum.novisad.app.repository.StudentOnYearRepository;
import rs.ac.singidunum.novisad.app.repository.StudentRepository;
import rs.ac.singidunum.novisad.app.repository.EvaluationalKnowledgeRepository;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private StudentOnYearRepository studentOnYearRepository;
    
    @Autowired
    private ExaminationRepository examinationRepository;

    @Autowired
    private EvaluationalKnowledgeRepository evaluationKnowledgeRepository;

    public Iterable<Student> findAll() {
        return this.studentRepository.findAll();
    }
    
    public Optional<Student> findById(Integer id) {
        return this.studentRepository.findById(id);
    }

	public Iterable<TeacherStudentsDTO> findStudentsByTeacherId(Integer id, Long subjectId) {
        if (subjectId != null) {
            return this.studentRepository.findStudentByTeacherAndSubject(id, subjectId);
        }
		return this.studentRepository.findStudentsByTeacherId(id);
    }

    public Student save(Student c) {
        return this.studentRepository.save(c);
    }

    public void delete(Student c) {
        this.studentRepository.delete(c);
    }

    public void delete(Integer id) {
        this.studentRepository.deleteById(id);
    }

    @Transactional
    public Iterable<Student> findAllWithoutCourseAttendances() {
        Iterable<Student> students = this.studentRepository.findAll();
        for (Student student : students) {
            Hibernate.initialize(student.getAddress());
        }
        return students;
    }

    @Transactional
    public void deleteStudentById(Integer id) {
        Optional<Student> studentOpt = studentRepository.findById(id);
        if (studentOpt.isPresent()) {
            Student student = studentOpt.get();

            // Prvo obrišite sve StudentOnYear povezane sa studentom
            List<StudentOnYear> studentOnYears = studentOnYearRepository.findByStudentId(student.getId());
            for (StudentOnYear soy : studentOnYears) {
                // Obrišite sve ispite povezane sa StudentOnYear
                List<Examination> examinations = examinationRepository.findByStudentOnYearId(soy.getId());
                for (Examination exam : examinations) {
                    // Uklonite unose u tabeli evaluation_knowledge_examination
                    List<EvaluationKnowledge> evalKnowledgeList = exam.getEvaluationKnowledgeList();
                    for (EvaluationKnowledge evalKnowledge : evalKnowledgeList) {
                        evalKnowledge.getExaminations().remove(exam);
                        evaluationKnowledgeRepository.save(evalKnowledge);
                    }
                    // Sada obrišite Examination
                    examinationRepository.delete(exam);
                }
                // Obrišite StudentOnYear
                studentOnYearRepository.delete(soy);
            }

            // Sada možete obrisati samog studenta
            studentRepository.delete(student);
        }
    }

    @Transactional
    public void deleteStudentsByIds(List<Long> ids) {
        List<Integer> integerIds = ids.stream()
                                      .map(Long::intValue)
                                      .collect(Collectors.toList());
        Iterable<Student> students = studentRepository.findAllById(integerIds);

        for (Student student : students) {
            // Prvo obrišite sve StudentOnYear povezane sa studentom
            List<StudentOnYear> studentOnYears = studentOnYearRepository.findByStudentId(student.getId());
            for (StudentOnYear soy : studentOnYears) {
                // Obrišite sve ispite povezane sa StudentOnYear
                List<Examination> examinations = examinationRepository.findByStudentOnYearId(soy.getId());
                for (Examination exam : examinations) {
                    // Uklonite unose u tabeli evaluation_knowledge_examination
                    List<EvaluationKnowledge> evalKnowledgeList = exam.getEvaluationKnowledgeList();
                    for (EvaluationKnowledge evalKnowledge : evalKnowledgeList) {
                        evalKnowledge.getExaminations().remove(exam);
                        evaluationKnowledgeRepository.save(evalKnowledge);
                    }
                    // Sada obrišite Examination
                    examinationRepository.delete(exam);
                }
                // Obrišite StudentOnYear
                studentOnYearRepository.delete(soy);
            }

            // Sada obrišite studenta
            studentRepository.delete(student);
        }
    }
}
