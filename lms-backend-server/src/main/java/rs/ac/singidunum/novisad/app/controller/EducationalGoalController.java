package rs.ac.singidunum.novisad.app.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rs.ac.singidunum.novisad.app.dto.EducationalGoalDTO;
import rs.ac.singidunum.novisad.app.model.EducationalGoal;
import rs.ac.singidunum.novisad.app.service.EducationalGoalService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/educational-goals")
public class EducationalGoalController {
    private EducationalGoalService educationalGoalService;

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<EducationalGoalDTO>> findAll(){
        List<EducationalGoalDTO> educationalGoalDTOS = new ArrayList<>();
        Iterable<EducationalGoal> educationalGoals = this.educationalGoalService.findAll();

        for (EducationalGoal eg : educationalGoals) {
            educationalGoalDTOS.add(new EducationalGoalDTO(eg.getId(), eg.getDescription()));
        }

        return new ResponseEntity<>(educationalGoalDTOS, HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<EducationalGoalDTO> create(@RequestBody EducationalGoalDTO educationalGoalDTO) {
        EducationalGoal educationalGoal = this.educationalGoalService.save(new EducationalGoal(null, educationalGoalDTO.getDescription(), educationalGoalDTO.getOutcomes()));
        return new ResponseEntity<>(new EducationalGoalDTO(educationalGoal.getId(), educationalGoal.getDescription()), HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<EducationalGoalDTO> getCourseRealization(@PathVariable("id") Long id) {
        Optional<EducationalGoal> educationalGoal = this.educationalGoalService.findById(id);
        if (educationalGoal.isPresent()) {
            EducationalGoal eg = educationalGoal.get();
            return new ResponseEntity<>(new EducationalGoalDTO(eg.getId(), eg.getDescription()), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<EducationalGoalDTO> updateCourseRealization(@PathVariable("id") Long id, @RequestBody EducationalGoalDTO newEducationalGoal) {
        Optional<EducationalGoal> educationalGoalOptional = this.educationalGoalService.findById(id);

        if (educationalGoalOptional.isPresent()) {
            EducationalGoal educationalGoal = educationalGoalOptional.get();
            educationalGoal.setId(newEducationalGoal.getId());

            this.educationalGoalService.save(educationalGoal);
            return new ResponseEntity<>(new EducationalGoalDTO(educationalGoal.getId(), educationalGoal.getDescription()), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteCourseRealization(@PathVariable("id") Long id) {
        Optional<EducationalGoal> educationalGoalOptional = this.educationalGoalService.findById(id);
        if (educationalGoalOptional.isPresent()) {
            this.educationalGoalService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
