package rs.ac.singidunum.novisad.app.model;

import jakarta.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class University {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 100)
    private String name;

    @Column(nullable = false)
    private Date foundationDate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "rector_id")
    private Teacher rector;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "address_id")
    private Address address;

    @OneToMany(mappedBy = "university", fetch = FetchType.LAZY)
    private List<Faculty> faculties;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getFoundationDate() {
        return foundationDate;
    }

    public void setFoundationDate(Date foundationDate) {
        this.foundationDate = foundationDate;
    }

    public Teacher getRector() {
        return rector;
    }

    public void setRector(Teacher rector) {
        this.rector = rector;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
	}

	public List<Faculty> getFaculties() {
		return faculties;
	}

	public void setFaculties(List<Faculty> faculties) {
		this.faculties = faculties;
	}

	public University() {
		super();
	}

	public University(Integer id, String name, Date foundationDate, Teacher rector, Address address,
			List<Faculty> faculties) {
		super();
		this.id = id;
		this.name = name;
		this.foundationDate = foundationDate;
		this.rector = rector;
		this.address = address;
		this.faculties = faculties;
	}
}
