package rs.ac.singidunum.novisad.app.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import rs.ac.singidunum.novisad.app.model.Outcome;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
public class EducationalGoalDTO {

    private Long id;

    private String description;

    private List<Outcome> outcomes;

    public EducationalGoalDTO(Long id, String description) {
        this.id = id;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Outcome> getOutcomes() {
        return outcomes;
    }

    public void setOutcomes(List<Outcome> outcomes) {
        this.outcomes = outcomes;
    }
}
