package rs.ac.singidunum.novisad.app.model;

import jakarta.persistence.*;

import java.util.List;

@Entity
public class Teacher {

    @Id
    private Long id;

    @OneToOne
    @MapsId
    @JoinColumn(name = "id")
    private User user;
    
    @Column(nullable = false, length = 13)
    private String jmbg;
    
    @Column(nullable = false, length = 30)
    private String firstName;
    
    @Column(nullable = false, length = 30)
    private String lastName;
    
    @Column(nullable = false, length = 1000)
    private String biography;
    
    @OneToOne
    private Title title;
    
    @OneToOne(mappedBy = "dean")
    private Faculty faculty;
    
    @OneToOne(mappedBy = "rector")
    private University university;
    
    @OneToOne(mappedBy = "coordinator")
    private StudyProgramme studyProgramme;
    
    @OneToOne
    private Address address;

    @OneToMany(mappedBy = "teacher")
    private List<CourseRealization> courseRealizations;

    public Teacher(User user, String jmbg, String firstName, String lastName, String biography) {
        this.user = user;
        this.jmbg = jmbg;
        this.firstName = firstName;
        this.lastName = lastName;
        this.biography = biography;
    }

    public Teacher(User user, String jmbg, String firstName, String lastName, String biography, Title title) {
        this.user = user;
        this.jmbg = jmbg;
        this.firstName = firstName;
        this.lastName = lastName;
        this.biography = biography;
        this.title = title;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getJmbg() {
        return jmbg;
    }

    public void setJmbg(String jmbg) {
        this.jmbg = jmbg;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    public University getUniversity() {
        return university;
    }

    public void setUniversity(University university) {
        this.university = university;
    }

    public StudyProgramme getStudyProgramme() {
        return studyProgramme;
    }

    public void setStudyProgramme(StudyProgramme studyProgramme) {
        this.studyProgramme = studyProgramme;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Teacher() {
        super();
    }

    public Teacher(Long id, String jmbg, String firstName, String lastName, String biography, Title title,
            Faculty faculty, University university, StudyProgramme studyProgramme, Address address) {
        super();
        this.id = id;
        this.jmbg = jmbg;
        this.firstName = firstName;
        this.lastName = lastName;
        this.biography = biography;
        this.title = title;
        this.faculty = faculty;
        this.university = university;
        this.studyProgramme = studyProgramme;
        this.address = address;
    }

    public Teacher(Long id, String firstName, String lastName, Title title) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.title = title;
    }
}
