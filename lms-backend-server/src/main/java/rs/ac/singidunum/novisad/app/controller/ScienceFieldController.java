package rs.ac.singidunum.novisad.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import rs.ac.singidunum.novisad.app.dto.ScienceFieldDTO;
import rs.ac.singidunum.novisad.app.model.ScienceField;
import rs.ac.singidunum.novisad.app.model.Title;
import rs.ac.singidunum.novisad.app.service.ScienceFieldService;

@Controller
@RequestMapping("/api/scienceFields")
public class ScienceFieldController {

    private ScienceFieldService scienceFieldService;

    @Autowired
    public ScienceFieldController(ScienceFieldService scienceFieldService) {
        this.scienceFieldService = scienceFieldService;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<ScienceFieldDTO>> findAll() {
        ArrayList<ScienceFieldDTO> scienceFieldsDTO = new ArrayList<>();
        Iterable<ScienceField> scienceFields = this.scienceFieldService.findAll();

        for (ScienceField s : scienceFields) {
            scienceFieldsDTO.add(new ScienceFieldDTO(
                    s.getId(),
                    s.getName()
            ));
        }

        return new ResponseEntity<>(scienceFieldsDTO, HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<ScienceFieldDTO> createScienceField(@RequestBody ScienceFieldDTO newScienceField) {
        ScienceField s = this.scienceFieldService.save(new ScienceField(
                null,
                newScienceField.getName()
        ));
        if (s != null) {
            return new ResponseEntity<>(new ScienceFieldDTO(
                    s.getId(),
                    s.getName()
            ), HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<ScienceFieldDTO> getScienceField(@PathVariable("id") Integer id) {
        Optional<ScienceField> scienceField = this.scienceFieldService.findById(id);
        if (scienceField.isPresent()) {
            return new ResponseEntity<>(new ScienceFieldDTO(
                    scienceField.get().getId(),
                    scienceField.get().getName()
            ), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<ScienceFieldDTO> updateScienceField(@PathVariable("id") Integer id, @RequestBody ScienceFieldDTO newScienceField) {
        Optional<ScienceField> scienceField = this.scienceFieldService.findById(id);

        if (scienceField.isPresent()) {
            scienceField.get().setId(id);
            scienceField.get().setName(newScienceField.getName());

            this.scienceFieldService.save(scienceField.get());
            return new ResponseEntity<>(new ScienceFieldDTO(
                    scienceField.get().getId(),
                    scienceField.get().getName()
            ), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<ScienceFieldDTO> deleteScienceField(@PathVariable("id") Integer id) {
        Optional<ScienceField> scienceField = this.scienceFieldService.findById(id);
        if (scienceField.isPresent()) {
            this.scienceFieldService.delete(scienceField.get());
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
