package rs.ac.singidunum.novisad.app.service;

import jakarta.websocket.server.ServerEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.singidunum.novisad.app.model.File;
import rs.ac.singidunum.novisad.app.model.Notification;
import rs.ac.singidunum.novisad.app.repository.FileRepository;
import rs.ac.singidunum.novisad.app.repository.NotificationRepository;

import java.util.Optional;

@Service
public class NotificationService {

    @Autowired
    private NotificationRepository notificationRepository;

    public Iterable<Notification> findAll(){
        return this.notificationRepository.findAll();
    }

    public Optional<Notification> findById(Long id) {
        return this.notificationRepository.findById(id);
    }

    public Notification save(Notification notification) {
        return this.notificationRepository.save(notification);
    }

    public void delete(Notification notification) {
        this.notificationRepository.delete(notification);
    }

    public void delete(Long id) {
        this.notificationRepository.deleteById(id);
    }
}
