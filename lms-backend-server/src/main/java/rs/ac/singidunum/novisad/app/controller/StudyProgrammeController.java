package rs.ac.singidunum.novisad.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import rs.ac.singidunum.novisad.app.dto.StudyProgrammeDTO;
import rs.ac.singidunum.novisad.app.model.StudyProgramme;
import rs.ac.singidunum.novisad.app.service.StudyProgrammeService;

import java.util.ArrayList;
import java.util.Optional;

@Controller
@RequestMapping("/api/studyProgrammes")
public class StudyProgrammeController {

    private StudyProgrammeService studyProgrammeService;

    @Autowired
    public StudyProgrammeController(StudyProgrammeService studyProgrammeService) {
        this.studyProgrammeService = studyProgrammeService;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<StudyProgrammeDTO>> findAll() {
        ArrayList<StudyProgrammeDTO> studyProgrammesDTO = new ArrayList<>();
        Iterable<StudyProgramme> studyProgrammes = studyProgrammeService.findAll();

        for (StudyProgramme s : studyProgrammes) {
            studyProgrammesDTO.add(new StudyProgrammeDTO(s.getId(), s.getName()));
        }

        return new ResponseEntity<>(studyProgrammesDTO, HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<StudyProgrammeDTO> createStudyProgramme(@RequestBody StudyProgrammeDTO newStudyProgramme) {
        StudyProgramme s = this.studyProgrammeService.save(new StudyProgramme(
                null,
                newStudyProgramme.getName(),
                null,
                null,
                null
        ));
        if (s != null) {
            return new ResponseEntity<>(new StudyProgrammeDTO(s.getId(), s.getName()), HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<StudyProgrammeDTO> getStudyProgramme(@PathVariable("id") Integer id) {
        Optional<StudyProgramme> studyProgramme = this.studyProgrammeService.findById(id);
        if (studyProgramme.isPresent()) {
            return new ResponseEntity<>(
                    new StudyProgrammeDTO(studyProgramme.get().getId(), studyProgramme.get().getName()),
                    HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<StudyProgrammeDTO> updateStudyProgramme(@PathVariable("id") Integer id, @RequestBody StudyProgrammeDTO newStudyProgramme) {
        Optional<StudyProgramme> studyProgramme = this.studyProgrammeService.findById(id);

        if (studyProgramme.isPresent()) {
            studyProgramme.get().setId(id);
            studyProgramme.get().setName(newStudyProgramme.getName());
            this.studyProgrammeService.save(studyProgramme.get());
            return new ResponseEntity<>(
                    new StudyProgrammeDTO(studyProgramme.get().getId(), studyProgramme.get().getName()),
                    HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<StudyProgrammeDTO> deleteStudyProgramme(@PathVariable("id") Integer id) {
        Optional<StudyProgramme> studyProgramme = this.studyProgrammeService.findById(id);
        if (studyProgramme.isPresent()) {
            this.studyProgrammeService.delete(studyProgramme.get());
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
