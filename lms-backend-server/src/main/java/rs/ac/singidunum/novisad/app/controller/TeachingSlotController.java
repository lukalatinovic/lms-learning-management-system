package rs.ac.singidunum.novisad.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rs.ac.singidunum.novisad.app.dto.TeachingSlotDTO;
import rs.ac.singidunum.novisad.app.model.Outcome;
import rs.ac.singidunum.novisad.app.model.TeachingSlot;
import rs.ac.singidunum.novisad.app.service.CourseRealizationService;
import rs.ac.singidunum.novisad.app.service.OutcomeService;
import rs.ac.singidunum.novisad.app.service.TeachingSlotService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/teaching-slots")
public class TeachingSlotController {

    @Autowired
    private TeachingSlotService teachingSlotService;

    @Autowired
    private CourseRealizationService courseRealizationService;

    @Autowired
    private OutcomeService outcomeService;

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<TeachingSlotDTO>> findAll(){
        List<TeachingSlotDTO> teachingSlotDTOS = new ArrayList<>();
        Iterable<TeachingSlot> teachingSlots = this.teachingSlotService.findAll();

        for (TeachingSlot ts : teachingSlots) {
            teachingSlotDTOS.add(new TeachingSlotDTO(ts.getId(), ts.getStartTime(), ts.getEndTime(), ts.getTeachingType()));
        }

        return new ResponseEntity<>(teachingSlotDTOS, HttpStatus.OK);
    }

    @RequestMapping(path = "/teacher/{id}", method = RequestMethod.GET)
    public ResponseEntity<Iterable<TeachingSlotDTO>> getAllTeacherSlots(@PathVariable("id") Long id){
        List<TeachingSlotDTO> teachingSlotDTOS = new ArrayList<>();
        Iterable<TeachingSlot> teachingSlots = this.teachingSlotService.getAllTeacherSlots(id);

        for (TeachingSlot ts : teachingSlots) {
            teachingSlotDTOS.add(new TeachingSlotDTO(ts.getId(), ts.getStartTime(), ts.getEndTime(), ts.getOutcome()));
        }

        return new ResponseEntity<>(teachingSlotDTOS, HttpStatus.OK);
    }

    @RequestMapping(path = "/teacher/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> addOutcomeToSlot(@PathVariable("id") Long id, @RequestBody TeachingSlotDTO teachingSlotDTO){
        Optional<TeachingSlot> teachingSlot = this.teachingSlotService.findById(id);
        if (teachingSlot.isPresent()) {
            TeachingSlot tm = teachingSlot.get();
            Outcome outcome = teachingSlotDTO.getOutcome();
            Outcome newOutcome = outcomeService.save(outcome);
            tm.setId(teachingSlotDTO.getId());
            tm.setOutcome(newOutcome);
            this.teachingSlotService.save(tm);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<TeachingSlotDTO> create(@RequestBody TeachingSlot teachingSlot) {

        TeachingSlot newTeachingSlot = this.teachingSlotService.save(
                new TeachingSlot(null, teachingSlot.getStartTime(), teachingSlot.getEndTime(),
                        teachingSlot.getTeachingType(), null, teachingSlot.getCourseRealization()));
        return new ResponseEntity<>(new TeachingSlotDTO(newTeachingSlot.getId(), newTeachingSlot.getStartTime(), newTeachingSlot.getEndTime()),
                HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<TeachingSlotDTO> getById(@PathVariable("id") Long id) {
        Optional<TeachingSlot> teachingSlot = this.teachingSlotService.findById(id);
        if (teachingSlot.isPresent()) {
            TeachingSlot ts = teachingSlot.get();
            return new ResponseEntity<>(new TeachingSlotDTO(ts.getId(),ts.getStartTime(), ts.getEndTime()), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<TeachingSlotDTO> update(@PathVariable("id") Long id, @RequestBody TeachingSlotDTO teachingSlotDTO) {
        Optional<TeachingSlot> teachingSlotOptional = this.teachingSlotService.findById(id);

        if (teachingSlotOptional.isPresent()) {
            TeachingSlot tm = teachingSlotOptional.get();
            tm.setId(teachingSlotDTO.getId());

            this.teachingSlotService.save(tm);
            return new ResponseEntity<>(new TeachingSlotDTO(tm.getId(), tm.getStartTime(), tm.getEndTime()), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        Optional<TeachingSlot> teachingSlot = this.teachingSlotService.findById(id);
        if (teachingSlot.isPresent()) {
            this.teachingSlotService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
