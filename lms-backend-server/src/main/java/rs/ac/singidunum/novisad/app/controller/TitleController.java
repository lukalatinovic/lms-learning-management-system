package rs.ac.singidunum.novisad.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import rs.ac.singidunum.novisad.app.dto.TitleDTO;
import rs.ac.singidunum.novisad.app.model.Title;
import rs.ac.singidunum.novisad.app.service.TitleService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/titles")
public class TitleController {

    private TitleService titleService;

    @Autowired
    public TitleController(TitleService titleService) {
        this.titleService = titleService;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<TitleDTO>> findAll() {
        List<TitleDTO> titlesDTO = new ArrayList<>();
        Iterable<Title> titles = titleService.findAll();

        for (Title t : titles) {
            titlesDTO.add(new TitleDTO(t.getId(), t.getElectedDate(), t.getExpireDate()));
        }

        return new ResponseEntity<>(titlesDTO, HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<TitleDTO> createTitle(@RequestBody Title newTitle) {
        Title title = this.titleService.save(new Title(null, newTitle.getElectedDate(), newTitle.getExpireDate(), newTitle.getScienceField(), newTitle.getTitleType()));
        return new ResponseEntity<>(new TitleDTO(title.getId(), title.getElectedDate(), title.getExpireDate()), HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<TitleDTO> getTitle(@PathVariable("id") Integer id) {
        Optional<Title> title = this.titleService.findById(id);
        if (title.isPresent()) {
            return new ResponseEntity<>(
                    new TitleDTO(title.get().getId(), title.get().getElectedDate(), title.get().getExpireDate()),
                    HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<TitleDTO> updateTitle(@PathVariable("id") Integer id, @RequestBody TitleDTO newTitle) {
        Optional<Title> title = this.titleService.findById(id);

        if (title.isPresent()) {
            title.get().setId(id);
            title.get().setElectedDate(newTitle.getElectedDate());
            title.get().setExpireDate(newTitle.getExpireDate());
            this.titleService.save(title.get());
            return new ResponseEntity<>(
                    new TitleDTO(title.get().getId(), title.get().getElectedDate(), title.get().getExpireDate()),
                    HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteTitle(@PathVariable("id") Integer id) {
        titleService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
