package rs.ac.singidunum.novisad.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.app.model.University;
import rs.ac.singidunum.novisad.app.repository.UniversityRepository;

@Service
public class UniversityService {
	@Autowired
	private UniversityRepository universityRepository;

	public Iterable<University> findAll(){
		return this.universityRepository.findAll();
	}
	
	public Optional<University> findById(Integer id) {
		return this.universityRepository.findById(id);
	}
	
	public University save(University u) {
		return this.universityRepository.save(u);
	}
	
	public void delete(University u) {
		this.universityRepository.delete(u);
	}
	
	public void delete(Integer id) {
		this.universityRepository.deleteById(id);
	}
}
