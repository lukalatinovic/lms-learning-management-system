package rs.ac.singidunum.novisad.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.app.model.Faculty;
import rs.ac.singidunum.novisad.app.repository.FacultyRepository;

@Service
public class FacultyService {
	@Autowired
	private FacultyRepository facultyRepository;

	public Iterable<Faculty> findAll(){
		return this.facultyRepository.findAll();
	}
	
	public Optional<Faculty> findById(Integer id) {
		return this.facultyRepository.findById(id);
	}
	
	public Faculty save(Faculty f) {
		return this.facultyRepository.save(f);
	}
	
	public void delete(Faculty f) {
		this.facultyRepository.delete(f);
	}
	
	public void delete(Integer id) {
		this.facultyRepository.deleteById(id);
	}
}
