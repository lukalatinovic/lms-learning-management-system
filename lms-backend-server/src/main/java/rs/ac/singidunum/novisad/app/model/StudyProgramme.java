package rs.ac.singidunum.novisad.app.model;

import jakarta.persistence.*;

@Entity
public class StudyProgramme {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false, length = 50)
	private String name;
	
	@OneToOne
	private Teacher coordinator;
	
	@ManyToOne
	private Faculty faculty;
	
	@OneToOne(mappedBy = "studyProgramme")
	private YearOfStudy yearOfStudy;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Teacher getCoordinator() {
		return coordinator;
	}

	public void setCoordinator(Teacher coordinator) {
		this.coordinator = coordinator;
	}

	public Faculty getFaculty() {
		return faculty;
	}

	public void setFaculty(Faculty faculty) {
		this.faculty = faculty;
	}

	public YearOfStudy getYearOfStudy() {
		return yearOfStudy;
	}

	public void setYearOfStudy(YearOfStudy yearOfStudy) {
		this.yearOfStudy = yearOfStudy;
	}

	public StudyProgramme() {
		super();
	}

	public StudyProgramme(Integer id, String name, Teacher coordinator, Faculty faculty, YearOfStudy yearOfStudy) {
		super();
		this.id = id;
		this.name = name;
		this.coordinator = coordinator;
		this.faculty = faculty;
		this.yearOfStudy = yearOfStudy;
	}
}
