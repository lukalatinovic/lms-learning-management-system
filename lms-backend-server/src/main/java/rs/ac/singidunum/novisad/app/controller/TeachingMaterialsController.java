package rs.ac.singidunum.novisad.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rs.ac.singidunum.novisad.app.dto.TeachingMaterialsDTO;
import rs.ac.singidunum.novisad.app.model.TeachingMaterials;
import rs.ac.singidunum.novisad.app.service.TeachingMaterialsService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/teaching-materials")
public class TeachingMaterialsController {

    private final TeachingMaterialsService teachingMaterialsService;

    @Autowired
    public TeachingMaterialsController(TeachingMaterialsService teachingMaterialsService) {
        this.teachingMaterialsService = teachingMaterialsService;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<TeachingMaterialsDTO>> findAll() {
        List<TeachingMaterialsDTO> teachingMaterialsDTOS = new ArrayList<>();
        Iterable<TeachingMaterials> teachingMaterials = this.teachingMaterialsService.findAll();

        for (TeachingMaterials tm : teachingMaterials) {
            teachingMaterialsDTOS.add(new TeachingMaterialsDTO(tm.getId(), tm.getName(), tm.getAuthors(), tm.getYearOfPublication()));
        }

        return new ResponseEntity<>(teachingMaterialsDTOS, HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<TeachingMaterialsDTO> create(@RequestBody TeachingMaterialsDTO teachingMaterialsDTO) {
        TeachingMaterials teachingMaterials = this.teachingMaterialsService.save(
                new TeachingMaterials(null, teachingMaterialsDTO.getName(), teachingMaterialsDTO.getAuthors(), teachingMaterialsDTO.getYearOfPublication()));
        return new ResponseEntity<>(new TeachingMaterialsDTO(teachingMaterials.getId(), teachingMaterials.getName(), teachingMaterials.getAuthors(), teachingMaterials.getYearOfPublication()),
                HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<TeachingMaterialsDTO> getById(@PathVariable("id") Long id) {
        Optional<TeachingMaterials> teachingMaterials = this.teachingMaterialsService.findById(id);
        if (teachingMaterials.isPresent()) {
            TeachingMaterials tm = teachingMaterials.get();
            return new ResponseEntity<>(new TeachingMaterialsDTO(tm.getId(), tm.getName(), tm.getAuthors(), tm.getYearOfPublication()), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<TeachingMaterialsDTO> update(@PathVariable("id") Long id, @RequestBody TeachingMaterialsDTO teachingMaterialsDTO) {
        Optional<TeachingMaterials> teachingMaterialsOptional = this.teachingMaterialsService.findById(id);

        if (teachingMaterialsOptional.isPresent()) {
            TeachingMaterials tm = teachingMaterialsOptional.get();
            tm.setName(teachingMaterialsDTO.getName());
            tm.setAuthors(teachingMaterialsDTO.getAuthors());
            tm.setYearOfPublication(teachingMaterialsDTO.getYearOfPublication());

            this.teachingMaterialsService.save(tm);
            return new ResponseEntity<>(new TeachingMaterialsDTO(tm.getId(), tm.getName(), tm.getAuthors(), tm.getYearOfPublication()), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        Optional<TeachingMaterials> teachingMaterialsOptional = this.teachingMaterialsService.findById(id);
        if (teachingMaterialsOptional.isPresent()) {
            this.teachingMaterialsService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
