package rs.ac.singidunum.novisad.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.singidunum.novisad.app.model.EvaluationKnowledge;
import rs.ac.singidunum.novisad.app.model.EvaluationType;
import rs.ac.singidunum.novisad.app.repository.EvaluationalKnowledgeRepository;
import rs.ac.singidunum.novisad.app.repository.EvulationalTypeRepository;

import java.util.Optional;

@Service
public class EvaluationTypeService {

    @Autowired
    private EvulationalTypeRepository evulationalTypeRepository;

    public Iterable<EvaluationType> findAll(){
        return this.evulationalTypeRepository.findAll();
    }

    public Optional<EvaluationType> findById(Long id) {
        return this.evulationalTypeRepository.findById(id);
    }

    public EvaluationType save(EvaluationType evaluationType) {
        return this.evulationalTypeRepository.save(evaluationType);
    }

    public void delete(EvaluationType evaluationType) {
        this.evulationalTypeRepository.delete(evaluationType);
    }

    public void delete(Long id) {
        this.evulationalTypeRepository.deleteById(id);
    }
}
