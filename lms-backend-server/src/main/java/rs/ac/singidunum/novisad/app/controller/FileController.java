package rs.ac.singidunum.novisad.app.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rs.ac.singidunum.novisad.app.dto.FileDTO;
import rs.ac.singidunum.novisad.app.model.File;
import rs.ac.singidunum.novisad.app.service.FileService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/files")
public class FileController {
    private FileService fileService;

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<FileDTO>> findAll(){
        List<FileDTO> fileDTOS = new ArrayList<>();
        Iterable<File> files = this.fileService.findAll();

        for (File f : files) {
            fileDTOS.add(new FileDTO(f.getId(), f.getDescription(), f.getUrl()));
        }

        return new ResponseEntity<>(fileDTOS, HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<FileDTO> create(@RequestBody FileDTO fileDTO) {
        File file = this.fileService.save(
                new File(null, fileDTO.getDescription(), fileDTO.getUrl(), fileDTO.getNotification()));
        return new ResponseEntity<>(new FileDTO(file.getId(), file.getDescription(), file.getUrl()),
                HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<FileDTO> getById(@PathVariable("id") Long id) {
        Optional<File> file = this.fileService.findById(id);
        if (file.isPresent()) {
            File f = file.get();
            return new ResponseEntity<>(new FileDTO(f.getId(), f.getDescription(), f.getUrl()), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<FileDTO> update(@PathVariable("id") Long id, @RequestBody FileDTO newFileDTO) {
        Optional<File> examinationOptional = this.fileService.findById(id);

        if (examinationOptional.isPresent()) {
            File file = examinationOptional.get();
            file.setId(newFileDTO.getId());

            this.fileService.save(file);
            return new ResponseEntity<>(new FileDTO(file.getId(), file.getDescription(), file.getUrl()), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        Optional<File> fileOptional = this.fileService.findById(id);
        if (fileOptional.isPresent()) {
            this.fileService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
