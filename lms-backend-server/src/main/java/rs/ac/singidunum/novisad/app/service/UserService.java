package rs.ac.singidunum.novisad.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.singidunum.novisad.app.model.Address;
import rs.ac.singidunum.novisad.app.model.User;
import rs.ac.singidunum.novisad.app.repository.AddressRepository;
import rs.ac.singidunum.novisad.app.repository.UserRepository;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public Iterable<User> findAll(){
        return this.userRepository.findAll();
    }

    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }

    public User save(User u) {
        return userRepository.save(u);
    }


    public void delete(User u) {
        userRepository.delete(u);
    }
}
