package rs.ac.singidunum.novisad.app.dto;

public class TeacherDTO {
	private Long id;
	private String jmbg;
	private String firstName;
	private String lastName;
	private String biography;
	private TitleDTO title;
	private FacultyDTO faculty;
	private UniversityDTO university;
	private StudyProgrammeDTO studyProgramme;
	private AddressDTO address;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getJmbg() {
		return jmbg;
	}
	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getBiography() {
		return biography;
	}
	public void setBiography(String biography) {
		this.biography = biography;
	}
	public TitleDTO getTitle() {
		return title;
	}
	public void setTitle(TitleDTO title) {
		this.title = title;
	}
	public FacultyDTO getFaculty() {
		return faculty;
	}
	public void setFaculty(FacultyDTO faculty) {
		this.faculty = faculty;
	}
	public UniversityDTO getUniversity() {
		return university;
	}
	public void setUniversity(UniversityDTO university) {
		this.university = university;
	}
	public StudyProgrammeDTO getStudyProgramme() {
		return studyProgramme;
	}
	public void setStudyProgramme(StudyProgrammeDTO studyProgramme) {
		this.studyProgramme = studyProgramme;
	}
	
	public AddressDTO getAddress() {
		return address;
	}
	public void setAddress(AddressDTO address) {
		this.address = address;
	}
	public TeacherDTO() {
		super();
	}
	
	public TeacherDTO(String jmbg, String firstName, String lastName, String biography) {
		super();
		this.jmbg = jmbg;
		this.firstName = firstName;
		this.lastName = lastName;
		this.biography = biography;
	}

	public TeacherDTO(Long id, String firstName, String lastName, TitleDTO title) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.title = title;
	}

	public TeacherDTO(Long id, String jmbg, String firstName, String lastName, String biography) {
		super();
		this.id = id;
		this.jmbg = jmbg;
		this.firstName = firstName;
		this.lastName = lastName;
		this.biography = biography;
	}
	
	public TeacherDTO(Long id, String jmbg, String firstName, String lastName, String biography, TitleDTO title,
			FacultyDTO faculty, UniversityDTO university, StudyProgrammeDTO studyProgramme, AddressDTO address) {
		super();
		this.id = id;
		this.jmbg = jmbg;
		this.firstName = firstName;
		this.lastName = lastName;
		this.biography = biography;
		this.title = title;
		this.faculty = faculty;
		this.university = university;
		this.studyProgramme = studyProgramme;
		this.address = address;
	}
	
	public TeacherDTO(Long id, String firstName, String lastName, String biography) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.biography = biography;
    }
	
	public TeacherDTO(Long id, String firstName, String lastName) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
	}
}
