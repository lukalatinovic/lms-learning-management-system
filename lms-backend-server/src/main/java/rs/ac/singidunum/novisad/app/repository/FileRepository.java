package rs.ac.singidunum.novisad.app.repository;

import org.springframework.data.repository.CrudRepository;
import rs.ac.singidunum.novisad.app.model.File;

public interface FileRepository extends CrudRepository<File, Long> {
}
