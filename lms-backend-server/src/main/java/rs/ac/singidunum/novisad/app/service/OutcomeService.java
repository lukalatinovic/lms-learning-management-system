package rs.ac.singidunum.novisad.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.app.model.Outcome;
import rs.ac.singidunum.novisad.app.repository.OutcomeRepository;

@Service
public class OutcomeService {
	@Autowired
	private OutcomeRepository outcomeRepository;

	public Iterable<Outcome> findAll(){
		return this.outcomeRepository.findAll();
	}
	
	public Optional<Outcome> findById(Integer id) {
		return this.outcomeRepository.findById(id);
	}
	
	public Outcome save(Outcome c) {
		return this.outcomeRepository.save(c);
	}
	
	public void delete(Outcome c) {
		this.outcomeRepository.delete(c);
	}
	
	public void delete(Integer id) {
		this.outcomeRepository.deleteById(id);
	}
}
