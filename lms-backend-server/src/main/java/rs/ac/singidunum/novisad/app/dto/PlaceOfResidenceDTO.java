package rs.ac.singidunum.novisad.app.dto;

public class PlaceOfResidenceDTO {
	private Integer id;
	private String name;
	private CountryDTO country;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public CountryDTO getCountry() {
		return country;
	}
	public void setCountry(CountryDTO country) {
		this.country = country;
	}
	
	public PlaceOfResidenceDTO() {
		super();
	}
	
	public PlaceOfResidenceDTO(String name) {
		super();
		this.name = name;
	}
	
	public PlaceOfResidenceDTO(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public PlaceOfResidenceDTO(Integer id, String name, CountryDTO country) {
		super();
		this.id = id;
		this.name = name;
		this.country = country;
	}
}
