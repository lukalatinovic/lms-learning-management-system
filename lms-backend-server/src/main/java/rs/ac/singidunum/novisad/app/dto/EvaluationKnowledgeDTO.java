package rs.ac.singidunum.novisad.app.dto;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import rs.ac.singidunum.novisad.app.model.EvaluationType;
import rs.ac.singidunum.novisad.app.model.Outcome;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
public class EvaluationKnowledgeDTO {


    private Long id;

    private LocalDateTime startTime;

    private LocalDateTime endTime;

    private Double points;

    private EvaluationType evaluationType;

    private Outcome outcome;

    public EvaluationKnowledgeDTO(Long id, LocalDateTime startTime, LocalDateTime endTime, Double points) {
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
        this.points = points;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public Double getPoints() {
        return points;
    }

    public void setPoints(Double points) {
        this.points = points;
    }

    public EvaluationType getEvaluationType() {
        return evaluationType;
    }

    public void setEvaluationType(EvaluationType evaluationType) {
        this.evaluationType = evaluationType;
    }

    public Outcome getOutcome() {
        return outcome;
    }

    public void setOutcome(Outcome outcome) {
        this.outcome = outcome;
    }
}
