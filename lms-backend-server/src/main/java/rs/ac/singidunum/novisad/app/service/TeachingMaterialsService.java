package rs.ac.singidunum.novisad.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.singidunum.novisad.app.model.TeachingMaterials;
import rs.ac.singidunum.novisad.app.repository.TeachingMaterialsRepository;

import java.util.Optional;

@Service
public class TeachingMaterialsService {

    @Autowired
    private TeachingMaterialsRepository teachingMaterialsRepository;

    public Iterable<TeachingMaterials> findAll() {
        return this.teachingMaterialsRepository.findAll();
    }

    public Optional<TeachingMaterials> findById(Long id) {
        return this.teachingMaterialsRepository.findById(id);
    }

    public TeachingMaterials save(TeachingMaterials teachingMaterials) {
        return this.teachingMaterialsRepository.save(teachingMaterials);
    }

    public void delete(TeachingMaterials teachingMaterials) {
        this.teachingMaterialsRepository.delete(teachingMaterials);
    }

    public void delete(Long id) {
        this.teachingMaterialsRepository.deleteById(id);
    }
}
