package rs.ac.singidunum.novisad.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import rs.ac.singidunum.novisad.app.config.auth.TokenProvider;
import rs.ac.singidunum.novisad.app.dto.JwtDTO;
import rs.ac.singidunum.novisad.app.dto.SignInDTO;
import rs.ac.singidunum.novisad.app.dto.SignUpDTO;
import rs.ac.singidunum.novisad.app.dto.UserDTO;
import rs.ac.singidunum.novisad.app.model.User;
import rs.ac.singidunum.novisad.app.service.AuthService;

@Controller
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private AuthService service;
    @Autowired
    private TokenProvider tokenService;

    @RequestMapping(path="/signup", method = RequestMethod.POST)
    public ResponseEntity<?> signUp(@RequestBody User data) throws Exception {
        UserDetails newUser =  service.signUp(new UserDTO(data.getUsername(), data.getPassword(), data.getEmail(), data.getDateOfBirth()));
        if (newUser != null) {
            var authUser = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    data.getUsername(),
                    data.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authUser);
            var accessToken = tokenService.generateAccessToken((User) authUser.getPrincipal());
            return new ResponseEntity<>(new JwtDTO(accessToken), HttpStatus.CREATED);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @RequestMapping(path="/signin", method = RequestMethod.POST)
    public ResponseEntity<JwtDTO> signIn(@RequestBody SignInDTO data) {
        var authUser = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                data.getUsername(),
                data.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authUser);
        var accessToken = tokenService.generateAccessToken((User) authUser.getPrincipal());
        return ResponseEntity.ok(new JwtDTO(accessToken));
    }
}