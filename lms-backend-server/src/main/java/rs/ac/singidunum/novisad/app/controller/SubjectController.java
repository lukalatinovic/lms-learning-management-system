package rs.ac.singidunum.novisad.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import rs.ac.singidunum.novisad.app.dto.CourseViewDTO;
import rs.ac.singidunum.novisad.app.dto.SubjectDTO;
import rs.ac.singidunum.novisad.app.model.*;
import rs.ac.singidunum.novisad.app.service.SubjectService;
import rs.ac.singidunum.novisad.app.service.TeacherService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/subjects")
public class SubjectController {

    private SubjectService subjectService;

    @Autowired
    public SubjectController(SubjectService subjectService) {
        this.subjectService = subjectService;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<SubjectDTO>> findAll() {
        ArrayList<SubjectDTO> subjectsDTO = new ArrayList<>();
        Iterable<Subject> subjects = subjectService.findAll();

        for (Subject s : subjects) {
            subjectsDTO.add(new SubjectDTO(
                    s.getId(),
                    s.getName(),
                    s.getEspb(),
                    s.getMandatory(),
                    s.getNumLectureClasses(),
                    s.getNumPracticeClasses(),
                    s.getNumOtherFormsClasses(),
                    s.getResearchWork(),
                    s.getNumOtherClasses(),
                    null
            ));
        }

        return new ResponseEntity<>(subjectsDTO, HttpStatus.OK);
    }

    @RequestMapping(path = "/teacher/{id}", method = RequestMethod.GET)
    public ResponseEntity<Iterable<SubjectDTO>> findSubjectsByTeacherId(@PathVariable("id") Integer id) {
        ArrayList<SubjectDTO> subjectsDTO = new ArrayList<>();
        Iterable<Subject> subjects = subjectService.findSubjectsByTeacherId(id);

        for (Subject s : subjects) {
            subjectsDTO.add(new SubjectDTO(
                    s.getId(),
                    s.getName(),
                    s.getEspb(),
                    s.getMandatory(),
                    s.getNumLectureClasses(),
                    s.getNumPracticeClasses(),
                    s.getNumOtherFormsClasses(),
                    s.getResearchWork(),
                    s.getNumOtherClasses(),
                    null
            ));
        }

        return new ResponseEntity<>(subjectsDTO, HttpStatus.OK);
    }

    @RequestMapping(path="/courses", method = RequestMethod.GET)
    public ResponseEntity<Iterable<CourseViewDTO>> getCourses() {
        Iterable<CourseViewDTO> courses = this.subjectService.getCourses();

        for (CourseViewDTO c : courses) {
            if (c.getSubject() != null) {
                c.getSubject().setPreconditionClasses(null);
                c.getSubject().setYearOfStudy(null);
            }

            if (c.getTeacher() != null) {
                c.getTeacher().setTitle(new Title(
                        new ScienceField(c.getTeacher().getTitle().getScienceField().getId(),
                                c.getTeacher().getTitle().getScienceField().getName()),
                                new TitleType(c.getTeacher().getTitle().getTitleType().getId(),
                                        c.getTeacher().getTitle().getTitleType().getName())));
                c.setTeacher(new Teacher(c.getTeacher().getId(), c.getTeacher().getFirstName(),
                        c.getTeacher().getLastName(), c.getTeacher().getTitle()));
                c.getTeacher().setStudyProgramme(null);
                c.getTeacher().setFaculty(null);
                c.getTeacher().setUser(null);
            }

        }



        return new ResponseEntity<>(courses, HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<SubjectDTO> createSubject(@RequestBody SubjectDTO newSubject) {
        Subject s = this.subjectService.save(new Subject(
                null,
                newSubject.getName(),
                newSubject.getEspb(),
                newSubject.getMandatory(),
                newSubject.getNumLectureClasses(),
                newSubject.getNumPracticeClasses(),
                newSubject.getNumOtherFormsClasses(),
                newSubject.getResearchWork(),
                newSubject.getNumOtherClasses(),
                null,
                null
        ));
        if (s != null) {
            return new ResponseEntity<>(new SubjectDTO(
                    s.getId(),
                    s.getName(),
                    s.getEspb(),
                    s.getMandatory(),
                    s.getNumLectureClasses(),
                    s.getNumPracticeClasses(),
                    s.getNumOtherFormsClasses(),
                    s.getResearchWork(),
                    s.getNumOtherClasses(),
                    null
            ), HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<SubjectDTO> getSubject(@PathVariable("id") Integer id) {
        Optional<Subject> subject = this.subjectService.findById(id);
        if (subject.isPresent()) {
            return new ResponseEntity<>(new SubjectDTO(
                    subject.get().getId(),
                    subject.get().getName(),
                    subject.get().getEspb(),
                    subject.get().getMandatory(),
                    subject.get().getNumLectureClasses(),
                    subject.get().getNumPracticeClasses(),
                    subject.get().getNumOtherFormsClasses(),
                    subject.get().getResearchWork(),
                    subject.get().getNumOtherClasses(),
                    null
            ), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<SubjectDTO> updateSubject(@PathVariable("id") Integer id, @RequestBody SubjectDTO newSubject) {
        Optional<Subject> subject = this.subjectService.findById(id);

        if (subject.isPresent()) {
            subject.get().setId(id);
            subject.get().setName(newSubject.getName());
            subject.get().setEspb(newSubject.getEspb());
            subject.get().setMandatory(newSubject.getMandatory());
            subject.get().setNumLectureClasses(newSubject.getNumLectureClasses());
            subject.get().setNumPracticeClasses(newSubject.getNumPracticeClasses());
            subject.get().setNumOtherFormsClasses(newSubject.getNumOtherFormsClasses());
            subject.get().setNumOtherClasses(newSubject.getNumOtherClasses());
            this.subjectService.save(subject.get());
            return new ResponseEntity<>(new SubjectDTO(
                    subject.get().getId(),
                    subject.get().getName(),
                    subject.get().getEspb(),
                    subject.get().getMandatory(),
                    subject.get().getNumLectureClasses(),
                    subject.get().getNumPracticeClasses(),
                    subject.get().getNumOtherFormsClasses(),
                    subject.get().getResearchWork(),
                    subject.get().getNumOtherClasses(),
                    null
            ), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<SubjectDTO> deleteSubject(@PathVariable("id") Integer id) {
        Optional<Subject> subject = this.subjectService.findById(id);
        if (subject.isPresent()) {
            this.subjectService.delete(subject.get());
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
