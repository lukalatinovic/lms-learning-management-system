package rs.ac.singidunum.novisad.app.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rs.ac.singidunum.novisad.app.dto.ExaminationDTO;
import rs.ac.singidunum.novisad.app.model.Examination;
import rs.ac.singidunum.novisad.app.service.ExaminationService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/examinations")
public class ExaminationController {

    private ExaminationService examinationService;

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<ExaminationDTO>> findAll(){
        List<ExaminationDTO> examinationDTOS = new ArrayList<>();
        Iterable<Examination> examinations = this.examinationService.findAll();

        for (Examination e : examinations) {
            examinationDTOS.add(new ExaminationDTO(e.getId(), e.getPoints()));
        }

        return new ResponseEntity<>(examinationDTOS, HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<ExaminationDTO> create(@RequestBody ExaminationDTO eDTO) {
        Examination examination = this.examinationService.save(
                new Examination(null, eDTO.getPoints(), eDTO.getStudentOnYear(), eDTO.getEvaluationKnowledgeList()));
        return new ResponseEntity<>(new ExaminationDTO(examination.getId(), examination.getPoints()),
                HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<ExaminationDTO> getById(@PathVariable("id") Long id) {
        Optional<Examination> examination = this.examinationService.findById(id);
        if (examination.isPresent()) {
            Examination e = examination.get();
            return new ResponseEntity<>(new ExaminationDTO(e.getId(), e.getPoints()), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<ExaminationDTO> update(@PathVariable("id") Long id, @RequestBody ExaminationDTO newExaminationDTO) {
        Optional<Examination> examinationOptional = this.examinationService.findById(id);

        if (examinationOptional.isPresent()) {
            Examination examination = examinationOptional.get();
            examination.setId(newExaminationDTO.getId());

            this.examinationService.save(examination);
            return new ResponseEntity<>(new ExaminationDTO(examination.getId(), examination.getPoints()), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        Optional<Examination> examinationOptional = this.examinationService.findById(id);
        if (examinationOptional.isPresent()) {
            this.examinationService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
