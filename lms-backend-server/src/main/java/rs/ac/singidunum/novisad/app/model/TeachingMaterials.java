package rs.ac.singidunum.novisad.app.model;

import jakarta.persistence.*;
import java.time.LocalDateTime;

@Entity
public class TeachingMaterials {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String authors;

    @Column(nullable = false)
    private LocalDateTime yearOfPublication;

    public TeachingMaterials() {}

    public TeachingMaterials(Long id, String name, String authors, LocalDateTime yearOfPublication) {
        this.id = id;
        this.name = name;
        this.authors = authors;
        this.yearOfPublication = yearOfPublication;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public LocalDateTime getYearOfPublication() {
        return yearOfPublication;
    }

    public void setYearOfPublication(LocalDateTime yearOfPublication) {
        this.yearOfPublication = yearOfPublication;
    }
}
