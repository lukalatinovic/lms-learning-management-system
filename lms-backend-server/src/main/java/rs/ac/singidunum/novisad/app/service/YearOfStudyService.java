package rs.ac.singidunum.novisad.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.app.model.YearOfStudy;
import rs.ac.singidunum.novisad.app.repository.YearOfStudyRepository;

@Service
public class YearOfStudyService {
	@Autowired
	private YearOfStudyRepository yearOfStudyRepository;

	public Iterable<YearOfStudy> findAll(){
		return this.yearOfStudyRepository.findAll();
	}
	
	public Optional<YearOfStudy> findById(Integer id) {
		return this.yearOfStudyRepository.findById(id);
	}
	
	public YearOfStudy save(YearOfStudy y) {
		return this.yearOfStudyRepository.save(y);
	}
	
	public void delete(YearOfStudy y) {
		this.yearOfStudyRepository.delete(y);
	}
	
	public void delete(Integer id) {
		this.yearOfStudyRepository.deleteById(id);
	}
}
