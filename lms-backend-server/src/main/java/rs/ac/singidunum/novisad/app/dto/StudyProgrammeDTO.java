package rs.ac.singidunum.novisad.app.dto;

import java.util.List;

public class StudyProgrammeDTO {
    private Integer id;
    private String name;
    private TeacherDTO coordinator;
    private FacultyDTO faculty;
    private YearOfStudyDTO yearOfStudy;
    private List<SubjectDTO> subjects;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TeacherDTO getCoordinator() {
        return coordinator;
    }

    public void setCoordinator(TeacherDTO coordinator) {
        this.coordinator = coordinator;
    }

    public FacultyDTO getFaculty() {
        return faculty;
    }

    public void setFaculty(FacultyDTO faculty) {
        this.faculty = faculty;
    }

    public YearOfStudyDTO getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(YearOfStudyDTO yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }
    
    public List<SubjectDTO> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<SubjectDTO> subjects) {
        this.subjects = subjects;
    }

    // Constructors

    public StudyProgrammeDTO() {
        super();
    }

    public StudyProgrammeDTO(String name) {
        super();
        this.name = name;
    }

    public StudyProgrammeDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public StudyProgrammeDTO(Integer id, String name, TeacherDTO coordinator, FacultyDTO faculty, YearOfStudyDTO yearOfStudy) {
        super();
        this.id = id;
        this.name = name;
        this.coordinator = coordinator;
        this.faculty = faculty;
        this.yearOfStudy = yearOfStudy;
    }
    
    public StudyProgrammeDTO(Integer id, String name, TeacherDTO coordinator, FacultyDTO faculty, YearOfStudyDTO yearOfStudy, List<SubjectDTO> subjects) {
        this.id = id;
        this.name = name;
        this.coordinator = coordinator;
        this.faculty = faculty;
        this.yearOfStudy = yearOfStudy;
        this.subjects = subjects;
    }
}
