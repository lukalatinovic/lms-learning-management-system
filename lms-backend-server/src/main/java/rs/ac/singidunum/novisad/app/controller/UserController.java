package rs.ac.singidunum.novisad.app.controller;

import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rs.ac.singidunum.novisad.app.dto.UserDTO;
import rs.ac.singidunum.novisad.app.model.Role;
import rs.ac.singidunum.novisad.app.model.User;
import rs.ac.singidunum.novisad.app.repository.RoleRepository;
import rs.ac.singidunum.novisad.app.service.RoleService;
import rs.ac.singidunum.novisad.app.service.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    RoleRepository roleRepository;

    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<UserDTO>> findAll() {
        ArrayList<UserDTO> usersDto = new ArrayList<>();
        Iterable<User> users = userService.findAll();

        for (User u : users) {
            usersDto.add(new UserDTO(u.getId(), u.getUsername(), u.getPassword(), u.getEmail(), u.getDateOfBirth(), u.getRoles()));
        }

        return new ResponseEntity<>(usersDto, HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<UserDTO> addUser(@RequestBody User user) {
        User newUser = userService.save(user);
        UserDTO userDTO = new UserDTO(newUser.getId(), newUser.getUsername(), newUser.getPassword(), newUser.getEmail(),
                newUser.getDateOfBirth(), newUser.getRoles());

        return new ResponseEntity<>(userDTO, HttpStatus.OK);
    }

    @RequestMapping(path = "/studentService", method = RequestMethod.POST)
    public ResponseEntity<UserDTO> addStudentService(@RequestBody User user) {
        Role role = this.roleRepository.findByName("studentService").get();
        List<Role> roles = new ArrayList<>();
        roles.add(role);
        user.setRoles(roles);
        String encryptedPassword = new BCryptPasswordEncoder().encode(user.getPassword());
        user.setPassword(encryptedPassword);
        User newUser = userService.save(user);
        UserDTO userDTO = new UserDTO(newUser.getId(), newUser.getUsername(), newUser.getPassword(), newUser.getEmail(),
                newUser.getDateOfBirth(), newUser.getRoles());

        return new ResponseEntity<>(userDTO, HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<UserDTO> updateUser(@PathVariable("id") Long id, @RequestBody User user) {
        Optional<User> userOptional = userService.findById(id);

        if (userOptional.isPresent()) {
            User updatedUser = userOptional.get();
//            updatedUser.setUsername(userDto.getUsername());
//            if (!encoder.matches(userDto.getPassword(), updatedUser.getPassword())) {
//                String encryptedPassword = encoder.encode(userDto.getPassword());
//                updatedUser.setPassword(encryptedPassword);
//            }
            updatedUser.setRoles(user.getRoles());
            userService.save(updatedUser);
            return new ResponseEntity<>(
                    new UserDTO(updatedUser.getId(), updatedUser.getUsername(), updatedUser.getPassword(),
                            updatedUser.getEmail(), updatedUser.getDateOfBirth(), updatedUser.getRoles()),
                    HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> delete(@PathVariable("id") Long id) {
        Optional<User> user = userService.findById(id);
        if (user.isPresent()) {
            userService.delete(user.get());
            return new ResponseEntity<>("Successfully deleted", HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
