package rs.ac.singidunum.novisad.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.app.model.StudyProgramme;
import rs.ac.singidunum.novisad.app.repository.StudyProgrammeRepository;

@Service
public class StudyProgrammeService {
	@Autowired
	private StudyProgrammeRepository studyProgrammeRepository;

	public Iterable<StudyProgramme> findAll(){
		return this.studyProgrammeRepository.findAll();
	}
	
	public Optional<StudyProgramme> findById(Integer id) {
		return this.studyProgrammeRepository.findById(id);
	}
	
	public StudyProgramme save(StudyProgramme c) {
		return this.studyProgrammeRepository.save(c);
	}
	
	public void delete(StudyProgramme c) {
		this.studyProgrammeRepository.delete(c);
	}
	
	public void delete(Integer id) {
		this.studyProgrammeRepository.deleteById(id);
	}
}
