package rs.ac.singidunum.novisad.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import rs.ac.singidunum.novisad.app.dto.CountryDTO;
import rs.ac.singidunum.novisad.app.dto.PlaceOfResidenceDTO;
import rs.ac.singidunum.novisad.app.model.Country;
import rs.ac.singidunum.novisad.app.model.PlaceOfResidence;
import rs.ac.singidunum.novisad.app.service.PlaceOfResidenceService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/placesOfResidence")
public class PlaceOfResidenceController {

    private PlaceOfResidenceService placeOfResidenceService;

    @Autowired
    public PlaceOfResidenceController(PlaceOfResidenceService placeOfResidenceService) {
        this.placeOfResidenceService = placeOfResidenceService;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<PlaceOfResidenceDTO>> findAll() {
        List<PlaceOfResidenceDTO> placeOfResidenceDTOs = new ArrayList<>();
        Iterable<PlaceOfResidence> placesOfResidence = this.placeOfResidenceService.findAll();

        for (PlaceOfResidence placeOfResidence : placesOfResidence) {
            placeOfResidenceDTOs.add(new PlaceOfResidenceDTO(
                    placeOfResidence.getId(),
                    placeOfResidence.getName(),
                    new CountryDTO(
                            placeOfResidence.getCountry().getId(),
                            placeOfResidence.getCountry().getName()
                    )
            ));
        }

        return new ResponseEntity<>(placeOfResidenceDTOs, HttpStatus.OK);
    }

    @RequestMapping(path = "/country/{countryId}", method = RequestMethod.GET)
    public ResponseEntity<List<PlaceOfResidenceDTO>> findByCountryId(@PathVariable Integer countryId) {
        List<PlaceOfResidence> placesOfResidence = this.placeOfResidenceService.findByCountryId(countryId);
        List<PlaceOfResidenceDTO> placeOfResidenceDTOs = new ArrayList<>();
        for (PlaceOfResidence placeOfResidence : placesOfResidence) {
            placeOfResidenceDTOs.add(new PlaceOfResidenceDTO(
                    placeOfResidence.getId(),
                    placeOfResidence.getName(),
                    new CountryDTO(
                            placeOfResidence.getCountry().getId(),
                            placeOfResidence.getCountry().getName()
                    )
            ));
        }
        return new ResponseEntity<>(placeOfResidenceDTOs, HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<PlaceOfResidenceDTO> createPlaceOfResidence(@RequestBody PlaceOfResidenceDTO newPlaceOfResidence) {
        PlaceOfResidence placeOfResidence = new PlaceOfResidence();
        placeOfResidence.setName(newPlaceOfResidence.getName());
        
        Country country = new Country(newPlaceOfResidence.getCountry().getId(), newPlaceOfResidence.getCountry().getName());

        placeOfResidence.setCountry(country);
        placeOfResidence = this.placeOfResidenceService.save(placeOfResidence);
        return new ResponseEntity<>(new PlaceOfResidenceDTO(
                placeOfResidence.getId(),
                placeOfResidence.getName(),
                newPlaceOfResidence.getCountry()
        ), HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<PlaceOfResidenceDTO> getPlaceOfResidence(@PathVariable("id") Integer id) {
        Optional<PlaceOfResidence> placeOfResidenceOptional = this.placeOfResidenceService.findById(id);
        if (placeOfResidenceOptional.isPresent()) {
            PlaceOfResidence placeOfResidence = placeOfResidenceOptional.get();
            return new ResponseEntity<>(new PlaceOfResidenceDTO(
                    placeOfResidence.getId(),
                    placeOfResidence.getName(),
                    new CountryDTO(
                            placeOfResidence.getCountry().getId(),
                            placeOfResidence.getCountry().getName()
                    )
            ), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<PlaceOfResidenceDTO> updatePlaceOfResidence(@PathVariable("id") Integer id, @RequestBody PlaceOfResidenceDTO newPlaceOfResidence) {
        Optional<PlaceOfResidence> placeOfResidenceOptional = this.placeOfResidenceService.findById(id);
        if (placeOfResidenceOptional.isPresent()) {
            PlaceOfResidence placeOfResidence = placeOfResidenceOptional.get();
            placeOfResidence.setName(newPlaceOfResidence.getName());
            
            Country country = new Country(newPlaceOfResidence.getCountry().getId(), newPlaceOfResidence.getCountry().getName());

            placeOfResidence.setCountry(country);
            placeOfResidence = this.placeOfResidenceService.save(placeOfResidence);
            return new ResponseEntity<>(new PlaceOfResidenceDTO(
                    placeOfResidence.getId(),
                    placeOfResidence.getName(),
                    newPlaceOfResidence.getCountry()
            ), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deletePlaceOfResidence(@PathVariable("id") Integer id) {
        this.placeOfResidenceService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
