package rs.ac.singidunum.novisad.app.model;

import jakarta.persistence.*;
import java.util.List;

@NamedEntityGraph(name = "Faculty.graph", attributeNodes = {
    @NamedAttributeNode("address"),
    @NamedAttributeNode("dean"),
    @NamedAttributeNode("studyProgrammes"),
    @NamedAttributeNode("university")
})
@Entity
public class Faculty {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 30)
    private String name;

    @OneToOne(fetch = FetchType.EAGER)
    private Address address;

    @OneToOne(fetch = FetchType.EAGER)
    private Teacher dean;

    @OneToMany(mappedBy = "faculty", fetch = FetchType.LAZY)
    private List<StudyProgramme> studyProgrammes;

    @ManyToOne(fetch = FetchType.EAGER)
    private University university;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Teacher getDean() {
		return dean;
	}

	public void setDean(Teacher dean) {
		this.dean = dean;
	}

	public List<StudyProgramme> getStudyProgrammes() {
		return studyProgrammes;
	}

	public void setStudyProgrammes(List<StudyProgramme> studyProgrammes) {
		this.studyProgrammes = studyProgrammes;
	}

	public University getUniversity() {
		return university;
	}

	public void setUniversity(University university) {
		this.university = university;
	}

	public Faculty() {
		super();
	}

	public Faculty(Integer id, String name, Address address, Teacher dean, List<StudyProgramme> studyProgrammes,
			University university) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.dean = dean;
		this.studyProgrammes = studyProgrammes;
		this.university = university;
	}
}
