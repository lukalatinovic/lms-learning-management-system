package rs.ac.singidunum.novisad.app.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import org.springframework.data.repository.query.Param;
import rs.ac.singidunum.novisad.app.dto.CourseViewDTO;
import rs.ac.singidunum.novisad.app.model.Subject;

import java.util.List;

public interface SubjectRepository extends CrudRepository<Subject, Integer>{
    @Query("SELECT DISTINCT s FROM Subject s JOIN CourseRealization cr ON s.id = cr.subject.id WHERE cr.teacher.id = :teacherId")
    List<Subject> findSubjectsByTeacherId(@Param("teacherId") Integer teacherId);

    @Query("SELECT new rs.ac.singidunum.novisad.app.dto.CourseViewDTO(s, yos.year, sp.name, f.name, t, cr.id) " +
            "FROM Subject s LEFT JOIN CourseRealization cr ON s.id = cr.subject.id " +
            "LEFT JOIN Teacher t ON cr.teacher.id = t.id " +
            "LEFT JOIN YearOfStudy yos ON s.yearOfStudy.id = yos.id " +
            "LEFT JOIN StudyProgramme sp ON yos.studyProgramme.id = sp.id " +
            "LEFT JOIN Faculty f ON sp.faculty.id = f.id " +
            "GROUP BY s, yos.year, sp.name, f.name, t, cr.id")
    List<CourseViewDTO> getCourses();
}
