package rs.ac.singidunum.novisad.app.dto;

import java.util.Date;

public class StudentOnYearDTO {
    private Integer id;
    private Date dateOfEnroll;
    private String indexNumber;
    private Integer studentId;
    private Integer yearOfStudyId;
    private YearOfStudyDTO yearOfStudy; // Dodat atribut

    // Getters and Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateOfEnroll() {
        return dateOfEnroll;
    }

    public void setDateOfEnroll(Date dateOfEnroll) {
        this.dateOfEnroll = dateOfEnroll;
    }

    public String getIndexNumber() {
        return indexNumber;
    }

    public void setIndexNumber(String indexNumber) {
        this.indexNumber = indexNumber;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public Integer getYearOfStudyId() {
        return yearOfStudyId;
    }

    public void setYearOfStudyId(Integer yearOfStudyId) {
        this.yearOfStudyId = yearOfStudyId;
    }

    public YearOfStudyDTO getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(YearOfStudyDTO yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    // Constructors

    public StudentOnYearDTO() {
        super();
    }

    public StudentOnYearDTO(Integer id, Date dateOfEnroll, String indexNumber, Integer studentId, Integer yearOfStudyId) {
        this.id = id;
        this.dateOfEnroll = dateOfEnroll;
        this.indexNumber = indexNumber;
        this.studentId = studentId;
        this.yearOfStudyId = yearOfStudyId;
    }

    public StudentOnYearDTO(Integer id, Date dateOfEnroll, String indexNumber, Integer studentId, YearOfStudyDTO yearOfStudy) {
        this.id = id;
        this.dateOfEnroll = dateOfEnroll;
        this.indexNumber = indexNumber;
        this.studentId = studentId;
        this.yearOfStudy = yearOfStudy;
    }
}
