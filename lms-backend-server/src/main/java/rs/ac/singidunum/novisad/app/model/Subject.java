package rs.ac.singidunum.novisad.app.model;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Subject {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    @Column(nullable = false, length = 30)
    private String name;
    
    @Column(nullable = false)
    private Integer espb;
    
    @Column(nullable = false)
    private Boolean mandatory;
    
    @Column(nullable = false)
    private Integer numLectureClasses;
    
    @Column(nullable = false)
    private Integer numPracticeClasses;
    
    @Column
    private Integer numOtherFormsClasses;
    
    @Column
    private Integer researchWork;
    
    @Column
    private Integer numOtherClasses;
    
//    @ManyToMany(mappedBy = "preconditionClasses")
//    private List<Subject> preconditionClasses = new ArrayList<>();
    
    @OneToMany(mappedBy = "subject")
    private List<CourseRealization> courseRealizations = new ArrayList<>();

    @ManyToOne
    private YearOfStudy yearOfStudy;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getEspb() {
        return espb;
    }

    public void setEspb(Integer espb) {
        this.espb = espb;
    }

    public Boolean getMandatory() {
        return mandatory;
    }

    public void setMandatory(Boolean mandatory) {
        this.mandatory = mandatory;
    }

    public Integer getNumLectureClasses() {
        return numLectureClasses;
    }

    public void setNumLectureClasses(Integer numLectureClasses) {
        this.numLectureClasses = numLectureClasses;
    }

    public Integer getNumPracticeClasses() {
        return numPracticeClasses;
    }

    public void setNumPracticeClasses(Integer numPracticeClasses) {
        this.numPracticeClasses = numPracticeClasses;
    }

    public Integer getNumOtherFormsClasses() {
        return numOtherFormsClasses;
    }

    public void setNumOtherFormsClasses(Integer numOtherFormsClasses) {
        this.numOtherFormsClasses = numOtherFormsClasses;
    }

    public Integer getResearchWork() {
        return researchWork;
    }

    public void setResearchWork(Integer researchWork) {
        this.researchWork = researchWork;
    }

    public Integer getNumOtherClasses() {
        return numOtherClasses;
    }

    public void setNumOtherClasses(Integer numOtherClasses) {
        this.numOtherClasses = numOtherClasses;
    }

    public List<CourseRealization> getPreconditionClasses() {
        return courseRealizations;
    }

    public void setPreconditionClasses(List<CourseRealization> courseRealizations) {
        this.courseRealizations = courseRealizations;
    }

    public YearOfStudy getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(YearOfStudy yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    public Subject() {
        super();
    }

    public Subject(Integer id, String name, Integer espb, Boolean mandatory, Integer numLectureClasses,
            Integer numPracticeClasses, Integer numOtherFormsClasses, Integer researchWork, Integer numOtherClasses,
            List<CourseRealization> courseRealizations, YearOfStudy yearOfStudy) {
    	
		 super();
		 this.id = id;
		 this.name = name;
		 this.espb = espb;
		 this.mandatory = mandatory;
		 this.numLectureClasses = numLectureClasses;
		 this.numPracticeClasses = numPracticeClasses;
		 this.numOtherFormsClasses = numOtherFormsClasses;
		 this.researchWork = researchWork;
		 this.numOtherClasses = numOtherClasses;
		 this.courseRealizations = courseRealizations;
         this.yearOfStudy = yearOfStudy;
		 
    }

}
