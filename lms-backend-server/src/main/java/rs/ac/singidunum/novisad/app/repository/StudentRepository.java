package rs.ac.singidunum.novisad.app.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import rs.ac.singidunum.novisad.app.dto.StudentDTO;
import rs.ac.singidunum.novisad.app.dto.TeacherStudentsDTO;
import rs.ac.singidunum.novisad.app.model.Student;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student, Integer>, JpaSpecificationExecutor<Student> {
    
    @Query("SELECT new rs.ac.singidunum.novisad.app.dto.TeacherStudentsDTO(" +
            "s.firstName, s.lastName, sy.indexNumber, sy.dateOfEnroll, " +
            "AVG(ca2.finalGrade), COALESCE(SUM(sub.espb), 0)) " +
            "FROM Student s " +
            "JOIN StudentOnYear sy ON s.id = sy.student.id " +
            "JOIN YearOfStudy ys ON sy.yearOfStudy.id = ys.id " +
            "JOIN CourseRealization cr ON ys.id = cr.subject.yearOfStudy.id " +
            "JOIN Teacher t ON cr.teacher.id = t.id " +
            "LEFT JOIN CourseAttendance ca2 ON ca2.student = s AND ca2.courseRealization.subject.yearOfStudy = ys " +
            "LEFT JOIN Subject sub ON sub.id = cr.subject.id AND ca2.courseRealization.subject.id = sub.id " +
            "WHERE t.id = :teacherId " +
            "GROUP BY s.id, sy.indexNumber, sy.dateOfEnroll")
    List<TeacherStudentsDTO> findStudentsByTeacherId(@Param("teacherId") Integer teacherId);

    @Query("SELECT new rs.ac.singidunum.novisad.app.dto.TeacherStudentsDTO(" +
            "s.firstName, s.lastName, sy.indexNumber, sy.dateOfEnroll, " +
            "AVG(ca2.finalGrade), COALESCE(SUM(sub.espb), 0)) " +
            "FROM Student s " +
            "JOIN StudentOnYear sy ON s.id = sy.student.id " +
            "JOIN YearOfStudy ys ON sy.yearOfStudy.id = ys.id " +
            "JOIN CourseRealization cr ON ys.id = cr.subject.yearOfStudy.id " +
            "JOIN Teacher t ON cr.teacher.id = t.id " +
            "LEFT JOIN CourseAttendance ca2 ON ca2.student = s AND ca2.courseRealization.subject.yearOfStudy = ys " +
            "LEFT JOIN Subject sub ON sub.id = cr.subject.id AND ca2.courseRealization.subject.id = sub.id " +
            "WHERE t.id = :teacherId AND sub.id = :subjectId " +
            "GROUP BY s.id, sy.indexNumber, sy.dateOfEnroll")
    List<TeacherStudentsDTO> findStudentByTeacherAndSubject(@Param("teacherId") Integer teacherId, @Param("subjectId") Long subjectId);

}

