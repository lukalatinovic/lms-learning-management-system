package rs.ac.singidunum.novisad.app.dto;

public class AddressDTO {
    private Integer id;
    private String street;
    private String number;
    private PlaceOfResidenceDTO placeOfResidence;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public PlaceOfResidenceDTO getPlaceOfResidence() {
        return placeOfResidence;
    }

    public void setPlaceOfResidence(PlaceOfResidenceDTO placeOfResidence) {
        this.placeOfResidence = placeOfResidence;
    }

    public AddressDTO() {
        super();
    }

    public AddressDTO(String street, String number) {
        super();
        this.street = street;
        this.number = number;
    }

    public AddressDTO(Integer id, String street, String number) {
        super();
        this.id = id;
        this.street = street;
        this.number = number;
    }

    public AddressDTO(Integer id, String street, String number, PlaceOfResidenceDTO placeOfResidence) {
        super();
        this.id = id;
        this.street = street;
        this.number = number;
        this.placeOfResidence = placeOfResidence;
    }
}
