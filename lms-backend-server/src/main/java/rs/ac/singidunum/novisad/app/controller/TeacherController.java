package rs.ac.singidunum.novisad.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import rs.ac.singidunum.novisad.app.dto.ProfessorsAssistantsDTO;
import rs.ac.singidunum.novisad.app.dto.TeacherDTO;
import rs.ac.singidunum.novisad.app.dto.TitleDTO;
import rs.ac.singidunum.novisad.app.dto.TitleTypeDTO;
import rs.ac.singidunum.novisad.app.model.*;
import rs.ac.singidunum.novisad.app.repository.RoleRepository;
import rs.ac.singidunum.novisad.app.service.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/teachers")
public class TeacherController {

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private TitleService titleService;


    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<TeacherDTO>> findAll() {
        List<TeacherDTO> teachersDTO = new ArrayList<>();
        Iterable<Teacher> teachers = teacherService.findAll();

        for (Teacher teacher : teachers) {
            TeacherDTO teacherDTO = new TeacherDTO();
            teacherDTO.setId(teacher.getId());
            teacherDTO.setJmbg(teacher.getJmbg());
            teacherDTO.setFirstName(teacher.getFirstName());
            teacherDTO.setLastName(teacher.getLastName());
            teacherDTO.setBiography(teacher.getBiography());
            teachersDTO.add(teacherDTO);
        }

        return new ResponseEntity<>(teachersDTO, HttpStatus.OK);
    }

    @RequestMapping(path = "/professorsAndAssistents", method = RequestMethod.GET)
    public ResponseEntity<ProfessorsAssistantsDTO> getAllProfessors() {
        Iterable<Teacher> professors = this.teacherService.getAllProfessors();
        Iterable<Teacher> assistants = this.teacherService.getAllAssistants();
        List<TeacherDTO> proffesorDTOS = new ArrayList<>();
        List<TeacherDTO> assistantsDTOS = new ArrayList<>();


        for (Teacher t : professors) {
            TeacherDTO teacherDTO = new TeacherDTO(
                t.getId(), t.getFirstName(), t.getLastName(),
                   new TitleDTO(new TitleTypeDTO(t.getTitle().getTitleType().getName()))
            );
            proffesorDTOS.add(teacherDTO);
        }

        for (Teacher t : assistants) {
            TeacherDTO teacherDTO = new TeacherDTO(
                    t.getId(), t.getFirstName(), t.getLastName(),
                    new TitleDTO(new TitleTypeDTO(t.getTitle().getTitleType().getName()))
            );
            assistantsDTOS.add(teacherDTO);
        }

        return new ResponseEntity<>(new ProfessorsAssistantsDTO(proffesorDTOS, assistantsDTOS), HttpStatus.OK);
    }

    @Transactional
    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<TeacherDTO> createTeacher(@RequestBody Teacher newTeacher) {
        Title title = titleService.save(newTeacher.getTitle());
        newTeacher.setTitle(title);
        List<Role> roles = new ArrayList<>();
        Role role = roleRepository.findByName("teacher").get();
        roles.add(role);
        User user = newTeacher.getUser();
        user.setRoles(roles);
        String encryptedPassword = new BCryptPasswordEncoder().encode(user.getPassword());
        user.setPassword(encryptedPassword);
        User newUser = userService.save(user);
        newTeacher.setUser(newUser);
        Teacher teacher = teacherService.save(newTeacher);

        TeacherDTO teacherDTO = new TeacherDTO(teacher.getId(), teacher.getJmbg(),
                teacher.getFirstName(), teacher.getLastName(), teacher.getBiography());

        return new ResponseEntity<>(teacherDTO, HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<TeacherDTO> getTeacher(@PathVariable("id") Long id) {
        Optional<Teacher> teacher = teacherService.findById(id);
        if (teacher.isPresent()) {
            TeacherDTO teacherDTO = new TeacherDTO();
            teacherDTO.setId(teacher.get().getId());
            teacherDTO.setJmbg(teacher.get().getJmbg());
            teacherDTO.setFirstName(teacher.get().getFirstName());
            teacherDTO.setLastName(teacher.get().getLastName());
            teacherDTO.setBiography(teacher.get().getBiography());
            return new ResponseEntity<>(teacherDTO, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<TeacherDTO> updateTeacher(@PathVariable("id") Long id, @RequestBody TeacherDTO newTeacher) {
        Optional<Teacher> teacher = teacherService.findById(id);

        if (teacher.isPresent()) {
            Teacher updatedTeacher = teacher.get();
            updatedTeacher.setId(id);
            updatedTeacher.setJmbg(newTeacher.getJmbg());
            updatedTeacher.setFirstName(newTeacher.getFirstName());
            updatedTeacher.setLastName(newTeacher.getLastName());
            updatedTeacher.setBiography(newTeacher.getBiography());
            teacherService.save(updatedTeacher);
            TeacherDTO teacherDTO = new TeacherDTO();
            teacherDTO.setId(updatedTeacher.getId());
            teacherDTO.setJmbg(updatedTeacher.getJmbg());
            teacherDTO.setFirstName(updatedTeacher.getFirstName());
            teacherDTO.setLastName(updatedTeacher.getLastName());
            teacherDTO.setBiography(updatedTeacher.getBiography());
            return new ResponseEntity<>(teacherDTO, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteTeacher(@PathVariable("id") Long id) {
        teacherService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
