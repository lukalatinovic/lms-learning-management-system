package rs.ac.singidunum.novisad.app.dto;

import jakarta.persistence.Column;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import rs.ac.singidunum.novisad.app.model.Notification;

@NoArgsConstructor
@AllArgsConstructor
public class FileDTO {

    private Long id;

    private String description;

    private String url;


    private Notification notification;

    public FileDTO(Long id, String description, String url) {
        this.id = id;
        this.description = description;
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }
}
