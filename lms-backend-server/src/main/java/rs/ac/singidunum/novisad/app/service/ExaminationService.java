package rs.ac.singidunum.novisad.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.singidunum.novisad.app.model.EvaluationType;
import rs.ac.singidunum.novisad.app.model.Examination;
import rs.ac.singidunum.novisad.app.repository.EvulationalTypeRepository;
import rs.ac.singidunum.novisad.app.repository.ExaminationRepository;

import java.util.Optional;

@Service
public class ExaminationService {

    @Autowired
    private ExaminationRepository examinationRepository;

    public Iterable<Examination> findAll(){
        return this.examinationRepository.findAll();
    }

    public Optional<Examination> findById(Long id) {
        return this.examinationRepository.findById(id);
    }

    public Examination save(Examination examination) {
        return this.examinationRepository.save(examination);
    }

    public void delete(Examination examination) {
        this.examinationRepository.delete(examination);
    }

    public void delete(Long id) {
        this.examinationRepository.deleteById(id);
    }
}
