package rs.ac.singidunum.novisad.app.repository;

import org.springframework.data.repository.CrudRepository;

import rs.ac.singidunum.novisad.app.model.Title;

public interface TitleRepository extends CrudRepository<Title, Integer>{

}
