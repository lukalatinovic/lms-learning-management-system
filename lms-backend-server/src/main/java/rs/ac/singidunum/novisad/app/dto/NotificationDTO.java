package rs.ac.singidunum.novisad.app.dto;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import rs.ac.singidunum.novisad.app.model.CourseRealization;
import rs.ac.singidunum.novisad.app.model.File;
import rs.ac.singidunum.novisad.app.model.TeacherAtImplementation;

import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
public class NotificationDTO {

    private Long id;

    private LocalDateTime postingDateTime;

    private String title;

    private String content;

    private List<File> attachments;

    private TeacherAtImplementation teacherAtImplementation;

    private CourseRealization courseRealization;

    public NotificationDTO(Long id, LocalDateTime postingDateTime, String title, String content) {
        this.id = id;
        this.postingDateTime = postingDateTime;
        this.title = title;
        this.content = content;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getPostingDateTime() {
        return postingDateTime;
    }

    public void setPostingDateTime(LocalDateTime postingDateTime) {
        this.postingDateTime = postingDateTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<File> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<File> attachments) {
        this.attachments = attachments;
    }

    public TeacherAtImplementation getTeacherAtImplementation() {
        return teacherAtImplementation;
    }

    public void setTeacherAtImplementation(TeacherAtImplementation teacherAtImplementation) {
        this.teacherAtImplementation = teacherAtImplementation;
    }

    public CourseRealization getCourseRealization() {
        return courseRealization;
    }

    public void setCourseRealization(CourseRealization courseRealization) {
        this.courseRealization = courseRealization;
    }
}
