package rs.ac.singidunum.novisad.app.repository;

import org.springframework.data.repository.CrudRepository;
import rs.ac.singidunum.novisad.app.model.EvaluationKnowledge;
import rs.ac.singidunum.novisad.app.model.EvaluationType;

public interface EvulationalTypeRepository extends CrudRepository<EvaluationType, Long> {
}
