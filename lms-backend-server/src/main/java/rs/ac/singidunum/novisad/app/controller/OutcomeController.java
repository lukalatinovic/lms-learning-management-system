package rs.ac.singidunum.novisad.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import rs.ac.singidunum.novisad.app.dto.OutcomeDTO;
import rs.ac.singidunum.novisad.app.model.Outcome;
import rs.ac.singidunum.novisad.app.service.OutcomeService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/outcomes")
public class OutcomeController {

    private OutcomeService outcomeService;

    @Autowired
    public OutcomeController(OutcomeService outcomeService) {
        this.outcomeService = outcomeService;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<OutcomeDTO>> findAllOutcomes() {
        List<OutcomeDTO> outcomeDTOList = new ArrayList<>();
        Iterable<Outcome> outcomes = outcomeService.findAll();

        for (Outcome outcome : outcomes) {
            outcomeDTOList.add(new OutcomeDTO(outcome.getDescription()));
        }

        return new ResponseEntity<>(outcomeDTOList, HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<OutcomeDTO> findOutcomeById(@PathVariable("id") Integer id) {
        Optional<Outcome> outcomeOptional = outcomeService.findById(id);
        if (outcomeOptional.isPresent()) {
            Outcome outcome = outcomeOptional.get();
            return new ResponseEntity<>(new OutcomeDTO(outcome.getDescription()), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<OutcomeDTO> createOutcome(@RequestBody OutcomeDTO outcomeDTO) {
        Outcome outcome = new Outcome(outcomeDTO.getDescription());
        outcome = outcomeService.save(outcome);
        return new ResponseEntity<>(new OutcomeDTO(outcome.getDescription()), HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<OutcomeDTO> updateOutcome(@PathVariable("id") Integer id, @RequestBody OutcomeDTO outcomeDTO) {
        Optional<Outcome> outcomeOptional = outcomeService.findById(id);
        if (outcomeOptional.isPresent()) {
            Outcome outcome = outcomeOptional.get();
            outcome.setDescription(outcomeDTO.getDescription());
            outcome = outcomeService.save(outcome);
            return new ResponseEntity<>(new OutcomeDTO(outcome.getDescription()), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteOutcome(@PathVariable("id") Integer id) {
        outcomeService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
