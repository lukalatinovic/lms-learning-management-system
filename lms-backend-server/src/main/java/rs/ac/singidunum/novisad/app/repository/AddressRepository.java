package rs.ac.singidunum.novisad.app.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.singidunum.novisad.app.model.Address;

public interface AddressRepository extends JpaRepository<Address, Integer> {
    Optional<Address> findByStreetAndNumberAndPlaceOfResidenceId(String street, String number, Integer placeOfResidenceId);
}

