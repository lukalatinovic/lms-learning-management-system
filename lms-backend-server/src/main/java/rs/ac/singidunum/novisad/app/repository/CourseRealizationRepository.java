package rs.ac.singidunum.novisad.app.repository;

import org.springframework.data.repository.CrudRepository;

import rs.ac.singidunum.novisad.app.model.CourseRealization;

public interface CourseRealizationRepository extends CrudRepository<CourseRealization, Integer>{

}
