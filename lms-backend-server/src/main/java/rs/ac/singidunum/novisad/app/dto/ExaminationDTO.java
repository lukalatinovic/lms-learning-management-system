package rs.ac.singidunum.novisad.app.dto;

import jakarta.persistence.Column;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import rs.ac.singidunum.novisad.app.model.EvaluationKnowledge;
import rs.ac.singidunum.novisad.app.model.StudentOnYear;

import java.util.ArrayList;
import java.util.List;

public class ExaminationDTO {

    private Long id;

    private double points;

    private StudentOnYear studentOnYear;

    private List<EvaluationKnowledge> evaluationKnowledgeList = new ArrayList<>();

    public ExaminationDTO(Long id, double points) {
        this.id = id;
        this.points = points;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getPoints() {
        return points;
    }

    public void setPoints(double points) {
        this.points = points;
    }

    public StudentOnYear getStudentOnYear() {
        return studentOnYear;
    }

    public void setStudentOnYear(StudentOnYear studentOnYear) {
        this.studentOnYear = studentOnYear;
    }

    public List<EvaluationKnowledge> getEvaluationKnowledgeList() {
        return evaluationKnowledgeList;
    }

    public void setEvaluationKnowledgeList(List<EvaluationKnowledge> evaluationKnowledgeList) {
        this.evaluationKnowledgeList = evaluationKnowledgeList;
    }
}
