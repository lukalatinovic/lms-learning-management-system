package rs.ac.singidunum.novisad.app.dto;

public class TeachingTypeDTO {

    private Integer id;
    private String name;

    public TeachingTypeDTO() {
    }

    public TeachingTypeDTO(String name) {
        this.name = name;
    }

    public TeachingTypeDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
