package rs.ac.singidunum.novisad.app.dto;

public class OutcomeDTO {

	private Integer id;
    private String description;

    public OutcomeDTO() {
    }

    public OutcomeDTO(String description) {
        this.description = description;
    }
    
    
    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
