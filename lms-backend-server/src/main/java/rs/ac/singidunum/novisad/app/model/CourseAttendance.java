package rs.ac.singidunum.novisad.app.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;

@Entity
public class CourseAttendance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private int finalGrade;

    @ManyToOne
    //@JoinColumn(name = "student_id", referencedColumnName = "id")
    private Student student;

    @ManyToOne
    //@JoinColumn(name = "course_realization_id", referencedColumnName = "id")
    private CourseRealization courseRealization;

    public CourseAttendance() {
    }

    public CourseAttendance(int finalGrade, Student student, CourseRealization courseRealization) {
        this.finalGrade = finalGrade;
        this.student = student;
        this.courseRealization = courseRealization;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getFinalGrade() {
        return finalGrade;
    }

    public void setFinalGrade(int finalGrade) {
        this.finalGrade = finalGrade;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public CourseRealization getCourseRealization() {
        return courseRealization;
    }

    public void setCourseRealization(CourseRealization courseRealization) {
        this.courseRealization = courseRealization;
    }
}
