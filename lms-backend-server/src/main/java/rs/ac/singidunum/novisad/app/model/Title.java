package rs.ac.singidunum.novisad.app.model;

import jakarta.persistence.*;

import java.util.Date;

@Entity
public class Title {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false)
	private Date electedDate;
	
	@Column(nullable = false)
	private Date expireDate;
	
	@ManyToOne
	private ScienceField scienceField;
	
	@ManyToOne
	private TitleType titleType;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getElectedDate() {
		return electedDate;
	}

	public void setElectedDate(Date electedDate) {
		this.electedDate = electedDate;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public ScienceField getScienceField() {
		return scienceField;
	}

	public void setScienceField(ScienceField scienceField) {
		this.scienceField = scienceField;
	}

	public TitleType getTitleType() {
		return titleType;
	}

	public void setTitleType(TitleType titleType) {
		this.titleType = titleType;
	}

	public Title() {
		super();
	}
	
	public Title(Integer id, Date electedDate, Date expireDate) {
		super();
		this.id = id;
		this.electedDate = electedDate;
		this.expireDate = expireDate;
	}

	public Title(Integer id, Date electedDate, Date expireDate, ScienceField scienceField, TitleType titleType) {
		super();
		this.id = id;
		this.electedDate = electedDate;
		this.expireDate = expireDate;
		this.scienceField = scienceField;
		this.titleType = titleType;
	}

	public Title(ScienceField scienceField, TitleType titleType) {
		this.scienceField = scienceField;
		this.titleType = titleType;
	}
}
