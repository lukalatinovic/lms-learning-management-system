package rs.ac.singidunum.novisad.app.dto;

public class TeacherAtImplementationDTO {

	private Integer id;
    private int numberOfClasses;

    public TeacherAtImplementationDTO() {
    }

    public TeacherAtImplementationDTO(int numberOfClasses) {
        this.numberOfClasses = numberOfClasses;
    }
    
    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getNumberOfClasses() {
        return numberOfClasses;
    }

    public void setNumberOfClasses(int numberOfClasses) {
        this.numberOfClasses = numberOfClasses;
    }
}
