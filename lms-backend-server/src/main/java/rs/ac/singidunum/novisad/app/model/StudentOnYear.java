package rs.ac.singidunum.novisad.app.model;

import jakarta.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class StudentOnYear {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    @Column(nullable = false)
    private Date dateOfEnroll;
    
    @Column(nullable = false)
    private String indexNumber;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "student_id", nullable = false)
    private Student student;

    
    @ManyToOne(fetch = FetchType.EAGER)
    private YearOfStudy yearOfStudy;


    @OneToMany(mappedBy = "studentOnYear")
    private List<Examination> examinationList;


    // Getters and Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateOfEnroll() {
        return dateOfEnroll;
    }

    public void setDateOfEnroll(Date dateOfEnroll) {
        this.dateOfEnroll = dateOfEnroll;
    }

    public String getIndexNumber() {
        return indexNumber;
    }

    public void setIndexNumber(String indexNumber) {
        this.indexNumber = indexNumber;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public YearOfStudy getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(YearOfStudy yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    public List<Examination> getExaminationList() {
        return examinationList;
    }

    public void setExaminationList(List<Examination> examinationList) {
        this.examinationList = examinationList;
    }
    
    // Constructors

    public StudentOnYear() {
        super();
    }

    public StudentOnYear(Integer id, Date dateOfEnroll, String indexNumber, Student student, YearOfStudy yearOfStudy) {
        super();
        this.id = id;
        this.dateOfEnroll = dateOfEnroll;
        this.indexNumber = indexNumber;
        this.student = student;
        this.yearOfStudy = yearOfStudy;
    }
}
