package rs.ac.singidunum.novisad.app.repository;

import org.springframework.data.repository.CrudRepository;

import rs.ac.singidunum.novisad.app.model.CourseAttendance;

public interface CourseAttendanceRepository extends CrudRepository<CourseAttendance, Integer>{

}
