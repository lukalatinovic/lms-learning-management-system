package rs.ac.singidunum.novisad.app.config.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import rs.ac.singidunum.novisad.app.service.AuthService;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration {

    String[] roles = new String[] {"user", "admin", "student", "teacher", "studentService"};

    @Autowired
    SecurityFilter securityFilter;

    @Autowired
    AuthService authService;

    /*
    We will have 5 roles in our app (user, student, teacher, admin, studentService)
    We should use exactly these names in our database while we adding roles
    */

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
        return httpSecurity
                .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .authorizeHttpRequests(authorize -> authorize
                        .requestMatchers(HttpMethod.GET, "/api/universities/**").permitAll()
                        .requestMatchers(HttpMethod.GET, "/api/teachers/**").permitAll()
                        .requestMatchers(HttpMethod.GET, "/api/faculties/**").permitAll()
                        .requestMatchers(HttpMethod.GET, "/api/addresses/**").permitAll()
                        .requestMatchers(HttpMethod.GET, "/api/placeOfResidence/**").permitAll()
                        .requestMatchers(HttpMethod.GET, "/api/countries/**").permitAll()
                        .requestMatchers(HttpMethod.GET, "/api/teaching-materials/**").permitAll()
                        .requestMatchers(HttpMethod.GET, "/api/students/**").hasAnyRole("studentService", "admin", "teacher")
                        .requestMatchers(HttpMethod.GET, "/api/users/**").hasAnyRole("admin", "studentService")
                        .requestMatchers(HttpMethod.GET, "/api/studyProgrammes/**").hasAnyRole("studentService", "admin", "teacher")
                        .requestMatchers(HttpMethod.GET, "/api/yearsOfStudy/**").hasAnyRole("studentService", "admin", "teacher")
                        .requestMatchers(HttpMethod.POST, "/api/auth/**").permitAll()
                        .requestMatchers(HttpMethod.POST, "/api/addresses/**").permitAll()
                        .requestMatchers(HttpMethod.POST, "/api/users/studentService").hasRole("admin")
                        .requestMatchers(HttpMethod.POST, "/api/teachers").hasRole("admin")
                        .requestMatchers(HttpMethod.POST, "/api/teaching-slots").hasRole("studentService")
                        .requestMatchers(HttpMethod.GET, "/api/subjects",
                                "/api/notifications",
                                "/api/course-attendances",
                                "/api/course-realizations").hasAnyRole("student", "studentService", "admin")
                        .requestMatchers("/api/subjects").hasAnyRole("teacher", "studentService", "admin")
                        .anyRequest().authenticated())
                .authenticationProvider(authenticationProvider())
                .formLogin(AbstractHttpConfigurer::disable)
                .csrf(AbstractHttpConfigurer::disable)
                .addFilterBefore(securityFilter, UsernamePasswordAuthenticationFilter.class)
                .build();
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return authService;
    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService());
        authenticationProvider.setPasswordEncoder(passwordEncoder());
        return authenticationProvider;
    }

    @Bean
    AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration)
            throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
