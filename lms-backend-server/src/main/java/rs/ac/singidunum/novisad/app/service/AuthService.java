package rs.ac.singidunum.novisad.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import rs.ac.singidunum.novisad.app.dto.SignUpDTO;
import rs.ac.singidunum.novisad.app.dto.UserDTO;
import rs.ac.singidunum.novisad.app.model.Role;
import rs.ac.singidunum.novisad.app.model.User;
import rs.ac.singidunum.novisad.app.repository.RoleRepository;
import rs.ac.singidunum.novisad.app.repository.UserRepository;

import java.util.Collections;
import java.util.Optional;

@Service
public class AuthService implements UserDetailsService {

    @Autowired
    UserRepository repository;

    @Autowired
    RoleRepository roleRepository;

    @Override
    public UserDetails loadUserByUsername(String username) {
        Optional<UserDetails> user = repository.findByUsername(username);
        if (user.isPresent()) {
            return user.get();
        }
        else {
            throw  new UsernameNotFoundException(username);
        }
    }

    public UserDetails signUp(UserDTO data) throws Exception {
        if (repository.findByUsername(data.getUsername()).isPresent()) {
            throw new Exception("Username already exists");
        }
        String encryptedPassword = new BCryptPasswordEncoder().encode(data.getPassword());
        Role roles = roleRepository.findByName("user").get();
        User newUser = new User(data.getUsername(), encryptedPassword, data.getEmail(), data.getDateOfBirth(), Collections.singletonList(roles));
        return repository.save(newUser);
    }
}