package rs.ac.singidunum.novisad.app.model;

import jakarta.persistence.*;

@Entity
public class TeacherAtImplementation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private int numberOfClasses;

    @ManyToOne
    //@JoinColumn(name = "teacher_id", nullable = false)
    private Teacher teacher;
    
    @ManyToOne
    //@JoinColumn(name = "teaching_type_id", nullable = false)
    private TeachingType teachingType;

    public TeacherAtImplementation() {
    }
    
    public TeacherAtImplementation(int numberOfClasses) {
        this.numberOfClasses = numberOfClasses;
    }

    public TeacherAtImplementation(Integer id, int numberOfClasses, Teacher teacher, TeachingType teachingType) {
        this.id = id;
        this.numberOfClasses = numberOfClasses;
        this.teacher = teacher;
        this.teachingType = teachingType;
    }

    public TeacherAtImplementation(int numberOfClasses, Teacher teacher, TeachingType teachingType) {
        this.numberOfClasses = numberOfClasses;
        this.teacher = teacher;
        this.teachingType = teachingType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getNumberOfClasses() {
        return numberOfClasses;
    }

    public void setNumberOfClasses(int numberOfClasses) {
        this.numberOfClasses = numberOfClasses;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public TeachingType getTeachingType() {
        return teachingType;
    }

    public void setTeachingType(TeachingType teachingType) {
        this.teachingType = teachingType;
    }
}
