package rs.ac.singidunum.novisad.app.model;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Examination {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private double points;

    @ManyToOne
    private StudentOnYear studentOnYear;

    @ManyToMany(mappedBy = "examinations")
    private List<EvaluationKnowledge> evaluationKnowledgeList = new ArrayList<>();

    public Examination(Long id, double points, StudentOnYear studentOnYear, List<EvaluationKnowledge> evaluationKnowledgeList) {
        this.id = id;
        this.points = points;
        this.studentOnYear = studentOnYear;
        this.evaluationKnowledgeList = evaluationKnowledgeList;
    }

    public Examination() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getPoints() {
        return points;
    }

    public void setPoints(double points) {
        this.points = points;
    }

    public StudentOnYear getStudentOnYear() {
        return studentOnYear;
    }

    public void setStudentOnYear(StudentOnYear studentOnYear) {
        this.studentOnYear = studentOnYear;
    }

    public List<EvaluationKnowledge> getEvaluationKnowledgeList() {
        return evaluationKnowledgeList;
    }

    public void setEvaluationKnowledgeList(List<EvaluationKnowledge> evaluationKnowledgeList) {
        this.evaluationKnowledgeList = evaluationKnowledgeList;
    }
}
