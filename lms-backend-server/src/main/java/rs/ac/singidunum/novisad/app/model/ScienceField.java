package rs.ac.singidunum.novisad.app.model;

import jakarta.persistence.*;

import java.util.List;

@Entity
public class ScienceField {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false, length = 20)
	private String name;
	
	@OneToMany(mappedBy = "scienceField")
	private List<Title> titles;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public ScienceField() {
		super();
	}

	public ScienceField(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public List<Title> getTitles() {
		return titles;
	}

	public void setTitles(List<Title> titles) {
		this.titles = titles;
	}
}
