package rs.ac.singidunum.novisad.app.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Entity
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDateTime postingDateTime;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private String content;

    @OneToMany(mappedBy = "notification")
    private List<File> attachments;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "teacher_at_implementation_id", referencedColumnName = "id")
    private TeacherAtImplementation teacherAtImplementation;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "course_realization_id", referencedColumnName = "id")
    private CourseRealization courseRealization;

    public Notification() {

    }

    public Notification(Long id, LocalDateTime postingDateTime, String title, String content, List<File> attachments, TeacherAtImplementation teacherAtImplementation, CourseRealization courseRealization) {
        this.id = id;
        this.postingDateTime = postingDateTime;
        this.title = title;
        this.content = content;
        this.attachments = attachments;
        this.teacherAtImplementation = teacherAtImplementation;
        this.courseRealization = courseRealization;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getPostingDateTime() {
        return postingDateTime;
    }

    public void setPostingDateTime(LocalDateTime postingDateTime) {
        this.postingDateTime = postingDateTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<File> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<File> attachments) {
        this.attachments = attachments;
    }

    public TeacherAtImplementation getTeacherAtImplementation() {
        return teacherAtImplementation;
    }

    public void setTeacherAtImplementation(TeacherAtImplementation teacherAtImplementation) {
        this.teacherAtImplementation = teacherAtImplementation;
    }

    public CourseRealization getCourseRealization() {
        return courseRealization;
    }

    public void setCourseRealization(CourseRealization courseRealization) {
        this.courseRealization = courseRealization;
    }
}
