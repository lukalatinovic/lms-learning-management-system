package rs.ac.singidunum.novisad.app.repository;

import org.springframework.data.repository.CrudRepository;

import rs.ac.singidunum.novisad.app.model.Outcome;

public interface OutcomeRepository extends CrudRepository<Outcome, Integer>{

}
