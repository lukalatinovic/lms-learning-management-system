package rs.ac.singidunum.novisad.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.app.dto.CourseViewDTO;
import rs.ac.singidunum.novisad.app.model.Subject;
import rs.ac.singidunum.novisad.app.repository.SubjectRepository;

@Service
public class SubjectService {
	@Autowired
	private SubjectRepository subjectRepository;

	public Iterable<Subject> findAll(){
		return this.subjectRepository.findAll();
	}
	
	public Optional<Subject> findById(Integer id) {
		return this.subjectRepository.findById(id);
	}

	public Iterable<Subject> findSubjectsByTeacherId(Integer id){
		return this.subjectRepository.findSubjectsByTeacherId(id);
	}

	public Iterable<CourseViewDTO> getCourses() {
		return this.subjectRepository.getCourses();
	}
	
	public Subject save(Subject c) {
		return this.subjectRepository.save(c);
	}
	
	public void delete(Subject c) {
		this.subjectRepository.delete(c);
	}
	
	public void delete(Integer id) {
		this.subjectRepository.deleteById(id);
	}
}
