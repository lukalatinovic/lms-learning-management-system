package rs.ac.singidunum.novisad.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.singidunum.novisad.app.model.Role;
import rs.ac.singidunum.novisad.app.repository.RoleRepository;

@Service

public class RoleService {

    @Autowired
    RoleRepository roleRepository;

    public Iterable<Role> findAll() {
        return roleRepository.findAll();
    }
}
