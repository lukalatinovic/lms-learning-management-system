package rs.ac.singidunum.novisad.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import rs.ac.singidunum.novisad.app.dto.*;
import rs.ac.singidunum.novisad.app.model.Faculty;
import rs.ac.singidunum.novisad.app.service.FacultyService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Controller
@RequestMapping("/api/faculties")
public class FacultyController {

    private final FacultyService facultyService;

    @Autowired
    public FacultyController(FacultyService facultyService) {
        this.facultyService = facultyService;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<FacultyDTO>> findAll() {
        List<FacultyDTO> facultiesDTO = StreamSupport.stream(facultyService.findAll().spliterator(), false)
                .map(this::convertToDTO)
                .collect(Collectors.toList());

        return new ResponseEntity<>(facultiesDTO, HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<FacultyDTO> getFaculty(@PathVariable("id") Integer id) {
        Optional<Faculty> faculty = this.facultyService.findById(id);
        return faculty.map(value -> new ResponseEntity<>(convertToDTO(value), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    private FacultyDTO convertToDTO(Faculty faculty) {
        AddressDTO addressDTO = null;
        if (faculty.getAddress() != null) {
            addressDTO = new AddressDTO(
                faculty.getAddress().getId(),
                faculty.getAddress().getStreet(),
                faculty.getAddress().getNumber(),
                new PlaceOfResidenceDTO(
                    faculty.getAddress().getPlaceOfResidence().getId(),
                    faculty.getAddress().getPlaceOfResidence().getName(),
                    new CountryDTO(
                        faculty.getAddress().getPlaceOfResidence().getCountry().getId(),
                        faculty.getAddress().getPlaceOfResidence().getCountry().getName()
                    )
                )
            );
        }

        TeacherDTO deanDTO = faculty.getDean() != null ? 
            new TeacherDTO(faculty.getDean().getId(), faculty.getDean().getFirstName(), faculty.getDean().getLastName()) : null;

        List<StudyProgrammeDTO> studyProgrammeDTOs = faculty.getStudyProgrammes() != null ?
                faculty.getStudyProgrammes().stream()
                        .map(sp -> {
                            List<SubjectDTO> subjectsDTO = sp.getYearOfStudy() != null && sp.getYearOfStudy().getSubjects() != null
                                ? sp.getYearOfStudy().getSubjects().stream()
                                    .map(subject -> new SubjectDTO(subject.getId(), subject.getName(), subject.getEspb(), subject.getMandatory()))
                                    .collect(Collectors.toList())
                                : null;

                            return new StudyProgrammeDTO(
                                sp.getId(), 
                                sp.getName(), 
                                sp.getCoordinator() != null ? new TeacherDTO(sp.getCoordinator().getId(), sp.getCoordinator().getFirstName(), sp.getCoordinator().getLastName()) : null,
                                sp.getFaculty() != null ? new FacultyDTO(sp.getFaculty().getId(), sp.getFaculty().getName()) : null,
                                sp.getYearOfStudy() != null ? new YearOfStudyDTO(sp.getYearOfStudy().getId(), sp.getYearOfStudy().getYear()) : null,
                                subjectsDTO
                            );
                        })
                        .collect(Collectors.toList()) : null;

        UniversityDTO universityDTO = faculty.getUniversity() != null ? 
            new UniversityDTO(faculty.getUniversity().getId(), faculty.getUniversity().getName()) : null;

        return new FacultyDTO(faculty.getId(), faculty.getName(), addressDTO, deanDTO, studyProgrammeDTOs, universityDTO);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<FacultyDTO> createFaculty(@RequestBody FacultyDTO newFaculty) {
        Faculty faculty = new Faculty();
        // Postavljanje atributa na osnovu DTO-a (ukoliko je potrebno)
        faculty = this.facultyService.save(faculty);
        return new ResponseEntity<>(convertToDTO(faculty), HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<FacultyDTO> updateFaculty(@PathVariable("id") Integer id, @RequestBody FacultyDTO newFaculty) {
        Optional<Faculty> facultyOptional = this.facultyService.findById(id);

        if (facultyOptional.isPresent()) {
            Faculty faculty = facultyOptional.get();
            faculty.setName(newFaculty.getName());
            // Ažuriranje ostalih polja (adresa, dekan, studijski programi, univerzitet) ako je potrebno
            this.facultyService.save(faculty);
            return new ResponseEntity<>(convertToDTO(faculty), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteFaculty(@PathVariable("id") Integer id) {
        Optional<Faculty> facultyOptional = this.facultyService.findById(id);
        if (facultyOptional.isPresent()) {
            this.facultyService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
