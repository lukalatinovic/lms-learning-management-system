package rs.ac.singidunum.novisad.app.dto;

public class TitleTypeDTO {
	private Integer id;
	private String name;
	private TitleDTO title;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public TitleDTO getTitle() {
		return title;
	}
	public void setTitle(TitleDTO title) {
		this.title = title;
	}
	
	public TitleTypeDTO() {
		super();
	}
	
	public TitleTypeDTO(String name) {
		super();
		this.name = name;
	}
	
	public TitleTypeDTO(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}	
	
	public TitleTypeDTO(Integer id, String name, TitleDTO title) {
		super();
		this.id = id;
		this.name = name;
		this.title = title;
	}
}
