package rs.ac.singidunum.novisad.app.dto;

import java.util.Date;

public class TitleDTO {
	private Integer id;
	private Date electedDate;
	private Date expireDate;
	private ScienceFieldDTO scienceField;
	private TitleTypeDTO titleType;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Date getElectedDate() {
		return electedDate;
	}

	public void setElectedDate(Date electedDate) {
		this.electedDate = electedDate;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public ScienceFieldDTO getScienceField() {
		return scienceField;
	}

	public void setScienceField(ScienceFieldDTO scienceField) {
		this.scienceField = scienceField;
	}

	public TitleTypeDTO getTitleType() {
		return titleType;
	}

	public void setTitleType(TitleTypeDTO titleType) {
		this.titleType = titleType;
	}

	public TitleDTO() {
		super();
	}

	public TitleDTO(Date electedDate, Date expireDate) {
		super();
		this.electedDate = electedDate;
		this.expireDate = expireDate;
	}

	public TitleDTO(Integer id, Date electedDate, Date expireDate) {
		super();
		this.id = id;
		this.electedDate = electedDate;
		this.expireDate = expireDate;
	}	
	
	public TitleDTO(Integer id, Date electedDate, Date expireDate, ScienceFieldDTO scienceField, TitleTypeDTO titleType) {
		super();
		this.id = id;
		this.electedDate = electedDate;
		this.expireDate = expireDate;
		this.scienceField = scienceField;
		this.titleType = titleType;
	}

	public TitleDTO(TitleTypeDTO titleType) {
		this.titleType = titleType;
	}
}
