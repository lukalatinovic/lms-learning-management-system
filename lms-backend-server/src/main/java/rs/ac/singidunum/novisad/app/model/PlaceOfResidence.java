package rs.ac.singidunum.novisad.app.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;

@Entity
public class PlaceOfResidence {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false, length = 30)
	private String name;
	
	@ManyToOne // Promenjen odnos
	private Country country;

    // Bez mappedBy za Address jer nije dvosmerna veza u ovom kontekstu

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public PlaceOfResidence() {
        super();
    }

    public PlaceOfResidence(Integer id, String name) {
        super();
        this.id = id;
        this.name = name;
    }
    
    public PlaceOfResidence(Integer id, String name, Country country) {
        this.id = id;
        this.name = name;
        this.country = country;
    }
}
