package rs.ac.singidunum.novisad.app.repository;

import org.springframework.data.repository.CrudRepository;

import rs.ac.singidunum.novisad.app.model.StudyProgramme;

public interface StudyProgrammeRepository extends CrudRepository<StudyProgramme, Integer>{

}