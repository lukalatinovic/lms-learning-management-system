package rs.ac.singidunum.novisad.app.model;

import jakarta.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class YearOfStudy {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    @Column(nullable = false)
    private Date year;
    
    @ManyToOne(fetch = FetchType.EAGER)
    private StudyProgramme studyProgramme;
    
    @OneToMany(mappedBy = "yearOfStudy")
    private List<StudentOnYear> studentOnYear;

    @OneToMany(mappedBy = "yearOfStudy", fetch = FetchType.LAZY)
    private List<Subject> subjects;


    public YearOfStudy() {
        super();
    }

    public YearOfStudy(Integer id, Date year, StudyProgramme studyProgramme, List<StudentOnYear> studentOnYear) {
        super();
        this.id = id;
        this.year = year;
        this.studyProgramme = studyProgramme;
        this.studentOnYear = studentOnYear;
    }

    public YearOfStudy(Date year) {
        this.year = year;
    }

    // Getters and Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getYear() {
        return year;
    }

    public void setYear(Date year) {
        this.year = year;
    }

    public StudyProgramme getStudyProgramme() {
        return studyProgramme;
    }

    public void setStudyProgramme(StudyProgramme studyProgramme) {
        this.studyProgramme = studyProgramme;
    }

    public List<StudentOnYear> getStudentOnYear() {
        return studentOnYear;
    }

    public void setStudentOnYear(List<StudentOnYear> studentOnYear) {
        this.studentOnYear = studentOnYear;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }
}
