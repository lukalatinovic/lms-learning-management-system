package rs.ac.singidunum.novisad.app.repository;

import org.springframework.data.repository.CrudRepository;
import rs.ac.singidunum.novisad.app.model.EvaluationKnowledge;

public interface EvaluationalKnowledgeRepository extends CrudRepository<EvaluationKnowledge, Long> {
}
