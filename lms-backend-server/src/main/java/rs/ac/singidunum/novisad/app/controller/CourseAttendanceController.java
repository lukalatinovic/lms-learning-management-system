package rs.ac.singidunum.novisad.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import rs.ac.singidunum.novisad.app.dto.CourseAttendanceDTO;
import rs.ac.singidunum.novisad.app.dto.CourseRealizationDTO;
import rs.ac.singidunum.novisad.app.dto.SubjectDTO;
import rs.ac.singidunum.novisad.app.model.CourseAttendance;
import rs.ac.singidunum.novisad.app.model.CourseRealization;
import rs.ac.singidunum.novisad.app.model.Student;
import rs.ac.singidunum.novisad.app.service.CourseAttendanceService;
import rs.ac.singidunum.novisad.app.service.CourseRealizationService;
import rs.ac.singidunum.novisad.app.service.StudentService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/course-attendances")
public class CourseAttendanceController {

    private CourseAttendanceService courseAttendanceService;
    private StudentService studentService;
    private CourseRealizationService courseRealizationService;

    @Autowired
    public CourseAttendanceController(CourseAttendanceService courseAttendanceService, StudentService studentService, CourseRealizationService courseRealizationService) {
        this.courseAttendanceService = courseAttendanceService;
        this.studentService = studentService;
        this.courseRealizationService = courseRealizationService;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<CourseAttendanceDTO>> findAll() {
        List<CourseAttendanceDTO> courseAttendanceDTOList = new ArrayList<>();
        Iterable<CourseAttendance> courseAttendances = courseAttendanceService.findAll();

        for (CourseAttendance courseAttendance : courseAttendances) {
            CourseRealizationDTO courseRealizationDTO = new CourseRealizationDTO(
                courseAttendance.getCourseRealization().getId(),
                new SubjectDTO(
                    courseAttendance.getCourseRealization().getSubject().getId(),
                    courseAttendance.getCourseRealization().getSubject().getName()
                )
            );

            courseAttendanceDTOList.add(new CourseAttendanceDTO(
                courseAttendance.getId(),
                courseAttendance.getFinalGrade(),
                courseAttendance.getStudent().getId(),
                courseAttendance.getCourseRealization().getId(),
                courseRealizationDTO
            ));
        }

        return new ResponseEntity<>(courseAttendanceDTOList, HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<CourseAttendanceDTO> createCourseAttendance(@RequestBody CourseAttendanceDTO newCourseAttendance) {
        if (newCourseAttendance.getStudentId() == null || newCourseAttendance.getCourseRealizationId() == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Optional<Student> studentOptional = studentService.findById(newCourseAttendance.getStudentId());
        Optional<CourseRealization> courseRealizationOptional = courseRealizationService.findById(newCourseAttendance.getCourseRealizationId());

        if (!studentOptional.isPresent() || !courseRealizationOptional.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        CourseAttendance courseAttendance = new CourseAttendance(
            newCourseAttendance.getFinalGrade(),
            studentOptional.get(),
            courseRealizationOptional.get()
        );

        courseAttendance = courseAttendanceService.save(courseAttendance);

        CourseRealizationDTO courseRealizationDTO = new CourseRealizationDTO(
            courseAttendance.getCourseRealization().getId(),
            new SubjectDTO(
                courseAttendance.getCourseRealization().getSubject().getId(),
                courseAttendance.getCourseRealization().getSubject().getName()
            )
        );

        return new ResponseEntity<>(new CourseAttendanceDTO(
            courseAttendance.getId(),
            courseAttendance.getFinalGrade(),
            courseAttendance.getStudent().getId(),
            courseAttendance.getCourseRealization().getId(),
            courseRealizationDTO
        ), HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<CourseAttendanceDTO> getCourseAttendanceById(@PathVariable("id") Integer id) {
        Optional<CourseAttendance> courseAttendance = courseAttendanceService.findById(id);
        if (courseAttendance.isPresent()) {
            CourseAttendance ca = courseAttendance.get();
            CourseRealizationDTO courseRealizationDTO = new CourseRealizationDTO(
                ca.getCourseRealization().getId(),
                new SubjectDTO(
                    ca.getCourseRealization().getSubject().getId(),
                    ca.getCourseRealization().getSubject().getName()
                )
            );

            return new ResponseEntity<>(new CourseAttendanceDTO(
                ca.getId(),
                ca.getFinalGrade(),
                ca.getStudent().getId(),
                ca.getCourseRealization().getId(),
                courseRealizationDTO
            ), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<CourseAttendanceDTO> updateCourseAttendance(@PathVariable("id") Integer id, @RequestBody CourseAttendanceDTO newCourseAttendance) {
        Optional<CourseAttendance> courseAttendance = courseAttendanceService.findById(id);

        if (courseAttendance.isPresent()) {
            CourseAttendance ca = courseAttendance.get();
            ca.setFinalGrade(newCourseAttendance.getFinalGrade());
            courseAttendanceService.save(ca);
            CourseRealizationDTO courseRealizationDTO = new CourseRealizationDTO(
                ca.getCourseRealization().getId(),
                new SubjectDTO(
                    ca.getCourseRealization().getSubject().getId(),
                    ca.getCourseRealization().getSubject().getName()
                )
            );

            return new ResponseEntity<>(new CourseAttendanceDTO(
                ca.getId(),
                ca.getFinalGrade(),
                ca.getStudent().getId(),
                ca.getCourseRealization().getId(),
                courseRealizationDTO
            ), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteCourseAttendance(@PathVariable("id") Integer id) {
        courseAttendanceService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
