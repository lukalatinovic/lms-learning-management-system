package rs.ac.singidunum.novisad.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.app.model.CourseRealization;
import rs.ac.singidunum.novisad.app.repository.CourseRealizationRepository;

@Service
public class CourseRealizationService {
	@Autowired
	private CourseRealizationRepository courseRealizationRepository;

	public Iterable<CourseRealization> findAll(){
		return this.courseRealizationRepository.findAll();
	}
	
	public Optional<CourseRealization> findById(Integer id) {
		return this.courseRealizationRepository.findById(id);
	}
	
	public CourseRealization save(CourseRealization c) {
		return this.courseRealizationRepository.save(c);
	}
	
	public void delete(CourseRealization c) {
		this.courseRealizationRepository.delete(c);
	}
	
	public void delete(Integer id) {
		this.courseRealizationRepository.deleteById(id);
	}
}
