package rs.ac.singidunum.novisad.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import rs.ac.singidunum.novisad.app.dto.TeacherAtImplementationDTO;
import rs.ac.singidunum.novisad.app.model.TeacherAtImplementation;
import rs.ac.singidunum.novisad.app.service.TeacherAtImplementationService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/teachers-at-implementations")
public class TeacherAtImplementationController {

    private TeacherAtImplementationService teacherAtImplementationService;

    @Autowired
    public TeacherAtImplementationController(TeacherAtImplementationService teacherAtImplementationService) {
        this.teacherAtImplementationService = teacherAtImplementationService;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<List<TeacherAtImplementationDTO>> findAllTeachersAtImplementations() {
        List<TeacherAtImplementationDTO> teacherAtImplementationDTOList = new ArrayList<>();
        Iterable<TeacherAtImplementation> teachersAtImplementations = teacherAtImplementationService.findAll();

        for (TeacherAtImplementation teacherAtImplementation : teachersAtImplementations) {
            teacherAtImplementationDTOList.add(new TeacherAtImplementationDTO(teacherAtImplementation.getNumberOfClasses()));
        }

        return new ResponseEntity<>(teacherAtImplementationDTOList, HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<TeacherAtImplementationDTO> findTeacherAtImplementationById(@PathVariable("id") Integer id) {
        Optional<TeacherAtImplementation> teacherAtImplementation = teacherAtImplementationService.findById(id);
        if (teacherAtImplementation.isPresent()) {
            return new ResponseEntity<>(new TeacherAtImplementationDTO(teacherAtImplementation.get().getNumberOfClasses()), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<TeacherAtImplementationDTO> createTeacherAtImplementation(@RequestBody TeacherAtImplementationDTO teacherAtImplementationDTO) {
        TeacherAtImplementation teacherAtImplementation = teacherAtImplementationService.save(new TeacherAtImplementation(teacherAtImplementationDTO.getNumberOfClasses()));
        return new ResponseEntity<>(new TeacherAtImplementationDTO(teacherAtImplementation.getNumberOfClasses()), HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<TeacherAtImplementationDTO> updateTeacherAtImplementation(@PathVariable("id") Integer id, @RequestBody TeacherAtImplementationDTO teacherAtImplementationDTO) {
        Optional<TeacherAtImplementation> existingTeacherAtImplementation = teacherAtImplementationService.findById(id);

        if (existingTeacherAtImplementation.isPresent()) {
            TeacherAtImplementation updatedTeacherAtImplementation = existingTeacherAtImplementation.get();
            updatedTeacherAtImplementation.setNumberOfClasses(teacherAtImplementationDTO.getNumberOfClasses());
            teacherAtImplementationService.save(updatedTeacherAtImplementation);
            return new ResponseEntity<>(new TeacherAtImplementationDTO(updatedTeacherAtImplementation.getNumberOfClasses()), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteTeacherAtImplementation(@PathVariable("id") Integer id) {
        teacherAtImplementationService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
