package rs.ac.singidunum.novisad.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.app.model.TeachingType;
import rs.ac.singidunum.novisad.app.repository.TeachingTypeRepository;

@Service
public class TeachingTypeService {
	@Autowired
	private TeachingTypeRepository teachingTypeRepository;

	public Iterable<TeachingType> findAll(){
		return this.teachingTypeRepository.findAll();
	}
	
	public Optional<TeachingType> findById(Integer id) {
		return this.teachingTypeRepository.findById(id);
	}
	
	public TeachingType save(TeachingType t) {
		return this.teachingTypeRepository.save(t);
	}
	
	public void delete(TeachingType t) {
		this.teachingTypeRepository.delete(t);
	}
	
	public void delete(Integer id) {
		this.teachingTypeRepository.deleteById(id);
	}
}
