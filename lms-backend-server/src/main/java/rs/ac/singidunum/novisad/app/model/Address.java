package rs.ac.singidunum.novisad.app.model;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;

@Entity
@Table(name = "address", uniqueConstraints = @UniqueConstraint(columnNames = {"place_of_residence_id", "street", "number"}))
public class Address implements Serializable {

    private static final long serialVersionUID = 2314863837363621411L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 30)
    private String street;

    @Column(nullable = false)
    private String number;

    @ManyToOne
    @JoinColumn(name = "place_of_residence_id", referencedColumnName = "id", nullable = false)
    private PlaceOfResidence placeOfResidence;

    @OneToOne(mappedBy = "address")
    private University university;

    @OneToOne(mappedBy = "address")
    private Student student;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public PlaceOfResidence getPlaceOfResidence() {
        return placeOfResidence;
    }

    public void setPlaceOfResidence(PlaceOfResidence placeOfResidence) {
        this.placeOfResidence = placeOfResidence;
    }

    public University getUniversity() {
        return university;
    }

    public void setUniversity(University university) {
        this.university = university;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Address() {
        super();
    }

    public Address(Integer id, String street, String number) {
        super();
        this.id = id;
        this.street = street;
        this.number = number;
    }

    public Address(Integer id, String street, String number, PlaceOfResidence placeOfResidence, University university,
                   Student student) {
        super();
        this.id = id;
        this.street = street;
        this.number = number;
        this.placeOfResidence = placeOfResidence;
        this.university = university;
        this.student = student;
    }
}
