package rs.ac.singidunum.novisad.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.singidunum.novisad.app.model.Role;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(String name);
}
