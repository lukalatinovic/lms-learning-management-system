package rs.ac.singidunum.novisad.app.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import rs.ac.singidunum.novisad.app.model.Teacher;

public interface TeacherRepository extends CrudRepository<Teacher, Long>{

    @Query("SELECT t FROM Teacher t " +
            "JOIN Title tit ON t.title.id = tit.id " +
            "JOIN TitleType tt ON tit.titleType.id = tt.id " +
            "WHERE tt.name = 'Professor'")
    Iterable<Teacher> getAllProfessors();

    @Query("SELECT t FROM Teacher t " +
            "JOIN Title tit ON t.title.id = tit.id " +
            "JOIN TitleType tt ON tit.titleType.id = tt.id " +
            "WHERE tt.name = 'Assistant'")
    Iterable<Teacher> getAllAssistants();
}
