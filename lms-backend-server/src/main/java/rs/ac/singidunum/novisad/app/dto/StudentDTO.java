package rs.ac.singidunum.novisad.app.dto;

import java.util.List;

public class StudentDTO {
    private Integer id;
    private String jmbg;
    private String firstName;
    private String lastName;
    private AddressDTO address;
    private List<StudentOnYearDTO> studentOnYear;
    private List<CourseAttendanceDTO> courseAttendances;
    private String formattedAddress;
    private List<String> courseAttendancesFormatted;
    private UserDTO user;

    // Getters and Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getJmbg() {
        return jmbg;
    }

    public void setJmbg(String jmbg) {
        this.jmbg = jmbg;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }

    public List<StudentOnYearDTO> getStudentOnYear() {
        return studentOnYear;
    }

    public void setStudentOnYear(List<StudentOnYearDTO> studentOnYear) {
        this.studentOnYear = studentOnYear;
    }

    public List<CourseAttendanceDTO> getCourseAttendances() {
        return courseAttendances;
    }

    public void setCourseAttendances(List<CourseAttendanceDTO> courseAttendances) {
        this.courseAttendances = courseAttendances;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    public List<String> getCourseAttendancesFormatted() {
        return courseAttendancesFormatted;
    }

    public void setCourseAttendancesFormatted(List<String> courseAttendancesFormatted) {
        this.courseAttendancesFormatted = courseAttendancesFormatted;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    // Default constructor
    public StudentDTO() {
        super();
    }

    // Constructor with all fields
    public StudentDTO(Integer id, String jmbg, String firstName, String lastName, AddressDTO address, 
                      String formattedAddress, List<StudentOnYearDTO> studentOnYear, 
                      List<CourseAttendanceDTO> courseAttendances, UserDTO user) {
        this.id = id;
        this.jmbg = jmbg;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.formattedAddress = formattedAddress;
        this.studentOnYear = studentOnYear;
        this.courseAttendances = courseAttendances;
        this.user = user;
    }

    // Constructor with basic fields
    public StudentDTO(Integer id, String jmbg, String firstName, String lastName, AddressDTO address, 
                      String formattedAddress, List<StudentOnYearDTO> studentOnYear, UserDTO user) {
        this(id, jmbg, firstName, lastName, address, formattedAddress, studentOnYear, null, user);
    }
}