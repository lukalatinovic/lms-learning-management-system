package rs.ac.singidunum.novisad.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import rs.ac.singidunum.novisad.app.dto.AddressDTO;
import rs.ac.singidunum.novisad.app.dto.CountryDTO;
import rs.ac.singidunum.novisad.app.dto.PlaceOfResidenceDTO;
import rs.ac.singidunum.novisad.app.model.Address;
import rs.ac.singidunum.novisad.app.model.Country;
import rs.ac.singidunum.novisad.app.model.PlaceOfResidence;
import rs.ac.singidunum.novisad.app.service.AddressService;

@Controller
@RequestMapping("/api/addresses")
public class AddressController {

    private AddressService addressService;

    @Autowired
    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<AddressDTO>> findAll() {
        ArrayList<AddressDTO> addressesDTO = new ArrayList<>();
        Iterable<Address> addresses = addressService.findAll();

        for (Address a : addresses) {
            addressesDTO.add(new AddressDTO(a.getId(), a.getStreet(), a.getNumber()));
        }

        return new ResponseEntity<>(addressesDTO, HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<AddressDTO> createAddress(@RequestBody AddressDTO newAddress) {
        // Pronađite `PlaceOfResidence` iz `PlaceOfResidenceDTO`
        PlaceOfResidence placeOfResidence = new PlaceOfResidence(
            newAddress.getPlaceOfResidence().getId(),
            newAddress.getPlaceOfResidence().getName(),
            new Country(newAddress.getPlaceOfResidence().getCountry().getId(), newAddress.getPlaceOfResidence().getCountry().getName())
        );

        // Kreirajte `Address` sa svim poljima
        Address address = new Address(null, newAddress.getStreet(), newAddress.getNumber(), placeOfResidence, null, null);
        address = addressService.save(address);

        // Vratite kreirani `AddressDTO`
        return new ResponseEntity<>(new AddressDTO(address.getId(), address.getStreet(), address.getNumber(), 
            new PlaceOfResidenceDTO(
                address.getPlaceOfResidence().getId(), 
                address.getPlaceOfResidence().getName(), 
                new CountryDTO(
                    address.getPlaceOfResidence().getCountry().getId(), 
                    address.getPlaceOfResidence().getCountry().getName())
                )
            )
        , HttpStatus.CREATED);
    }


    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<AddressDTO> getAddress(@PathVariable("id") Integer id) {
        Optional<Address> address = addressService.findById(id);
        if (address.isPresent()) {
            return new ResponseEntity<>(
                    new AddressDTO(address.get().getId(), address.get().getStreet(), address.get().getNumber()),
                    HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<AddressDTO> updateAddress(@PathVariable("id") Integer id, @RequestBody AddressDTO newAddress) {
        Optional<Address> address = addressService.findById(id);

        if (address.isPresent()) {
            Address updatedAddress = address.get();
            updatedAddress.setStreet(newAddress.getStreet());
            updatedAddress.setNumber(newAddress.getNumber());
            addressService.save(updatedAddress);
            return new ResponseEntity<>(
                    new AddressDTO(updatedAddress.getId(), updatedAddress.getStreet(), updatedAddress.getNumber()),
                    HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<AddressDTO> deleteAddress(@PathVariable("id") Integer id) {
        Optional<Address> address = addressService.findById(id);
        if (address.isPresent()) {
            addressService.delete(address.get());
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
 // U AddressController klasi
    @RequestMapping(path = "/find", method = RequestMethod.GET)
    public ResponseEntity<AddressDTO> findAddressByDetails(
            @RequestParam("street") String street,
            @RequestParam("number") String number,
            @RequestParam("placeOfResidenceId") Integer placeOfResidenceId) {

        Optional<Address> address = addressService.findByDetails(street, number, placeOfResidenceId);
        if (address.isPresent()) {
            Address a = address.get();
            return new ResponseEntity<>(
                new AddressDTO(a.getId(), a.getStreet(), a.getNumber(), new PlaceOfResidenceDTO(a.getPlaceOfResidence().getId(), a.getPlaceOfResidence().getName(), new CountryDTO(a.getPlaceOfResidence().getCountry().getId(), a.getPlaceOfResidence().getCountry().getName()))),
                HttpStatus.OK
            );
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
