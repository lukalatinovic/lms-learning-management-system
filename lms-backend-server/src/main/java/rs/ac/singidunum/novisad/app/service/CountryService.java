package rs.ac.singidunum.novisad.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.app.model.Country;
import rs.ac.singidunum.novisad.app.repository.CountryRepository;

@Service
public class CountryService {
	@Autowired
	private CountryRepository countryRepository;

	public Iterable<Country> findAll() {
		return this.countryRepository.findAll();
	}

	public Optional<Country> findById(Integer id) {
		return this.countryRepository.findById(id);
	}

	public Country save(Country c) {
		return this.countryRepository.save(c);
	}

	public void delete(Country c) {
		this.countryRepository.delete(c);
	}

	public void delete(Integer id) {
		this.countryRepository.deleteById(id);
	}
}
