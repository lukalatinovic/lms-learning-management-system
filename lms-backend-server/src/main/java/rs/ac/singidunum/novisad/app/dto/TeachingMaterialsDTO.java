package rs.ac.singidunum.novisad.app.dto;

import java.time.LocalDateTime;

public class TeachingMaterialsDTO {

    private Long id;
    private String name;
    private String authors;
    private LocalDateTime yearOfPublication;

    public TeachingMaterialsDTO() {}

    public TeachingMaterialsDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public TeachingMaterialsDTO(Long id, String name, String authors, LocalDateTime yearOfPublication) {
        this.id = id;
        this.name = name;
        this.authors = authors;
        this.yearOfPublication = yearOfPublication;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public LocalDateTime getYearOfPublication() {
        return yearOfPublication;
    }

    public void setYearOfPublication(LocalDateTime yearOfPublication) {
        this.yearOfPublication = yearOfPublication;
    }
}
