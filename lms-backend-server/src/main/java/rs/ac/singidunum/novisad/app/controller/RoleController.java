package rs.ac.singidunum.novisad.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rs.ac.singidunum.novisad.app.dto.RoleDTO;
import rs.ac.singidunum.novisad.app.dto.TeacherDTO;
import rs.ac.singidunum.novisad.app.model.Role;
import rs.ac.singidunum.novisad.app.model.Teacher;
import rs.ac.singidunum.novisad.app.service.RoleService;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/roles")
public class RoleController {

    @Autowired
    RoleService roleService;

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<RoleDTO>> findAll() {
        List<RoleDTO> roleDTOS = new ArrayList<>();
        Iterable<Role> roles = roleService.findAll();

        for (Role role : roles) {
           roleDTOS.add(new RoleDTO(role.getId(), role.getName()));
        }

        return new ResponseEntity<>(roleDTOS, HttpStatus.OK);
    }
}
