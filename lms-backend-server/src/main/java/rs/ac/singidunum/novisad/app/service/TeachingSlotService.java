package rs.ac.singidunum.novisad.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.singidunum.novisad.app.model.TeachingMaterials;
import rs.ac.singidunum.novisad.app.model.TeachingSlot;
import rs.ac.singidunum.novisad.app.repository.TeachingMaterialsRepository;
import rs.ac.singidunum.novisad.app.repository.TeachingSlotRepository;

import java.util.Optional;

@Service
public class TeachingSlotService {

    @Autowired
    private TeachingSlotRepository teachingSlotRepository;

    public Iterable<TeachingSlot> findAll(){
        return this.teachingSlotRepository.findAll();
    }

    public Optional<TeachingSlot> findById(Long id) {
        return this.teachingSlotRepository.findById(id);
    }

    public Iterable<TeachingSlot> getAllTeacherSlots(Long id){
        return this.teachingSlotRepository.getAllTeacherSlots(id);
    }

    public TeachingSlot save(TeachingSlot teachingSlot) {
        return this.teachingSlotRepository.save(teachingSlot);
    }

    public void delete(TeachingSlot teachingSlot) {
        this.teachingSlotRepository.delete(teachingSlot);
    }

    public void delete(Long id) {
        this.teachingSlotRepository.deleteById(id);
    }
}
