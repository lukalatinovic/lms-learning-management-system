package rs.ac.singidunum.novisad.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.app.model.StudentOnYear;
import rs.ac.singidunum.novisad.app.repository.StudentOnYearRepository;

@Service
public class StudentOnYearService {
	@Autowired
	private StudentOnYearRepository studentOnYearRepository;

	public Iterable<StudentOnYear> findAll(){
		return this.studentOnYearRepository.findAll();
	}
	
	public Optional<StudentOnYear> findById(Integer id) {
		return this.studentOnYearRepository.findById(id);
	}
	
	public List<StudentOnYear> findByStudentId(Integer studentId) {
        return studentOnYearRepository.findByStudentId(studentId);
    }
	
	public StudentOnYear save(StudentOnYear c) {
		return this.studentOnYearRepository.save(c);
	}
	
	public void delete(StudentOnYear c) {
		this.studentOnYearRepository.delete(c);
	}
	
	public void delete(Integer id) {
		this.studentOnYearRepository.deleteById(id);
	}
}
