package rs.ac.singidunum.novisad.app.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import java.util.List;

@Entity
public class Country {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false, length = 30)
	private String name;

    @OneToMany(mappedBy = "country")  // Promenjen odnos
    private List<PlaceOfResidence> placesOfResidence;

	public Integer getId() {
		return id;
	}

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public List<PlaceOfResidence> getPlacesOfResidence() {
        return placesOfResidence;
    }

    public void setPlacesOfResidence(List<PlaceOfResidence> placesOfResidence) {
        this.placesOfResidence = placesOfResidence;
    }

    public Country() {
        super();
    }

    public Country(Integer id, String name) {
        super();
        this.id = id;
        this.name = name;
    }
}
