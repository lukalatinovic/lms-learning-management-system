package rs.ac.singidunum.novisad.app.dto;

import java.util.List;

public class ProfessorsAssistantsDTO {

    private List<TeacherDTO> professors;

    private List<TeacherDTO> assistants;

    public ProfessorsAssistantsDTO(List<TeacherDTO> professors, List<TeacherDTO> assistants) {
        this.professors = professors;
        this.assistants = assistants;
    }

    public List<TeacherDTO> getProfessors() {
        return professors;
    }

    public void setProfessors(List<TeacherDTO> professors) {
        this.professors = professors;
    }

    public List<TeacherDTO> getAssistants() {
        return assistants;
    }

    public void setAssistants(List<TeacherDTO> assistants) {
        this.assistants = assistants;
    }
}
