package rs.ac.singidunum.novisad.app.dto;


public class JwtDTO {

    private String accessToken;

    public JwtDTO(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
