package rs.ac.singidunum.novisad.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestBody;

import rs.ac.singidunum.novisad.app.dto.*;
import rs.ac.singidunum.novisad.app.model.*;
import rs.ac.singidunum.novisad.app.service.StudentService;
import rs.ac.singidunum.novisad.app.service.AddressService;
import rs.ac.singidunum.novisad.app.service.StudentOnYearService;
import rs.ac.singidunum.novisad.app.service.UserService;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/students")
public class StudentController {

    private final StudentService studentService;
    private final StudentOnYearService studentOnYearService;
    private final UserService userService;
    private final AddressService addressService;

    @Autowired
    public StudentController(StudentService studentService, StudentOnYearService studentOnYearService, UserService userService, AddressService addressService) {
        this.studentService = studentService;
        this.studentOnYearService = studentOnYearService;
        this.userService = userService;
        this.addressService = addressService;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<StudentDTO>> findAll() {
        ArrayList<StudentDTO> studentsDTO = new ArrayList<>();
        Iterable<Student> students = studentService.findAll();

        for (Student s : students) {
            s.setStudentOnYear(studentOnYearService.findByStudentId(s.getId()));
            Address address = s.getAddress();
            CountryDTO countryDTO = new CountryDTO(
                address.getPlaceOfResidence().getCountry().getId(),
                address.getPlaceOfResidence().getCountry().getName()
            );
            PlaceOfResidenceDTO placeOfResidenceDTO = new PlaceOfResidenceDTO(
                address.getPlaceOfResidence().getId(),
                address.getPlaceOfResidence().getName(),
                countryDTO
            );
            AddressDTO addressDTO = new AddressDTO(
                address.getId(),
                address.getStreet(),
                address.getNumber(),
                placeOfResidenceDTO
            );
            String formattedAddress = String.format("%s, %s, %s",
                countryDTO.getName(),
                placeOfResidenceDTO.getName(),
                address.getStreet() + " " + address.getNumber()
            );

            List<StudentOnYearDTO> studentOnYearDTOs = new ArrayList<>();
            for (StudentOnYear soy : s.getStudentOnYear()) {
                YearOfStudy yos = soy.getYearOfStudy();
                StudyProgrammeDTO studyProgrammeDTO = null;

                if (yos != null) {
                    StudyProgramme sp = yos.getStudyProgramme();
                    if (sp != null) {
                        studyProgrammeDTO = new StudyProgrammeDTO(
                            sp.getId(),
                            sp.getName()
                        );
                    }
                }

                YearOfStudyDTO yearOfStudyDTO = new YearOfStudyDTO(
                    yos.getId(),
                    yos.getYear(),
                    studyProgrammeDTO
                );

                StudentOnYearDTO studentOnYearDTO = new StudentOnYearDTO(
                    soy.getId(),
                    soy.getDateOfEnroll(),
                    soy.getIndexNumber(),
                    soy.getStudent().getId(),
                    yearOfStudyDTO
                );

                studentOnYearDTOs.add(studentOnYearDTO);
            }

            List<CourseAttendanceDTO> courseAttendances = s.getCourseAttendances().stream()
                .map(ca -> new CourseAttendanceDTO(
                    ca.getId(),
                    ca.getFinalGrade(),
                    ca.getStudent().getId(),
                    ca.getCourseRealization().getId(),
                    new CourseRealizationDTO(
                        ca.getCourseRealization().getId(),
                        new SubjectDTO(
                            ca.getCourseRealization().getSubject().getId(),
                            ca.getCourseRealization().getSubject().getName()
                        )
                    )
                ))
                .collect(Collectors.toList());

            User user = s.getUser();
            List<Role> roles = user.getRoles();

            UserDTO userDTO = new UserDTO(
                user.getId(),
                user.getUsername(),
                user.getPassword(),
                user.getEmail(),
                user.getDateOfBirth(),
                roles
            );

            StudentDTO studentDTO = new StudentDTO(
                s.getId(),
                s.getJmbg(),
                s.getFirstName(),
                s.getLastName(),
                addressDTO,
                formattedAddress,
                studentOnYearDTOs,
                courseAttendances,
                userDTO
            );

            studentsDTO.add(studentDTO);
        }

        return new ResponseEntity<>(studentsDTO, HttpStatus.OK);
    }

    @RequestMapping(path = "/teacher/{id}", method = RequestMethod.GET)
    public ResponseEntity<Iterable<TeacherStudentsDTO>> findStudentsByTeacherId(
        @PathVariable("id") Integer id,
        @RequestParam(value = "subjectId", required = false) Long subjectId
    ) {
        Iterable<TeacherStudentsDTO> students = studentService.findStudentsByTeacherId(id, subjectId);
        return new ResponseEntity<>(students, HttpStatus.OK);
    }

    @Transactional
    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<StudentDTO> createStudent(@RequestBody StudentDTO newStudent) {
        try {
            // Kreiranje i validacija korisnika
            User user = userService.findById(newStudent.getUser().getId())
                .orElseThrow(() -> new RuntimeException("User not found"));

            // Kreiranje adrese ili pronalaženje postojeće
            Address address = addressService.findByDetails(
                newStudent.getAddress().getStreet(),
                newStudent.getAddress().getNumber(),
                newStudent.getAddress().getPlaceOfResidence().getId()
            ).orElseGet(() -> {
                Address newAddress = new Address();
                newAddress.setStreet(newStudent.getAddress().getStreet());
                newAddress.setNumber(newStudent.getAddress().getNumber());
                PlaceOfResidence placeOfResidence = new PlaceOfResidence();
                placeOfResidence.setId(newStudent.getAddress().getPlaceOfResidence().getId());
                newAddress.setPlaceOfResidence(placeOfResidence);
                return addressService.save(newAddress);
            });

            // Kreiranje studenta
            Student student = new Student();
            student.setJmbg(newStudent.getJmbg());
            student.setFirstName(newStudent.getFirstName());
            student.setLastName(newStudent.getLastName());
            student.setAddress(address);
            student.setUser(user);

            // Ako student nije validan, baca se greška koja će poništiti sve promene
            student = studentService.save(student);

            // Kreiranje i vraćanje DTO-a
            Address addressSaved = student.getAddress();
            CountryDTO countryDTO = new CountryDTO(
                addressSaved.getPlaceOfResidence().getCountry().getId(),
                addressSaved.getPlaceOfResidence().getCountry().getName()
            );
            PlaceOfResidenceDTO placeOfResidenceDTO = new PlaceOfResidenceDTO(
                addressSaved.getPlaceOfResidence().getId(),
                addressSaved.getPlaceOfResidence().getName(),
                countryDTO
            );
            AddressDTO addressDTO = new AddressDTO(
                addressSaved.getId(),
                addressSaved.getStreet(),
                addressSaved.getNumber(),
                placeOfResidenceDTO
            );
            String formattedAddress = String.format("%s, %s, %s",
                countryDTO.getName(),
                placeOfResidenceDTO.getName(),
                addressSaved.getStreet() + " " + addressSaved.getNumber()
            );

            // Ignorisanje courseAttendances
            List<CourseAttendanceDTO> courseAttendances = null;

            List<Role> roles = user.getRoles();

            UserDTO userDTO = new UserDTO(
                user.getId(),
                user.getUsername(),
                user.getPassword(),
                user.getEmail(),
                user.getDateOfBirth(),
                roles
            );

            // Učitaj studentOnYear za novog studenta
            List<StudentOnYear> studentOnYears = studentOnYearService.findByStudentId(student.getId());
            List<StudentOnYearDTO> studentOnYearDTOs = new ArrayList<>();
            for (StudentOnYear soy : studentOnYears) {
                YearOfStudy yos = soy.getYearOfStudy();
                StudyProgrammeDTO studyProgrammeDTO = null;

                if (yos != null) {
                    StudyProgramme sp = yos.getStudyProgramme();
                    if (sp != null) {
                        studyProgrammeDTO = new StudyProgrammeDTO(
                            sp.getId(),
                            sp.getName()
                        );
                    }
                }

                YearOfStudyDTO yearOfStudyDTO = new YearOfStudyDTO(
                    yos.getId(),
                    yos.getYear(),
                    studyProgrammeDTO
                );

                StudentOnYearDTO studentOnYearDTO = new StudentOnYearDTO(
                    soy.getId(),
                    soy.getDateOfEnroll(),
                    soy.getIndexNumber(),
                    soy.getStudent().getId(),
                    yearOfStudyDTO
                );

                studentOnYearDTOs.add(studentOnYearDTO);
            }

            StudentDTO studentDTO = new StudentDTO(
                student.getId(),
                student.getJmbg(),
                student.getFirstName(),
                student.getLastName(),
                addressDTO,
                formattedAddress,
                studentOnYearDTOs,
                courseAttendances,
                userDTO
            );

            return new ResponseEntity<>(studentDTO, HttpStatus.CREATED);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<StudentDTO> getStudent(@PathVariable("id") Integer id) {
        Optional<Student> student = this.studentService.findById(id);
        if (student.isPresent()) {
            Student s = student.get();
            s.setStudentOnYear(studentOnYearService.findByStudentId(s.getId()));
            Address address = s.getAddress();
            CountryDTO countryDTO = new CountryDTO(
                address.getPlaceOfResidence().getCountry().getId(),
                address.getPlaceOfResidence().getCountry().getName()
            );
            PlaceOfResidenceDTO placeOfResidenceDTO = new PlaceOfResidenceDTO(
                address.getPlaceOfResidence().getId(),
                address.getPlaceOfResidence().getName(),
                countryDTO
            );
            AddressDTO addressDTO = new AddressDTO(
                address.getId(),
                address.getStreet(),
                address.getNumber(),
                placeOfResidenceDTO
            );
            String formattedAddress = String.format("%s, %s, %s",
                countryDTO.getName(),
                placeOfResidenceDTO.getName(),
                address.getStreet() + " " + address.getNumber()
            );

            List<StudentOnYearDTO> studentOnYearDTOs = new ArrayList<>();
            for (StudentOnYear soy : s.getStudentOnYear()) {
                YearOfStudy yos = soy.getYearOfStudy();
                StudyProgramme sp = yos.getStudyProgramme();

                StudyProgrammeDTO studyProgrammeDTO = new StudyProgrammeDTO(
                    sp.getId(),
                    sp.getName()
                );

                YearOfStudyDTO yearOfStudyDTO = new YearOfStudyDTO(
                    yos.getId(),
                    yos.getYear(),
                    studyProgrammeDTO
                );

                StudentOnYearDTO studentOnYearDTO = new StudentOnYearDTO(
                    soy.getId(),
                    soy.getDateOfEnroll(),
                    soy.getIndexNumber(),
                    soy.getStudent().getId(),
                    yearOfStudyDTO
                );

                studentOnYearDTOs.add(studentOnYearDTO);
            }

            // Mapiranje courseAttendances
            List<CourseAttendanceDTO> courseAttendances = s.getCourseAttendances().stream()
                .map(ca -> new CourseAttendanceDTO(
                    ca.getId(),
                    ca.getFinalGrade(),
                    ca.getStudent().getId(),
                    ca.getCourseRealization().getId(),
                    new CourseRealizationDTO(
                        ca.getCourseRealization().getId(),
                        new SubjectDTO(
                            ca.getCourseRealization().getSubject().getId(),
                            ca.getCourseRealization().getSubject().getName()
                        )
                    )
                ))
                .collect(Collectors.toList());

            User user = s.getUser();
            List<Role> roles = user.getRoles();

            UserDTO userDTO = new UserDTO(
                user.getId(),
                user.getUsername(),
                user.getPassword(),
                user.getEmail(),
                user.getDateOfBirth(),
                roles
            );

            StudentDTO studentDTO = new StudentDTO(
                s.getId(),
                s.getJmbg(),
                s.getFirstName(),
                s.getLastName(),
                addressDTO,
                formattedAddress,
                studentOnYearDTOs,
                courseAttendances,
                userDTO
            );

            return new ResponseEntity<>(studentDTO, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<StudentDTO> updateStudent(@PathVariable("id") Integer id, @RequestBody StudentDTO newStudent) {
        Optional<Student> student = this.studentService.findById(id);

        if (student.isPresent()) {
            Student s = student.get();
            s.setJmbg(newStudent.getJmbg());
            s.setFirstName(newStudent.getFirstName());
            s.setLastName(newStudent.getLastName());

            if (newStudent.getAddress() != null) {
                Address address = new Address();
                address.setId(newStudent.getAddress().getId());
                // Postavljanje drugih svojstava samo ako postoje
                if (newStudent.getAddress().getPlaceOfResidence() != null) {
                    PlaceOfResidence placeOfResidence = new PlaceOfResidence();
                    placeOfResidence.setId(newStudent.getAddress().getPlaceOfResidence().getId());
                    if (newStudent.getAddress().getPlaceOfResidence().getCountry() != null) {
                        Country country = new Country();
                        country.setId(newStudent.getAddress().getPlaceOfResidence().getCountry().getId());
                        placeOfResidence.setCountry(country);
                    }
                    address.setPlaceOfResidence(placeOfResidence);
                }
                s.setAddress(address);
            }

            User user = userService.findById(newStudent.getUser().getId())
                .orElseThrow(() -> new RuntimeException("User not found"));
            s.setUser(user);

            this.studentService.save(s);
            s.setStudentOnYear(studentOnYearService.findByStudentId(s.getId()));
            Address addressSaved = s.getAddress();
            CountryDTO countryDTO = new CountryDTO(
                addressSaved.getPlaceOfResidence().getCountry().getId(),
                addressSaved.getPlaceOfResidence().getCountry().getName()
            );
            PlaceOfResidenceDTO placeOfResidenceDTO = new PlaceOfResidenceDTO(
                addressSaved.getPlaceOfResidence().getId(),
                addressSaved.getPlaceOfResidence().getName(),
                countryDTO
            );
            AddressDTO addressDTO = new AddressDTO(
                addressSaved.getId(),
                addressSaved.getStreet(),
                addressSaved.getNumber(),
                placeOfResidenceDTO
            );
            String formattedAddress = String.format("%s, %s, %s",
                countryDTO.getName(),
                placeOfResidenceDTO.getName(),
                addressSaved.getStreet() + " " + addressSaved.getNumber()
            );

            List<StudentOnYearDTO> studentOnYearDTOs = new ArrayList<>();
            for (StudentOnYear soy : s.getStudentOnYear()) {
                YearOfStudy yos = soy.getYearOfStudy();
                StudyProgramme sp = yos.getStudyProgramme();

                StudyProgrammeDTO studyProgrammeDTO = new StudyProgrammeDTO(
                    sp.getId(),
                    sp.getName()
                );

                YearOfStudyDTO yearOfStudyDTO = new YearOfStudyDTO(
                    yos.getId(),
                    yos.getYear(),
                    studyProgrammeDTO
                );

                StudentOnYearDTO studentOnYearDTO = new StudentOnYearDTO(
                    soy.getId(),
                    soy.getDateOfEnroll(),
                    soy.getIndexNumber(),
                    soy.getStudent().getId(),
                    yearOfStudyDTO
                );

                studentOnYearDTOs.add(studentOnYearDTO);
            }

            // Mapiranje courseAttendances
            List<CourseAttendanceDTO> courseAttendances = s.getCourseAttendances().stream()
                .map(ca -> new CourseAttendanceDTO(
                    ca.getId(),
                    ca.getFinalGrade(),
                    ca.getStudent().getId(),
                    ca.getCourseRealization().getId(),
                    new CourseRealizationDTO(
                        ca.getCourseRealization().getId(),
                        new SubjectDTO(
                            ca.getCourseRealization().getSubject().getId(),
                            ca.getCourseRealization().getSubject().getName()
                        )
                    )
                ))
                .collect(Collectors.toList());

            List<Role> roles = user.getRoles();

            UserDTO userDTO = new UserDTO(
                user.getId(),
                user.getUsername(),
                user.getPassword(),
                user.getEmail(),
                user.getDateOfBirth(),
                roles
            );

            StudentDTO studentDTO = new StudentDTO(
                s.getId(),
                s.getJmbg(),
                s.getFirstName(),
                s.getLastName(),
                addressDTO,
                formattedAddress,
                studentOnYearDTOs,
                courseAttendances,
                userDTO
            );

            return new ResponseEntity<>(studentDTO, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteStudent(@PathVariable("id") Integer id) {
        Optional<Student> student = this.studentService.findById(id);
        if (student.isPresent()) {
            this.studentService.delete(student.get());
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @RequestMapping(path = "/batchDelete", method = RequestMethod.POST)
    public ResponseEntity<Void> batchDeleteStudents(@RequestBody List<Long> ids) {
        try {
            studentService.deleteStudentsByIds(ids);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
