package rs.ac.singidunum.novisad.app.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Entity
public class TeachingType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @OneToMany(mappedBy = "teachingType")
    private List<TeachingSlot> teachingSlots;

    public TeachingType(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TeachingSlot> getTeachingSlots() {
        return teachingSlots;
    }

    public void setTeachingSlots(List<TeachingSlot> teachingSlots) {
        this.teachingSlots = teachingSlots;
    }
}
