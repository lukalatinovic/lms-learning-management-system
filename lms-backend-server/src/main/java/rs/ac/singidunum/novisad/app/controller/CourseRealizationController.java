package rs.ac.singidunum.novisad.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import rs.ac.singidunum.novisad.app.dto.CourseRealizationDTO;
import rs.ac.singidunum.novisad.app.dto.SubjectDTO;
import rs.ac.singidunum.novisad.app.model.CourseRealization;
import rs.ac.singidunum.novisad.app.service.CourseRealizationService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/course-realizations")
public class CourseRealizationController {

    private CourseRealizationService courseRealizationService;

    @Autowired
    public CourseRealizationController(CourseRealizationService courseRealizationService) {
        this.courseRealizationService = courseRealizationService;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<CourseRealizationDTO>> findAll(){
        List<CourseRealizationDTO> courseRealizationDTOs = new ArrayList<>();
        Iterable<CourseRealization> courseRealizations = this.courseRealizationService.findAll();

        for (CourseRealization cr : courseRealizations) {
            SubjectDTO subjectDTO = new SubjectDTO(
                cr.getSubject().getId(),
                cr.getSubject().getName()
            );
            courseRealizationDTOs.add(new CourseRealizationDTO(cr.getId(), subjectDTO));
        }

        return new ResponseEntity<>(courseRealizationDTOs, HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<CourseRealizationDTO> createCourseRealization(@RequestBody CourseRealization newCourseRealization) {
        if (newCourseRealization.getTeacher() == null || newCourseRealization.getSubject() == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        newCourseRealization = this.courseRealizationService.save(newCourseRealization);
        return new ResponseEntity<>(new CourseRealizationDTO(newCourseRealization.getId(),
                newCourseRealization.getSubject().getId(), newCourseRealization.getTeacher().getId()), HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<CourseRealizationDTO> getCourseRealization(@PathVariable("id") Integer id) {
        Optional<CourseRealization> courseRealization = this.courseRealizationService.findById(id);
        if (courseRealization.isPresent()) {
            SubjectDTO subjectDTO = new SubjectDTO(
                courseRealization.get().getSubject().getId(),
                courseRealization.get().getSubject().getName()
            );
            return new ResponseEntity<>(new CourseRealizationDTO(courseRealization.get().getId(), subjectDTO), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<CourseRealizationDTO> updateCourseRealization(@PathVariable("id") Integer id, @RequestBody CourseRealization newCourseRealization) {
        Optional<CourseRealization> courseRealizationOptional = this.courseRealizationService.findById(id);

        if (courseRealizationOptional.isPresent()) {
            CourseRealization courseRealization = this.courseRealizationService.save(newCourseRealization);
            return new ResponseEntity<>(new CourseRealizationDTO(courseRealization.getId(), courseRealization.getSubject().getId()), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteCourseRealization(@PathVariable("id") Integer id) {
        Optional<CourseRealization> courseRealizationOptional = this.courseRealizationService.findById(id);
        if (courseRealizationOptional.isPresent()) {
            this.courseRealizationService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
