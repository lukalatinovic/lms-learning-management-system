package rs.ac.singidunum.novisad.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import rs.ac.singidunum.novisad.app.dto.TeachingTypeDTO;
import rs.ac.singidunum.novisad.app.model.TeachingType;
import rs.ac.singidunum.novisad.app.service.TeachingTypeService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/teaching-types")
public class TeachingTypeController {

    private TeachingTypeService teachingTypeService;

    @Autowired
    public TeachingTypeController(TeachingTypeService teachingTypeService) {
        this.teachingTypeService = teachingTypeService;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<TeachingTypeDTO>> findAll() {
        List<TeachingTypeDTO> teachingTypesDTO = new ArrayList<>();
        Iterable<TeachingType> teachingTypes = this.teachingTypeService.findAll();

        for (TeachingType t : teachingTypes) {
            teachingTypesDTO.add(new TeachingTypeDTO(t.getId(), t.getName()));
        }

        return new ResponseEntity<>(teachingTypesDTO, HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<TeachingTypeDTO> createTeachingType(@RequestBody TeachingTypeDTO newTeachingType) {
        TeachingType teachingType = this.teachingTypeService.save(new TeachingType(newTeachingType.getName()));
        return new ResponseEntity<>(new TeachingTypeDTO(teachingType.getName()), HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<TeachingTypeDTO> getTeachingType(@PathVariable("id") Integer id) {
        Optional<TeachingType> teachingType = this.teachingTypeService.findById(id);
        if (teachingType.isPresent()) {
            return new ResponseEntity<>(
                    new TeachingTypeDTO(teachingType.get().getName()),
                    HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<TeachingTypeDTO> updateTeachingType(@PathVariable("id") Integer id, @RequestBody TeachingTypeDTO newTeachingType) {
        Optional<TeachingType> teachingType = this.teachingTypeService.findById(id);

        if (teachingType.isPresent()) {
            TeachingType updatedTeachingType = teachingType.get();
            updatedTeachingType.setName(newTeachingType.getName());
            this.teachingTypeService.save(updatedTeachingType);
            return new ResponseEntity<>(
                    new TeachingTypeDTO(updatedTeachingType.getName()),
                    HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteTeachingType(@PathVariable("id") Integer id) {
        teachingTypeService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
