package rs.ac.singidunum.novisad.app.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rs.ac.singidunum.novisad.app.dto.FileDTO;
import rs.ac.singidunum.novisad.app.dto.NotificationDTO;
import rs.ac.singidunum.novisad.app.model.File;
import rs.ac.singidunum.novisad.app.model.Notification;
import rs.ac.singidunum.novisad.app.service.FileService;
import rs.ac.singidunum.novisad.app.service.NotificationService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/notifications")
public class NotificationController {

    private NotificationService notificationService;

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<NotificationDTO>> findAll(){
        List<NotificationDTO> notificationDTOS = new ArrayList<>();
        Iterable<Notification> notifications = this.notificationService.findAll();

        for (Notification n : notifications) {
            notificationDTOS.add(new NotificationDTO(n.getId(), n.getPostingDateTime(), n.getTitle(), n.getContent()));
        }

        return new ResponseEntity<>(notificationDTOS, HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<NotificationDTO> create(@RequestBody NotificationDTO newNotificationDTO) {
        Notification notification = this.notificationService.save(
                new Notification(null, newNotificationDTO.getPostingDateTime(), newNotificationDTO.getTitle(),
                        newNotificationDTO.getContent(), newNotificationDTO.getAttachments(),
                        newNotificationDTO.getTeacherAtImplementation(), newNotificationDTO.getCourseRealization()));
        return new ResponseEntity<>(new NotificationDTO(notification.getId(), notification.getPostingDateTime(),
                notification.getTitle(), notification.getContent()),
                HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<NotificationDTO> getById(@PathVariable("id") Long id) {
        Optional<Notification> notification = this.notificationService.findById(id);
        if (notification.isPresent()) {
            Notification n = notification.get();
            return new ResponseEntity<>(new NotificationDTO(n.getId(), n.getPostingDateTime(),
                    n.getTitle(), n.getContent()), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<NotificationDTO> update(@PathVariable("id") Long id, @RequestBody NotificationDTO newNotificationDTO) {
        Optional<Notification> notificationOptional = this.notificationService.findById(id);

        if (notificationOptional.isPresent()) {
            Notification notification = notificationOptional.get();
            notification.setId(newNotificationDTO.getId());

            this.notificationService.save(notification);
            return new ResponseEntity<>(new NotificationDTO(notification.getId(), notification.getPostingDateTime(),
                    notification.getTitle(), notification.getContent()), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        Optional<Notification> notificationOptional = this.notificationService.findById(id);
        if (notificationOptional.isPresent()) {
            this.notificationService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
