package rs.ac.singidunum.novisad.app.repository;

import org.springframework.data.repository.CrudRepository;

import rs.ac.singidunum.novisad.app.model.TitleType;

public interface TitleTypeRepository extends CrudRepository<TitleType, Integer>{

}
