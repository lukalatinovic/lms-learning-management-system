package rs.ac.singidunum.novisad.app.model;

import jakarta.persistence.*;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@NoArgsConstructor
@Entity
public class TeachingSlot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDateTime startTime;

    @Column(nullable = false)
    private LocalDateTime endTime;

    @ManyToOne
    private TeachingType teachingType;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "outcome_id", referencedColumnName = "id")
    private Outcome outcome;

    @ManyToOne
    private CourseRealization courseRealization;

    public TeachingSlot(Long id, LocalDateTime startTime, LocalDateTime endTime, TeachingType teachingType, Outcome outcome, CourseRealization courseRealization) {
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
        this.teachingType = teachingType;
        this.outcome = outcome;
        this.courseRealization = courseRealization;
    }

    public TeachingSlot(LocalDateTime startTime, LocalDateTime endTime, TeachingType teachingType, CourseRealization courseRealization) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.teachingType = teachingType;
        this.courseRealization = courseRealization;
    }

    public TeachingSlot(Long id, LocalDateTime startTime, LocalDateTime endTime) {
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public TeachingType getTeachingType() {
        return teachingType;
    }

    public void setTeachingType(TeachingType teachingType) {
        this.teachingType = teachingType;
    }

    public Outcome getOutcome() {
        return outcome;
    }

    public void setOutcome(Outcome outcome) {
        this.outcome = outcome;
    }

    public CourseRealization getCourseRealization() {
        return courseRealization;
    }

    public void setCourseRealization(CourseRealization courseRealization) {
        this.courseRealization = courseRealization;
    }
}
