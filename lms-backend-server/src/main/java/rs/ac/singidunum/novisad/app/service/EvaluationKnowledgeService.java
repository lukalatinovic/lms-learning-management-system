package rs.ac.singidunum.novisad.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.singidunum.novisad.app.model.EducationalGoal;
import rs.ac.singidunum.novisad.app.model.EvaluationKnowledge;
import rs.ac.singidunum.novisad.app.repository.EvaluationalKnowledgeRepository;

import java.util.Optional;

@Service
public class EvaluationKnowledgeService {

    @Autowired
    private EvaluationalKnowledgeRepository evaluationalKnowledgeRepository;

    public Iterable<EvaluationKnowledge> findAll(){
        return this.evaluationalKnowledgeRepository.findAll();
    }

    public Optional<EvaluationKnowledge> findById(Long id) {
        return this.evaluationalKnowledgeRepository.findById(id);
    }

    public EvaluationKnowledge save(EvaluationKnowledge evaluationKnowledge) {
        return this.evaluationalKnowledgeRepository.save(evaluationKnowledge);
    }

    public void delete(EvaluationKnowledge evaluationKnowledge) {
        this.evaluationalKnowledgeRepository.delete(evaluationKnowledge);
    }

    public void delete(Long id) {
        this.evaluationalKnowledgeRepository.deleteById(id);
    }
}
