package rs.ac.singidunum.novisad.app.dto;

public class CourseRealizationDTO {
    private Integer id;
    private SubjectDTO subject;

	private Integer subjectId;
	private Long teacherId;


    public CourseRealizationDTO(Integer id, SubjectDTO subject) {
        super();
        this.id = id;
        this.subject = subject;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SubjectDTO getSubject() {
        return subject;
    }

    public void setSubject(SubjectDTO subject) {
        this.subject = subject;
    }
    
	public CourseRealizationDTO() {
		super();
	}
	public CourseRealizationDTO(Integer id, Integer subjectId, Long teacherId) {
			super();
			this.id = id;
			this.subjectId = subjectId;
			this.teacherId = teacherId;
		}

	public CourseRealizationDTO(Integer id, Integer subjectId) {
		this.id = id;
		this.subjectId = subjectId;
	}
	public Integer getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(Integer subjectId) {
		this.subjectId = subjectId;
	}

}
