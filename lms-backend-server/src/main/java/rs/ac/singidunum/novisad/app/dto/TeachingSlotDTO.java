package rs.ac.singidunum.novisad.app.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import rs.ac.singidunum.novisad.app.model.Outcome;
import rs.ac.singidunum.novisad.app.model.TeachingType;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
public class TeachingSlotDTO {

    private Long id;

    private LocalDateTime startTime;

    private LocalDateTime endTime;

    private TeachingType teachingType;

    private Outcome outcome;

    public TeachingSlotDTO(Long id, LocalDateTime startTime, LocalDateTime endTime) {
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public TeachingSlotDTO(Long id, LocalDateTime startTime, LocalDateTime endTime, TeachingType teachingType) {
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
        this.teachingType = teachingType;
    }

    public TeachingSlotDTO(Long id, LocalDateTime startTime, LocalDateTime endTime, Outcome outcome) {
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
        this.outcome = outcome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public TeachingType getTeachingType() {
        return teachingType;
    }

    public void setTeachingType(TeachingType teachingType) {
        this.teachingType = teachingType;
    }

    public Outcome getOutcome() {
        return outcome;
    }

    public void setOutcome(Outcome outcome) {
        this.outcome = outcome;
    }
}
