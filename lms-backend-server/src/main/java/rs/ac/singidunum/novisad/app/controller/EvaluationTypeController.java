package rs.ac.singidunum.novisad.app.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rs.ac.singidunum.novisad.app.dto.EvaluationKnowledgeDTO;
import rs.ac.singidunum.novisad.app.dto.EvaluationTypeDTO;
import rs.ac.singidunum.novisad.app.model.EvaluationKnowledge;
import rs.ac.singidunum.novisad.app.model.EvaluationType;
import rs.ac.singidunum.novisad.app.service.EvaluationKnowledgeService;
import rs.ac.singidunum.novisad.app.service.EvaluationTypeService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/evaluation-types")
public class EvaluationTypeController {
    private EvaluationTypeService evaluationTypeService;

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<EvaluationTypeDTO>> findAll(){
        List<EvaluationTypeDTO> evaluationTypeDTOS = new ArrayList<>();
        Iterable<EvaluationType> evaluationTypes = this.evaluationTypeService.findAll();

        for (EvaluationType et : evaluationTypes) {
            evaluationTypeDTOS.add(new EvaluationTypeDTO(et.getId(), et.getName()));
        }

        return new ResponseEntity<>(evaluationTypeDTOS, HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<EvaluationTypeDTO> create(@RequestBody EvaluationTypeDTO etDTO) {
        EvaluationType evaluationType = this.evaluationTypeService.save(
                new EvaluationType(null, etDTO.getName(), etDTO.getEvaluationKnowledgeList()));
        return new ResponseEntity<>(new EvaluationTypeDTO(evaluationType.getId(), evaluationType.getName()),
                HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<EvaluationTypeDTO> getById(@PathVariable("id") Long id) {
        Optional<EvaluationType> evaluationType = this.evaluationTypeService.findById(id);
        if (evaluationType.isPresent()) {
            EvaluationType ek = evaluationType.get();
            return new ResponseEntity<>(new EvaluationTypeDTO(ek.getId(), ek.getName()), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<EvaluationTypeDTO> update(@PathVariable("id") Long id, @RequestBody EvaluationKnowledgeDTO newEvaluationKnowledge) {
        Optional<EvaluationType> evaluationTypeOptional = this.evaluationTypeService.findById(id);

        if (evaluationTypeOptional.isPresent()) {
            EvaluationType evaluationType = evaluationTypeOptional.get();
            evaluationType.setId(newEvaluationKnowledge.getId());

            this.evaluationTypeService.save(evaluationType);
            return new ResponseEntity<>(new EvaluationTypeDTO(evaluationType.getId(), evaluationType.getName()), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        Optional<EvaluationType> evaluationTypeOptional = this.evaluationTypeService.findById(id);
        if (evaluationTypeOptional.isPresent()) {
            this.evaluationTypeService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
