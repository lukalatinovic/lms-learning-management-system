package rs.ac.singidunum.novisad.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.app.model.Address;
import rs.ac.singidunum.novisad.app.repository.AddressRepository;

@Service
public class AddressService {
	@Autowired
	private AddressRepository addressRepository;

	public Iterable<Address> findAll(){
		return this.addressRepository.findAll();
	}
	
	public Optional<Address> findById(Integer id) {
		return this.addressRepository.findById(id);
	}
	
	public Address save(Address a) {
		return this.addressRepository.save(a);
	}
	
	public void delete(Address a) {
		this.addressRepository.delete(a);
	}
	
	public void delete(Integer id) {
		this.addressRepository.deleteById(id);
	}
	
	public Optional<Address> findByDetails(String street, String number, Integer placeOfResidenceId) {
	    return addressRepository.findByStreetAndNumberAndPlaceOfResidenceId(street, number, placeOfResidenceId);
	}

}