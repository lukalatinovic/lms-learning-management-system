package rs.ac.singidunum.novisad.app.dto;

public class CourseAttendanceDTO {

    private Integer id;
    private int finalGrade;
    private Integer studentId;
    private Integer courseRealizationId;
    private CourseRealizationDTO courseRealization;

    public CourseAttendanceDTO() {
        super();
    }

    public CourseAttendanceDTO(Integer id, int finalGrade, Integer studentId, Integer courseRealizationId, CourseRealizationDTO courseRealization) {
        super();
        this.id = id;
        this.finalGrade = finalGrade;
        this.studentId = studentId;
        this.courseRealizationId = courseRealizationId;
        this.courseRealization = courseRealization;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getFinalGrade() {
        return finalGrade;
    }

    public void setFinalGrade(int finalGrade) {
        this.finalGrade = finalGrade;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public Integer getCourseRealizationId() {
        return courseRealizationId;
    }

    public void setCourseRealizationId(Integer courseRealizationId) {
        this.courseRealizationId = courseRealizationId;
    }

    public CourseRealizationDTO getCourseRealization() {
        return courseRealization;
    }

    public void setCourseRealization(CourseRealizationDTO courseRealization) {
        this.courseRealization = courseRealization;
    }
}
