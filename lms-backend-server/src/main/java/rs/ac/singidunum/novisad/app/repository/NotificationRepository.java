package rs.ac.singidunum.novisad.app.repository;

import org.springframework.data.repository.CrudRepository;
import rs.ac.singidunum.novisad.app.model.Notification;

public interface NotificationRepository extends CrudRepository<Notification, Long> {
}
