package rs.ac.singidunum.novisad.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.app.model.CourseAttendance;
import rs.ac.singidunum.novisad.app.repository.CourseAttendanceRepository;

@Service
public class CourseAttendanceService {
	@Autowired
	private CourseAttendanceRepository courseAttendanceRepository;

	public Iterable<CourseAttendance> findAll(){
		return this.courseAttendanceRepository.findAll();
	}
	
	public Optional<CourseAttendance> findById(Integer id) {
		return this.courseAttendanceRepository.findById(id);
	}
	
	public CourseAttendance save(CourseAttendance c) {
		return this.courseAttendanceRepository.save(c);
	}
	
	public void delete(CourseAttendance c) {
		this.courseAttendanceRepository.delete(c);
	}
	
	public void delete(Integer id) {
		this.courseAttendanceRepository.deleteById(id);
	}
}
