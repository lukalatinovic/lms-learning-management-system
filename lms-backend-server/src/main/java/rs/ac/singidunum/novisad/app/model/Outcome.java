package rs.ac.singidunum.novisad.app.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Outcome {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String description;

    @ManyToOne
    private EducationalGoal educationalGoal;

    @ManyToOne
    //@JoinColumn(name = "subject_id", nullable = false)
    private Subject subject;


    public Outcome(Integer id, String description, Subject subject) {
        super();
        this.id = id;
        this.description = description;
        this.subject = subject;
    }

    public Outcome(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

}
