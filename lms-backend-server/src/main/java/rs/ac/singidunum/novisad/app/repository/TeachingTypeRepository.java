package rs.ac.singidunum.novisad.app.repository;

import org.springframework.data.repository.CrudRepository;

import rs.ac.singidunum.novisad.app.model.TeachingType;

public interface TeachingTypeRepository extends CrudRepository<TeachingType, Integer>{

}
