package rs.ac.singidunum.novisad.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.app.model.Title;
import rs.ac.singidunum.novisad.app.repository.TitleRepository;

@Service
public class TitleService {
	@Autowired
	private TitleRepository titleRepository;

	public Iterable<Title> findAll(){
		return this.titleRepository.findAll();
	}
	
	public Optional<Title> findById(Integer id) {
		return this.titleRepository.findById(id);
	}
	
	public Title save(Title t) {
		return this.titleRepository.save(t);
	}
	
	public void delete(Title t) {
		this.titleRepository.delete(t);
	}
	
	public void delete(Integer id) {
		this.titleRepository.deleteById(id);
	}
}
