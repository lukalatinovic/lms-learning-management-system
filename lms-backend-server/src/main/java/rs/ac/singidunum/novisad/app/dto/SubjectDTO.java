package rs.ac.singidunum.novisad.app.dto;

import java.util.ArrayList;

public class SubjectDTO {
    private Integer id;
    private String name;
    private Integer espb;
    private Boolean mandatory;
    private Integer numLectureClasses;
    private Integer numPracticeClasses;
    private Integer numOtherFormsClasses;
    private Integer researchWork;
    private Integer numOtherClasses;
    private ArrayList<SubjectDTO> precoditionClasses;

    public SubjectDTO() {
        super();
    }

    public SubjectDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public SubjectDTO(String name, Integer espb, Boolean mandatory, Integer numLectureClasses,
                      Integer numPracticeClasses, Integer numOtherFormsClasses, Integer researchWork, Integer numOtherClasses) {
        this.name = name;
        this.espb = espb;
        this.mandatory = mandatory;
        this.numLectureClasses = numLectureClasses;
        this.numPracticeClasses = numPracticeClasses;
        this.numOtherFormsClasses = numOtherFormsClasses;
        this.researchWork = researchWork;
        this.numOtherClasses = numOtherClasses;
    }

    public SubjectDTO(Integer id, String name, Integer espb, Boolean mandatory, Integer numLectureClasses,
                      Integer numPracticeClasses, Integer numOtherFormsClasses, Integer researchWork, Integer numOtherClasses) {
        this.id = id;
        this.name = name;
        this.espb = espb;
        this.mandatory = mandatory;
        this.numLectureClasses = numLectureClasses;
        this.numPracticeClasses = numPracticeClasses;
        this.numOtherFormsClasses = numOtherFormsClasses;
        this.researchWork = researchWork;
        this.numOtherClasses = numOtherClasses;
    }

    public SubjectDTO(Integer id, String name, Integer espb, Boolean mandatory, Integer numLectureClasses,
                      Integer numPracticeClasses, Integer numOtherFormsClasses, Integer researchWork, Integer numOtherClasses,
                      ArrayList<SubjectDTO> precoditionClasses) {
        this.id = id;
        this.name = name;
        this.espb = espb;
        this.mandatory = mandatory;
        this.numLectureClasses = numLectureClasses;
        this.numPracticeClasses = numPracticeClasses;
        this.numOtherFormsClasses = numOtherFormsClasses;
        this.researchWork = researchWork;
        this.numOtherClasses = numOtherClasses;
        this.precoditionClasses = precoditionClasses;
    }
    
    public SubjectDTO(Integer id, String name, Integer espb, Boolean mandatory) {
        this.id = id;
        this.name = name;
        this.espb = espb;
        this.mandatory = mandatory;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getEspb() {
        return espb;
    }

    public void setEspb(Integer espb) {
        this.espb = espb;
    }

    public Boolean getMandatory() {
        return mandatory;
    }

    public void setMandatory(Boolean mandatory) {
        this.mandatory = mandatory;
    }

    public Integer getNumLectureClasses() {
        return numLectureClasses;
    }

    public void setNumLectureClasses(Integer numLectureClasses) {
        this.numLectureClasses = numLectureClasses;
    }

    public Integer getNumPracticeClasses() {
        return numPracticeClasses;
    }

    public void setNumPracticeClasses(Integer numPracticeClasses) {
        this.numPracticeClasses = numPracticeClasses;
    }

    public Integer getNumOtherFormsClasses() {
        return numOtherFormsClasses;
    }

    public void setNumOtherFormsClasses(Integer numOtherFormsClasses) {
        this.numOtherFormsClasses = numOtherFormsClasses;
    }

    public Integer getResearchWork() {
        return researchWork;
    }

    public void setResearchWork(Integer researchWork) {
        this.researchWork = researchWork;
    }

    public Integer getNumOtherClasses() {
        return numOtherClasses;
    }

    public void setNumOtherClasses(Integer numOtherClasses) {
        this.numOtherClasses = numOtherClasses;
    }

    public ArrayList<SubjectDTO> getPrecoditionClasses() {
        return precoditionClasses;
    }

    public void setPrecoditionClasses(ArrayList<SubjectDTO> precoditionClasses) {
        this.precoditionClasses = precoditionClasses;
    }
}
