package rs.ac.singidunum.novisad.app.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import rs.ac.singidunum.novisad.app.model.EvaluationType;
import rs.ac.singidunum.novisad.app.model.Examination;

public interface ExaminationRepository extends CrudRepository<Examination, Long> {
	List<Examination> findByStudentOnYearId(Integer studentOnYearId);
}
