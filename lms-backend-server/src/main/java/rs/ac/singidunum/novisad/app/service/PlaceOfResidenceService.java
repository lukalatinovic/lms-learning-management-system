package rs.ac.singidunum.novisad.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.app.model.PlaceOfResidence;
import rs.ac.singidunum.novisad.app.repository.PlaceOfResidenceRepository;

@Service
public class PlaceOfResidenceService {
    @Autowired
    private PlaceOfResidenceRepository placeOfResidenceRepository;

    public Iterable<PlaceOfResidence> findAll(){
        return this.placeOfResidenceRepository.findAll();
    }

    public Optional<PlaceOfResidence> findById(Integer id) {
        return this.placeOfResidenceRepository.findById(id);
    }

    public PlaceOfResidence save(PlaceOfResidence c) {
        return this.placeOfResidenceRepository.save(c);
    }

    public void delete(PlaceOfResidence c) {
        this.placeOfResidenceRepository.delete(c);
    }

    public void delete(Integer id) {
        this.placeOfResidenceRepository.deleteById(id);
    }

    public List<PlaceOfResidence> findByCountryId(Integer countryId) {
        return this.placeOfResidenceRepository.findByCountryId(countryId);
    }
}
