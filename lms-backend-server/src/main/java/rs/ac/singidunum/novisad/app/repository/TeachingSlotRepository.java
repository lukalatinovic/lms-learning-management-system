package rs.ac.singidunum.novisad.app.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import rs.ac.singidunum.novisad.app.model.EvaluationKnowledge;
import rs.ac.singidunum.novisad.app.model.TeachingSlot;

import java.util.List;
import java.util.Optional;

public interface TeachingSlotRepository extends CrudRepository<TeachingSlot, Long> {
    @Query(
            "SELECT DISTINCT ts FROM TeachingSlot ts " +
            "JOIN TeachingType tt ON ts.teachingType.id = tt.id " +
            "JOIN TeacherAtImplementation tai ON tt.id = tai.teachingType.id " +
            "WHERE tai.teacher.id = :teacherId"
    )
    public List<TeachingSlot> getAllTeacherSlots(@Param("teacherId") Long teacherId);
}
