package rs.ac.singidunum.novisad.app.repository;

import org.springframework.data.repository.CrudRepository;
import rs.ac.singidunum.novisad.app.model.EducationalGoal;

public interface EducationalGoalRepository extends CrudRepository<EducationalGoal, Long> {
}
