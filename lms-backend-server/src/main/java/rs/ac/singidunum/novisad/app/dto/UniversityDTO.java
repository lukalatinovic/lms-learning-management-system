package rs.ac.singidunum.novisad.app.dto;

import java.util.ArrayList;
import java.util.Date;

public class UniversityDTO {
	private Integer id;
	private String name;
	private Date foundationDate;
	private TeacherDTO rector;
	private AddressDTO address;
	private ArrayList<FacultyDTO> faculties;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getFoundationDate() {
		return foundationDate;
	}
	public void setFoundationDate(Date foundationDate) {
		this.foundationDate = foundationDate;
	}
	public TeacherDTO getRector() {
		return rector;
	}
	public void setRector(TeacherDTO rector) {
		this.rector = rector;
	}
	public AddressDTO getAddress() {
		return address;
	}
	public void setAddress(AddressDTO address) {
		this.address = address;
	}
	public ArrayList<FacultyDTO> getFaculties() {
		return faculties;
	}
	public void setFaculties(ArrayList<FacultyDTO> faculties) {
		this.faculties = faculties;
	}
	
	public UniversityDTO() {
		super();
	}
	
	public UniversityDTO(Integer id, String name, Date foundationDate) {
		super();
		this.id = id;
		this.name = name;
		this.foundationDate = foundationDate;
	}
	
	public UniversityDTO(Integer id, String name, Date foundationDate, TeacherDTO rector, AddressDTO address,
			ArrayList<FacultyDTO> faculties) {
		super();
		this.id = id;
		this.name = name;
		this.foundationDate = foundationDate;
		this.rector = rector;
		this.address = address;
		this.faculties = faculties;
	}
	
	public UniversityDTO(Integer id, String name) {
		this.id = id;
		this.name = name;
	}
}
