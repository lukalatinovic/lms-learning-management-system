package rs.ac.singidunum.novisad.app.model;

import jakarta.persistence.*;

import java.util.List;

@Entity
public class TitleType {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable = false, length = 20)
	private String name;
	
	@OneToMany(mappedBy = "titleType")
	private List<Title> title;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public TitleType() {
		super();
	}

	public TitleType(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public List<Title> getTitle() {
		return title;
	}

	public void setTitle(List<Title> title) {
		this.title = title;
	}
}
