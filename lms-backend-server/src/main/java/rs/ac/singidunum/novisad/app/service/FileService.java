package rs.ac.singidunum.novisad.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.singidunum.novisad.app.model.EvaluationType;
import rs.ac.singidunum.novisad.app.model.File;
import rs.ac.singidunum.novisad.app.repository.EvulationalTypeRepository;
import rs.ac.singidunum.novisad.app.repository.FileRepository;

import java.util.Optional;

@Service
public class FileService {

    @Autowired
    private FileRepository fileRepository;

    public Iterable<File> findAll(){
        return this.fileRepository.findAll();
    }

    public Optional<File> findById(Long id) {
        return this.fileRepository.findById(id);
    }

    public File save(File file) {
        return this.fileRepository.save(file);
    }

    public void delete(File file) {
        this.fileRepository.delete(file);
    }

    public void delete(Long id) {
        this.fileRepository.deleteById(id);
    }
}
