package rs.ac.singidunum.novisad.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.novisad.app.dto.CountryDTO;
import rs.ac.singidunum.novisad.app.model.Country;
import rs.ac.singidunum.novisad.app.service.CountryService;

@Controller
@RequestMapping("/api/countries")
public class CountryController {

    private CountryService countryService;

    @Autowired
    public CountryController(CountryService countryService) {
        this.countryService = countryService;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<CountryDTO>> findAll() {
        ArrayList<CountryDTO> countriesDTO = new ArrayList<>();
        Iterable<Country> countries = countryService.findAll();

        for (Country c : countries) {
            countriesDTO.add(new CountryDTO(c.getId(), c.getName()));
        }

        return new ResponseEntity<>(countriesDTO, HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<CountryDTO> createCountry(@RequestBody CountryDTO newCountry) {
        Country c = countryService.save(new Country(null, newCountry.getName()));
        return new ResponseEntity<>(new CountryDTO(c.getId(), c.getName()), HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<CountryDTO> getCountry(@PathVariable("id") Integer id) {
        Optional<Country> country = countryService.findById(id);
        if (country.isPresent()) {
            return new ResponseEntity<>(
                    new CountryDTO(country.get().getId(), country.get().getName()),
                    HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<CountryDTO> updateCountry(@PathVariable("id") Integer id, @RequestBody CountryDTO newCountry) {
        Optional<Country> country = countryService.findById(id);

        if (country.isPresent()) {
            Country updatedCountry = country.get();
            updatedCountry.setName(newCountry.getName());
            countryService.save(updatedCountry);
            return new ResponseEntity<>(
                    new CountryDTO(updatedCountry.getId(), updatedCountry.getName()),
                    HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<CountryDTO> deleteCountry(@PathVariable("id") Integer id) {
        Optional<Country> country = countryService.findById(id);
        if (country.isPresent()) {
            countryService.delete(country.get());
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
