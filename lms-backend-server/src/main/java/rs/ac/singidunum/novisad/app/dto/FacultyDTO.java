package rs.ac.singidunum.novisad.app.dto;

import java.util.ArrayList;
import java.util.List;

public class FacultyDTO {
	private Integer id;
	private String name;
	private AddressDTO address;
	private TeacherDTO dean;
	private List<StudyProgrammeDTO> studyProgrammes;
	private UniversityDTO university;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public AddressDTO getAddress() {
		return address;
	}
	public void setAddress(AddressDTO address) {
		this.address = address;
	}
	public TeacherDTO getDean() {
		return dean;
	}
	public void setDean(TeacherDTO dean) {
		this.dean = dean;
	}
	public List<StudyProgrammeDTO> getStudyProgrammes() {
		return studyProgrammes;
	}
	public void setStudyProgrammes(ArrayList<StudyProgrammeDTO> studyProgrammes) {
		this.studyProgrammes = studyProgrammes;
	}
	public UniversityDTO getUniversity() {
		return university;
	}
	public void setUniversity(UniversityDTO university) {
		this.university = university;
	}
	
	public FacultyDTO() {
		super();
	}
	
	public FacultyDTO(String name) {
		super();
		this.name = name;
	}
	
	public FacultyDTO(String name, AddressDTO address) {
		super();
		this.name = name;
		this.address = address;
	}
	
	public FacultyDTO(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public FacultyDTO(Integer id, String name, AddressDTO address, TeacherDTO dean, ArrayList<StudyProgrammeDTO> studyProgrammes,
			UniversityDTO university) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.dean = dean;
		this.studyProgrammes = studyProgrammes;
		this.university = university;
	}

	public FacultyDTO(Integer id, String name, AddressDTO address, TeacherDTO dean) {
        super();
        this.id = id;
        this.name = name;
        this.address = address;
        this.dean = dean;
    }
	
	public FacultyDTO(Integer id, String name, AddressDTO address, TeacherDTO dean, List<StudyProgrammeDTO> studyProgrammes,
			UniversityDTO university) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.dean = dean;
		this.studyProgrammes = studyProgrammes;
		this.university = university;
	}
}
