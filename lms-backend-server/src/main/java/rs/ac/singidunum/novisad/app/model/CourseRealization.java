package rs.ac.singidunum.novisad.app.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;

@Entity
public class CourseRealization {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    //@JoinColumn(name = "subject_id", referencedColumnName = "id")
    private Subject subject;

    @ManyToOne
    private Teacher teacher;

    @OneToMany(mappedBy = "courseRealization")
    private Set<CourseAttendance> courseAttendances = new HashSet<>();

    @OneToMany(mappedBy = "courseRealization")
    private List<TeachingSlot> teachingSlot;

    public CourseRealization(Subject subject, Teacher teacher) {
        this.subject = subject;
        this.teacher = teacher;
    }

    public CourseRealization(Integer id, Subject subject, Teacher teacher) {
        this.id = id;
        this.subject = subject;
        this.teacher = teacher;
    }

    public CourseRealization() {
    }

    public CourseRealization(Subject subject) {
        this.subject = subject;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Subject getSubject() {
        return subject;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Set<CourseAttendance> getCourseAttendances() {
        return courseAttendances;
    }

    public void setCourseAttendances(Set<CourseAttendance> courseAttendances) {
        this.courseAttendances = courseAttendances;
    }

    public void addCourseAttendance(CourseAttendance courseAttendance) {
        courseAttendances.add(courseAttendance);
        courseAttendance.setCourseRealization(this);
    }

    public void removeCourseAttendance(CourseAttendance courseAttendance) {
        courseAttendances.remove(courseAttendance);
        courseAttendance.setCourseRealization(null);
    }

    public List<TeachingSlot> getTeachingSlot() {
        return teachingSlot;
    }

    public void setTeachingSlot(List<TeachingSlot> teachingSlot) {
        this.teachingSlot = teachingSlot;
    }
}
