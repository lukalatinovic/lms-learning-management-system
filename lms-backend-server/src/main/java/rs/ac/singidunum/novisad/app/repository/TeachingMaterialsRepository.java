package rs.ac.singidunum.novisad.app.repository;

import org.springframework.data.repository.CrudRepository;
import rs.ac.singidunum.novisad.app.model.TeachingMaterials;

public interface TeachingMaterialsRepository extends CrudRepository<TeachingMaterials, Long> {
}
