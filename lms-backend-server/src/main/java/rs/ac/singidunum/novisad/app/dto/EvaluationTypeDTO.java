package rs.ac.singidunum.novisad.app.dto;

import jakarta.persistence.Column;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import rs.ac.singidunum.novisad.app.model.EvaluationKnowledge;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
public class EvaluationTypeDTO {
    private Long id;

    private String name;

    private List<EvaluationKnowledge> evaluationKnowledgeList;

    public EvaluationTypeDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EvaluationKnowledge> getEvaluationKnowledgeList() {
        return evaluationKnowledgeList;
    }

    public void setEvaluationKnowledgeList(List<EvaluationKnowledge> evaluationKnowledgeList) {
        this.evaluationKnowledgeList = evaluationKnowledgeList;
    }
}
