package rs.ac.singidunum.novisad.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.app.model.ScienceField;
import rs.ac.singidunum.novisad.app.repository.ScienceFieldRepository;

@Service
public class ScienceFieldService {
	@Autowired
	private ScienceFieldRepository scienceFieldRepository;

	public Iterable<ScienceField> findAll(){
		return this.scienceFieldRepository.findAll();
	}
	
	public Optional<ScienceField> findById(Integer id) {
		return this.scienceFieldRepository.findById(id);
	}
	
	public ScienceField save(ScienceField c) {
		return this.scienceFieldRepository.save(c);
	}
	
	public void delete(ScienceField c) {
		this.scienceFieldRepository.delete(c);
	}
	
	public void delete(Integer id) {
		this.scienceFieldRepository.deleteById(id);
	}
}
