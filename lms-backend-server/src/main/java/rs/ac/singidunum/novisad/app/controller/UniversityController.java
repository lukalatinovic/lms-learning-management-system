package rs.ac.singidunum.novisad.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import rs.ac.singidunum.novisad.app.dto.*;
import rs.ac.singidunum.novisad.app.model.*;
import rs.ac.singidunum.novisad.app.service.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/api/universities")
public class UniversityController {

    private final UniversityService universityService;
    private final AddressService addressService;
    private final TeacherService teacherService;
    private final FacultyService facultyService;

    @Autowired
    public UniversityController(UniversityService universityService, AddressService addressService, TeacherService teacherService, FacultyService facultyService) {
        this.universityService = universityService;
        this.addressService = addressService;
        this.teacherService = teacherService;
        this.facultyService = facultyService;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<UniversityDTO>> findAll() {
        ArrayList<UniversityDTO> universitiesDTO = new ArrayList<>();
        Iterable<University> universities = universityService.findAll();

        for (University u : universities) {
            universitiesDTO.add(new UniversityDTO(u.getId(), u.getName(), u.getFoundationDate()));
        }

        return new ResponseEntity<>(universitiesDTO, HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<UniversityDTO> createUniversity(@RequestBody UniversityDTO newUniversity) {
        University university = this.universityService.save(new University(null, newUniversity.getName(), newUniversity.getFoundationDate(), null, null, null));
        return new ResponseEntity<>(new UniversityDTO(university.getId(), university.getName(), university.getFoundationDate()), HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<UniversityDTO> getUniversity(@PathVariable("id") Integer id) {
        Optional<University> universityOpt = universityService.findById(id);

        if (universityOpt.isPresent()) {
            University university = universityOpt.get();

            // Mapiranje podataka za Address
            Address address = university.getAddress();
            PlaceOfResidence placeOfResidence = address.getPlaceOfResidence();
            CountryDTO countryDTO = new CountryDTO(
                placeOfResidence.getCountry().getId(),
                placeOfResidence.getCountry().getName()
            );
            PlaceOfResidenceDTO placeOfResidenceDTO = new PlaceOfResidenceDTO(
                placeOfResidence.getId(),
                placeOfResidence.getName(),
                countryDTO
            );
            AddressDTO addressDTO = new AddressDTO(
                address.getId(),
                address.getStreet(),
                address.getNumber(),
                placeOfResidenceDTO
            );

            // Mapiranje podataka za Teacher (Rector)
            Teacher rector = university.getRector();
            TeacherDTO rectorDTO = new TeacherDTO(
                rector.getId(),
                rector.getFirstName(),
                rector.getLastName(),
                rector.getBiography()
            );

            // Mapiranje podataka za Faculty
            List<FacultyDTO> facultyDTOList = university.getFaculties().stream()
                .map(faculty -> new FacultyDTO(
                    faculty.getId(),
                    faculty.getName(),
                    new AddressDTO(
                        faculty.getAddress().getId(),
                        faculty.getAddress().getStreet(),
                        faculty.getAddress().getNumber(),
                        new PlaceOfResidenceDTO(
                            faculty.getAddress().getPlaceOfResidence().getId(),
                            faculty.getAddress().getPlaceOfResidence().getName(),
                            new CountryDTO(
                                faculty.getAddress().getPlaceOfResidence().getCountry().getId(),
                                faculty.getAddress().getPlaceOfResidence().getCountry().getName()
                            )
                        )
                    ),
                    new TeacherDTO(
                        faculty.getDean().getId(),
                        faculty.getDean().getFirstName(),
                        faculty.getDean().getLastName(),
                        faculty.getDean().getBiography()
                    )
                ))
                .collect(Collectors.toList());

            // Mapiranje podataka za UniversityDTO
            UniversityDTO universityDTO = new UniversityDTO();
            universityDTO.setId(university.getId());
            universityDTO.setName(university.getName());
            universityDTO.setFoundationDate(university.getFoundationDate());
            universityDTO.setRector(rectorDTO);
            universityDTO.setAddress(addressDTO);
            universityDTO.setFaculties(new ArrayList<>(facultyDTOList));

            return new ResponseEntity<>(universityDTO, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<UniversityDTO> updateUniversity(@PathVariable("id") Integer id, @RequestBody UniversityDTO newUniversity) {
        Optional<University> optionalUniversity = this.universityService.findById(id);
        if (optionalUniversity.isPresent()) {
            University university = optionalUniversity.get();
            university.setName(newUniversity.getName());
            university.setFoundationDate(newUniversity.getFoundationDate());
            this.universityService.save(university);
            return new ResponseEntity<>(new UniversityDTO(university.getId(), university.getName(), university.getFoundationDate()), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteUniversity(@PathVariable("id") Integer id) {
        Optional<University> university = this.universityService.findById(id);
        if (university.isPresent()) {
            this.universityService.delete(university.get());
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
