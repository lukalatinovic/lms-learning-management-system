package rs.ac.singidunum.novisad.app.dto;

import java.sql.Timestamp;
import java.util.List;

public class TeacherStudentsDTO {
    private String firstName;
    private String lastName;
    private String index;
    private Timestamp yearOfEnrollments;
    private Double averageGrade;

    private Long ectsScoredPoints;

    private List<Exam> passedExams;
    private List<Exam> failedExams;

    public TeacherStudentsDTO(String firstName, String lastName, String index, Timestamp yearOfEnrollments, Double averageGrade, Long ectsScoredPoints) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.index = index;
        this.yearOfEnrollments = yearOfEnrollments;
        this.averageGrade = averageGrade;
        this.ectsScoredPoints = ectsScoredPoints;
    }

    public TeacherStudentsDTO(String firstName, String lastName, String index, Timestamp yearOfEnrollments, Double averageGrade, Long ectsScoredPoints, List<Exam> passedExams, List<Exam> failedExams) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.index = index;
        this.yearOfEnrollments = yearOfEnrollments;
        this.averageGrade = averageGrade;
        this.ectsScoredPoints = ectsScoredPoints;
        this.passedExams = passedExams;
        this.failedExams = failedExams;
    }

    public Timestamp getYearOfEnrollments() {
        return yearOfEnrollments;
    }

    public void setYearOfEnrollments(Timestamp yearOfEnrollments) {
        this.yearOfEnrollments = yearOfEnrollments;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public Double getAverageGrade() {
        return averageGrade;
    }

    public void setAverageGrade(Double averageGrade) {
        this.averageGrade = averageGrade;
    }

    public Long getEctsScoredPoints() {
        return ectsScoredPoints;
    }

    public void setEctsScoredPoints(Long ectsScoredPoints) {
        this.ectsScoredPoints = ectsScoredPoints;
    }

    public List<Exam> getPassedExams() {
        return passedExams;
    }

    public void setPassedExams(List<Exam> passedExams) {
        this.passedExams = passedExams;
    }

    public List<Exam> getFailedExams() {
        return failedExams;
    }

    public void setFailedExams(List<Exam> failedExams) {
        this.failedExams = failedExams;
    }
}

class Exam {
    String subjectName;
    Integer grade;
    Integer scoredPoints;

    public Exam(String subjectName, Integer grade, Integer scoredPoints) {
        this.subjectName = subjectName;
        this.grade = grade;
        this.scoredPoints = scoredPoints;
    }
}
