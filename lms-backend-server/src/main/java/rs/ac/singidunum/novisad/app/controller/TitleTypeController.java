package rs.ac.singidunum.novisad.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import rs.ac.singidunum.novisad.app.dto.TitleTypeDTO;
import rs.ac.singidunum.novisad.app.model.TitleType;
import rs.ac.singidunum.novisad.app.service.TitleTypeService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/titleTypes")
public class TitleTypeController {

    private TitleTypeService titleTypeService;

    @Autowired
    public TitleTypeController(TitleTypeService titleTypeService) {
        this.titleTypeService = titleTypeService;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<TitleTypeDTO>> findAll() {
        List<TitleTypeDTO> titleTypesDTO = new ArrayList<>();
        Iterable<TitleType> titleTypes = titleTypeService.findAll();

        for (TitleType t : titleTypes) {
            titleTypesDTO.add(new TitleTypeDTO(t.getId(), t.getName(), null));
        }

        return new ResponseEntity<>(titleTypesDTO, HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<TitleTypeDTO> createTitleType(@RequestBody TitleTypeDTO newTitleType) {
        TitleType titleType = this.titleTypeService.save(new TitleType(null, newTitleType.getName()));
        return new ResponseEntity<>(new TitleTypeDTO(titleType.getId(), titleType.getName(), null), HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<TitleTypeDTO> getTitleType(@PathVariable("id") Integer id) {
        Optional<TitleType> titleType = this.titleTypeService.findById(id);
        if (titleType.isPresent()) {
            return new ResponseEntity<>(
                    new TitleTypeDTO(titleType.get().getId(), titleType.get().getName(), null),
                    HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<TitleTypeDTO> updateTitleType(@PathVariable("id") Integer id, @RequestBody TitleTypeDTO newTitleType) {
        Optional<TitleType> titleType = this.titleTypeService.findById(id);

        if (titleType.isPresent()) {
            titleType.get().setId(id);
            titleType.get().setName(newTitleType.getName());
            titleType.get().setTitle(null);
            this.titleTypeService.save(titleType.get());
            return new ResponseEntity<>(
                    new TitleTypeDTO(titleType.get().getId(), titleType.get().getName(), null),
                    HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteTitleType(@PathVariable("id") Integer id) {
        titleTypeService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
